
$(document).ajaxStart(function () {
    Pace.restart()
});

function __404(element){
    $.ajax({
        url: '404.html',
        type: 'get',
        async: true,
        success: function (errorPage) {
            $(element).html(errorPage);
            __EndLoad();
        },
        error: function (error) {
            $(element).html(error);
            __EndLoad();
        }
    });
}

function showTab(idTab){
    $("#Li_Nav_" + idTab).click();
}

function restoreNavigation(url, tabInfo){
    __Load();
    $.ajax({
        url: url,
        type: 'post',
        data: JSON.stringify(tabInfo),
        success: function (navigation) {
            $("#nav_content_page").html(navigation);
            $.each(tabInfo, function (i, tab) {
                tabTrigger("a[href='#" + tab['TabId'] + "']");
                contentAutoHeight({
                    element: "#" + tab['TabId'] + "Content",
                    scroll: true,
                    drag: true,
                    substract: 170,
                    plugin: "slimScroll"
                });
                if (tab['TabStatus'] === "active") {
                    const activeId = "#" + tab['TabId'] + "Content";
                    showTab(tab.TabId);
                    setContentFromURL(tab['TabUrl'], activeId);
                }
            });
        },
        error: function (error) {
            $("#nav_content_page").html(error.responseText);
        }
    });
}

function contentAutoHeight(parameters) {
    const element = parameters.element,
        scroll = parameters.scroll,
        drag = parameters.drag,
        substract = parameters.substract,
        plugin = parameters.plugin;
    
    let options = "";
    const contentHeight = $(window).height() - substract;

    if (plugin === "niceScroll") {
        $(element).attr("style", "height:" + contentHeight + "px;");
        if (drag === true) {
            options = {
                touchbehavior: true,
                usetransition: true,
                hwacceleration: true
                //horizrailenabled:false
            };
        } else {
            options = "";
        }
        
        if (scroll === true) {
            $(element).niceScroll(options);
        }
    } else if (plugin === "slimScroll") {
        $(window).resize(function () {
            const height = $(window).height() - (substract + 7);
            $(element).height(height);
            $(element).parent('.slimScrollDiv').height(height + 7);
        });

        $(element).slimScroll({
            height: contentHeight,
            wheelStep: 10,
            touchScrollStep: 75,
            size: "4px",
            distance: '2px',
            railVisible: true
        });
    } else {
        console.log("No Plugin");
    }
}

function closeTab(id, navId){
    const currTab = $("#" + navId);
    const prev = currTab.prev();
    const next = currTab.next();

    currTab.remove();
    $("#" + id).remove();
    
    deleteData({
        key: 'mainTabs',
        param: 'TabId',
        find: id
    });

    if (next.length === 0) {
        prev.find("a").click();
    } else {
        next.find("a").click();
    }
}

function renderPage(html, data) {
    if(!data)
        return html;
    
    for (let key in data) {
        if (!key) {
            return html;
        }
        html = html.replace('{{' + key + '}}', data[key]);
    }
    return html;
}

function ajaxServices(params){
    const Module = params.Module, //Nama Module
        form = params.form, //form ID
        data = params.data, //form serialize
        action = params.action, //form url
        method = params.method, //form method
        type = params.type, //type of response data
        request = params.request, //Module name
        target = params.target; //element of target

    if (method.toLowerCase() === 'post') {
        $.post(action, data + '&request=' + request, function (response) {
            $(Module).find(target).html(renderPage(response.views, response.content));
        }, 'json');
        $(Module).find(form).find('[type=submit]').button('reset');
    } else if (method.toLowerCase() === 'get') {
        $.get(action, data, function (response) {
            //console.log(response);
            $(target).html(response);
        });
    } else {
        console.log("Method Not Found");
    }

}

function setContentFromURL(url, element, param) {
    $.get(url, param, function (response) {
        $(element).html(response);
        initializeForm();
        __EndLoad();
    }).fail(function () {
        __404(element);
    });
}

function updateTab(tab) {
    if (tab) {
        tab.TabStatus = "active";
        $.post("/NavTab/NavTabContent", tab, function (result) {
            $("#" + tab.TabId).replaceWith(result);
            showTab(tab.TabId);
            setContentFromURL(tab.TabUrl, "#" + tab.TabId + "Content");
        }).fail(function () {
            __EndLoad();
        });
    }
}

function appendTab(tab) {
    __Load();
    tab.TabStatus = "active";
    $.post("/NavTab/NavTab", tab, function (result) {
        $("#Ul_Tab_Nav").append(result);
        $.post("/NavTab/NavTabContent", tab, function (result) {
            $("#Tab_Content_Page").append(result);
            showTab(tab.TabId);
            setContentFromURL(tab.TabUrl, "#" + tab.TabId + "Content");
            __EndLoad();
        }).fail(function () {
            __EndLoad();
        });
    }).fail(function () {
        __EndLoad();
    });
}

function restoreSettings(){
    const tabs = getData({key: 'mainTabs'}); 
    const appSettings = getData({key: 'appSettings'});
    
    if (tabs !== null) {
        restoreNavigation("/NavTab/MainTab", tabs);
    } else {
        pushData({
            key: 'mainTabs',
            data: [{
                TabIndex: 0,
                TabId: "home",
                TabUrl: "/Admins/Home",
                TabTitle: "Home",
                TabIcon: "fa fa-home",
                TabDesc: "Selamat Datang Admin",
                TabStatus: 'active'
            }]
        });
        tabTrigger("a[href='#home']");
        $("a[href='#home']").tab('show');
        setContentFromURL("/Admins/Home", "#homeContent");
    }

    if (appSettings !== null) {
        if (appSettings[0].sidebar === "sidebar-collapse") {
            $("body").addClass(appSettings[0].sidebar);
        } else {
            $("body").removeClass(appSettings[0].sidebar);
        }
        if (appSettings[0].skin !== null) {
            $("body").addClass(appSettings[0].skin);
        }
    }

}

function reloadContent(element, url) {
    __Load();
    updateTab(findData({key: 'mainTabs', param: 'TabId', find: element}));
    contentAutoHeight({
        element: "#" + element + "Content",
        scroll: true,
        drag: false,
        substract: 170,
        plugin: "slimScroll"
    });
    $("#" + element + "Content").removeClass("animated fadeIn").width('auto').addClass("animated fadeIn");
}

function buildTabs(opt){
    const tabNav = $("#Ul_Tab_Nav"),
        tabCon = $("#Tab_Content_Page"),
        checkTabs = tabNav.find("a[href='#" + opt.id + "']"),
        prevIndex = tabNav.find("li:last-child").attr("data-index"),
        index = parseInt(prevIndex) + 1;

    tabNav.find('li.active').removeClass('active');
    tabCon.find('div.tab-pane.active').removeClass('active');


    if (checkTabs.length < 1) {
        const tab = {
            TabIndex: index,
            TabId: opt.id,
            TabUrl: opt.url,
            TabTitle: opt.title,
            TabIcon: opt.icon,
            TabDesc: opt.desc,
            TabStatus: ''
        };
        pushData({
            key: 'mainTabs',
            data: [tab]
        });
        appendTab(tab);
        contentAutoHeight({
            element: "#" + tab.TabId + "Content",
            scroll: true,
            drag: false,
            substract: 170,
            plugin: "slimScroll"
        });
        initDynamicTab();
        tabTrigger("a[href='#" + opt.id + "']");
        return true;
    } else {
        const checkReload = $("#" + opt.id + "Content");
        if (checkReload.hasClass("reload") === true) {
            setContentFromURL(opt.url, "#" + opt.id + "Content");
            checkReload.removeClass("animated fadeIn").width('auto').addClass("animated fadeIn");
            checkReload.removeClass("reload");
        }
        initDynamicTab();
        return false;
    }
}

function tabTrigger(element){
    $(element).on('shown.bs.tab', function () {
        const find = parseInt($(this).parent("li").attr("data-index"));
        changeData({
            key: 'mainTabs',
            findParam: 'TabIndex',
            setParam: 'TabStatus',
            find: find,
            setValue: 'active:'
        });
    });
}

function initMenuClick() {
    //Skin Click
    $(".tb-skin td").click(function(){
        const skin = $(this).find('img').attr("alt"), oldSkin = getData({key: "appSettings"});
        $("body").removeClass(oldSkin[0].skin).addClass(skin);
        updateData({key: 'appSettings',setParam:'skin',replace:true, setValue: skin});
    });

    //Save Sidebar Collapse Setting
    $('.sidebar-toggle').click(function() {
        const sidebar = getData({key: 'appSettings'});
        if ($('body').delay(300).hasClass('sidebar-collapse')) {
            if (sidebar !== null) {
                updateData({
                    key: 'appSettings',
                    findParam: 'sidebar',
                    setParam: 'sidebar',
                    find: 'sidebar-collapse',
                    setValue: ""
                });
            } else {
                pushData({key: 'appSettings', data: [{sidebar: "", skin: "skin-blue"}]});
            }
        } else {
            if (sidebar !== null) {
                updateData({
                    key: 'appSettings',
                    findParam: 'sidebar',
                    setParam: 'sidebar',
                    find: '',
                    setValue: "sidebar-collapse"
                });
            } else {
                pushData({key: 'appSettings', data: [{sidebar: "sidebar-collapse", skin: "skin-blue"}]});
            }
        }
    });

    $(".dropdown-menu li>.menu").slimScroll({height: "200px", size: "3px"});
    
    $(".subMenu").click(function(){
        let menuId = $(this);
        const opt = {
            url: $(this).data("url"),
            id: $(this).data("id"),
            desc: $(this).data("desc"),
            parentMenu: $(this).parents("li.treeview").find("a"),
            icon: $(this).find("i").attr("class"),
            title: $(this).find("span").html()
        };
        const checkTabs = buildTabs(opt);

        const currentTab = $('.mainNav>ul>li>a[href="#' + opt.id + '"]'),
            index = parseInt(currentTab.parent("li").attr("data-index"));
        currentTab.tab('show');
        
        if (checkTabs === false) {
            changeData({
                key: 'mainTabs',
                findParam: 'TabIndex',
                setParam: 'TabStatus',
                find: index,
                setValue: 'active:'
            });
        }
    });

    $(".tabCreate").click(function () {
        const opt = {
            url: $(this).data("url"),
            id: $(this).data("id"),
            desc: $(this).data("desc"),
            parentMenu: $(this).data("parent"),
            parentIcon: $(this).data("parent-icon"),
            icon: $(this).data("icon"),
            title: $(this).data("title")
        };
        const checkTabs = buildTabs(opt);
        const currentTab = $('.mainNav>ul>li>a[href="#' + opt.id + '"]'),
            index = parseInt(currentTab.parent("li").attr("data-index"));

        currentTab.tab('show');
        if (checkTabs === false) {
            changeData({
                key: 'mainTabs',
                findParam: 'TabIndex',
                setParam: 'TabStatus',
                find: index,
                setValue: 'active:' 
            });
        }
    });
}

function buildAppInfo(){
    const appData = $.ajax({
            url: 'app.json', type: 'get', async: false, success: function (data) {
                return data;
            }
        }),
        AppName = appData.responseJSON[0].AppName,
        AppDesc = appData.responseJSON[0].AppDesc,
        AppVersion = appData.responseJSON[0].AppVersion,
        AppLink = appData.responseJSON[0].AppLink,
        AppLogo = appData.responseJSON[0].AppLogo,
        AppWebsite = appData.responseJSON[0].AppWebsite,
        AppAuthor = appData.responseJSON[0].AppAuthor,
        AuthorWebsite = appData.responseJSON[0].AuthorWebsite,
        ModuleMenu = appData.responseJSON[0].DataContent[0].ModuleData;

    const appInfo = getData({key: 'appInfo'}),
        menu = getData({key: 'ModuleMenu'}),
        appSettings = getData({key: 'appSettings'}),
        c_AppLink = $("body>.wrapper>.main-header>a"),
        c_AppName = $("body>.wrapper>.main-header>a>span.logo-lg"),
        c_AppLogo = $("body>.wrapper>.main-header>a>span.logo-mini"),
        c_AppVersion = $("footer.main-footer>div>span.version"),
        c_AppAuthor = $("footer.main-footer>span.copyright>a");

    if (appInfo === null) {
        pushData({
            key: 'appInfo',
            data: [{
                AppName: AppName,
                AppDesc: AppDesc,
                AppVersion: AppVersion,
                AppLink: AppLink,
                AppLogo: AppLogo,
                AppWebsite: AppWebsite,
                AppAuthor: AppAuthor,
                AuthorWebsite: AuthorWebsite
            }]
        });
        // Set Title
        document.title = AppName + " | " + AppDesc;
        //Set Web Info
        c_AppLink.attr("href", AppLink);
        c_AppName.html(AppName);
        c_AppLogo.html("<i class='fa fa-shopping-bag'></img>");
        c_AppVersion.html(AppVersion);
        c_AppAuthor.html(AppAuthor);
        c_AppAuthor.attr("href", AuthorWebsite);
    } else {
        if (AppVersion !== appInfo[0].AppVersion) {
            jConfirm("current: app.json(" + appInfo[0].AppVersion + ")<br>new update: app.json(" + AppVersion + ")<br><br>Terdapat Update Setting, Update Sekarang?", "Update Setting", function (r) {
                if (r) {
                    pushData({
                        replace: true,
                        key: 'appInfo',
                        data: [{
                            AppName: AppName,
                            AppDesc: AppDesc,
                            AppVersion: AppVersion,
                            AppLink: AppLink,
                            AppLogo: AppLogo,
                            AppWebsite: AppWebsite,
                            AppAuthor: AppAuthor,
                            AuthorWebsite: AuthorWebsite
                        }]
                    });
                    // Set Title
                    document.title = AppName + " | " + AppDesc;
                    //Set Web Info
                    c_AppLink.attr("href", AppLink);
                    c_AppName.html(AppName);
                    c_AppLogo.html("<i class='fa fa-shopping-bag'></img>");
                    c_AppVersion.html(AppVersion);
                    c_AppAuthor.html(AppAuthor);
                    c_AppAuthor.attr("href", AuthorWebsite);
                } else {
                    document.title = appInfo[0].AppName + " | " + appInfo[0].AppDesc;
                    //Set Web Info
                    c_AppLink.attr("href", appInfo[0].AppLink);
                    c_AppName.html(appInfo[0].AppName);
                    c_AppLogo.html("<i class='fa fa-shopping-bag'></img>");
                    c_AppVersion.html(appInfo[0].AppVersion);
                    c_AppAuthor.html(appInfo[0].AppAuthor);
                    c_AppAuthor.attr("href", appInfo[0].AuthorWebsite);
                }
            });
        } else {
            document.title = appInfo[0].AppName + " | " + appInfo[0].AppDesc;
            //Set Web Info
            c_AppLink.attr("href", appInfo[0].AppLink);
            c_AppName.html(appInfo[0].AppName);
            c_AppLogo.html("<i class='fa fa-shopping-bag'></img>");
            c_AppVersion.html(appInfo[0].AppVersion);
            c_AppAuthor.html(appInfo[0].AppAuthor);
            c_AppAuthor.attr("href", appInfo[0].AuthorWebsite);
        }
    }

  //cek LocalStorage if data exist
    if (menu === null) {
        //Save Menu Data Into local Storage
        pushData({key: 'ModuleMenu', data: ModuleMenu});
        //Restore To Home
        pushData({
            key: 'mainTabs',
            replace: true,
            data: [{
                TabIndex: 0,
                TabId: "home",
                TabUrl: "/Admins/Home",
                TabTitle: "Home",
                TabIcon: "fa fa-home",
                TabDesc: "Selamat Datang Admin",
                TabStatus: 'active'
            }]
        });
        //Restore Setting if refresh browser
        restoreSettings();
        //Reset Menu Click
        initMenuClick();
    } else {
        if (JSON.stringify(ModuleMenu) !== JSON.stringify(menu)) {
            jConfirm("Terjadi perubahan struktur menu, Anda yakin akan <br> memuat konfigurasi menu yang baru?", AppName + " v. " + AppVersion, function (r) {
                if (r) {
                    //Build Menu
                    //Save Menu Data Into local Storage
                    pushData({key: 'ModuleMenu', data: ModuleMenu, replace: true});
                    //Restore To Home
                    pushData({
                        key: 'mainTabs',
                        replace: true,
                        data: [{
                            TabIndex: 0,
                            TabId: "home",
                            TabUrl: "/Admins/Home",
                            TabTitle: "Home",
                            TabIcon: "fa fa-home",
                            TabDesc: "Selamat Datang Admin",
                            TabStatus: 'active'
                        }]
                    });
                    //Restore Setting if refresh browser
                    restoreSettings();
                    //Reset Menu Click
                    initMenuClick();
                } else {
                    //Restore Setting if refresh browser
                    restoreSettings();
                    //Reset Menu Click
                    initMenuClick();
                }
            });
        } else {
            //Restore Setting if refresh browser
            restoreSettings();
            //Reset Menu Click
            initMenuClick();
        }
    }
  //set default
  if(appSettings===null){
        pushData({key:'appSettings',data:[{sidebar:"",skin:"skin-blue"}]});
        $("body").addClass("skin-blue");
  }
}

function initializeForm(){
    //Init Select2 with Validation
    $('.select2').each(function (i,obj) {
        $(obj).select2().on("change", function () {
            if ($(this).val() !== "") {
                $(this).valid();
            }
        });
    });

    //fixed height with slimscroll
    $("[data-fixedheight]").each(function (i, obj) {
        $(obj).slimScroll($(obj).data("fixedheight")[0]);
    });

    //Pretty Checkbox
    //Checkbox
    $("[type=checkbox]").each(function (i, obj) {
        let checkboxStyle = $(obj).data("style") !== undefined ? $(obj).data("style") : "";
        const checkboxToggle = $(obj).data("label-toggle") !== undefined ? $(obj).data("label-toggle") : "",
            checkboxLabelType = $(obj).data("label-type") !== undefined ? $(obj).data("label-type") : "";
        let checkboxIcon = "", checkboxIconOn = "", checkboxIconOff = "";

        if (checkboxToggle !== true || checkboxToggle === undefined) {
            const checkboxLabel = $(obj).data("label") !== undefined ? $(obj).data("label") : "",
                checkboxLabelStyle = $(obj).data("label-style") !== undefined ? $(obj).data("label-style") : "";
            if (checkboxLabelType === "icon") {
                checkboxStyle += " p-icon";
                checkboxIcon = $(obj).data("label-icon") !== undefined ? "<i class='icon " + $(obj).data("label-icon") + "'></i>" : "";
            } else if (checkboxLabelType === "image") {
                checkboxStyle += " p-image";
                checkboxIcon = $(obj).data("label-icon") !== undefined ? "<img src='" + $(obj).data("label-icon") + "'/>" : "";
            } else {
                checkboxStyle += " p-default";
                checkboxIcon = "";
            }
            if ($(obj).parent('.pretty').length < 1) {
                $(obj).wrap('<div class="pretty ' + checkboxStyle + '"></div>');
                $(obj).after('<div class="state ' + checkboxLabelStyle + '">' + checkboxIcon + '<label>' + checkboxLabel + '</label></div>');
            }
        } else {
            const checkboxLabelOn = $(obj).data("label-on") !== undefined ? $(obj).data("label-on") : "",
                checkboxLabelOff = $(obj).data("label-off") !== undefined ? $(obj).data("label-off") : "",
                checkboxLabelStyleOn = $(obj).data("label-style-on") !== undefined ? $(obj).data("label-style-on") : "",
                checkboxLabelStyleOff = $(obj).data("label-style-off") !== undefined ? $(obj).data("label-style-off") : "";

            if (checkboxLabelType === "icon") {
                checkboxStyle += " p-icon p-toggle";
                checkboxIconOn = $(obj).data("label-icon-on") !== undefined ? "<i class='icon " + $(obj).data("label-icon-on") + "'></i>" : "";
                checkboxIconOff = $(obj).data("label-icon-off") !== undefined ? "<i class='icon " + $(obj).data("label-icon-off") + "'></i>" : "";
            } else if (checkboxLabelType === "image") {
                checkboxStyle += " p-image p-toggle";
                checkboxIconOn = $(obj).data("label-icon-on") !== undefined ? "<img src='" + $(obj).data("label-icon-on") + "'/>" : "";
                checkboxIconOff = $(obj).data("label-icon-off") !== undefined ? "<img src='" + $(obj).data("label-icon-off") + "'/>" : "";
            } else {
                checkboxStyle += " p-default";
                checkboxIconOn = "";
                checkboxIconOff = "";
            }
            if ($(obj).parent('.pretty').length < 1) {
                $(obj).wrap('<div class="pretty ' + checkboxStyle + '"></div>');
                $(obj).after('<div class="state ' + checkboxLabelStyleOn + ' p-on">' + checkboxIconOn + '<label>' + checkboxLabelOn + '</label></div>');
                $(obj).after('<div class="state ' + checkboxLabelStyleOff + ' p-off">' + checkboxIconOff + '<label>' + checkboxLabelOff + '</label></div>');
            }
        }
    });

    //Radio
    $("[type=radio]").each(function (i, obj) {
        let radioStyle = $(obj).data("style") !== undefined ? $(obj).data("style") : "";
        const radioLabelType = $(obj).data("label-type") !== undefined ? $(obj).data("label-type") : "",
            radioLabel = $(obj).data("label") !== undefined ? $(obj).data("label") : "",
            radioLabelStyle = $(obj).data("label-style") !== undefined ? $(obj).data("label-style") : "";
        let radioIcon = "";

        if (radioLabelType === "icon") {
            radioStyle += " p-icon";
            radioIcon = $(obj).data("label-icon") !== undefined ? "<i class='icon " + $(obj).data("label-icon") + "'></i>" : "";
        } else if (radioLabelType === "image") {
            radioStyle += " p-image";
            radioIcon = $(obj).data("label-icon") !== undefined ? "<img src='" + $(obj).data("label-icon") + "'/>" : "";
        } else {
            radioStyle += " p-default";
            radioIcon = "";
        }
        if ($(obj).parent('.pretty').length < 1) {
            $(obj).wrap('<div class="pretty ' + radioStyle + '"></div>');
            $(obj).after('<div class="state ' + radioLabelStyle + '">' + radioIcon + '<label>' + radioLabel + '</label></div>');
        }
    });

    //Textarea Auto Height
    $("textarea[data-autoheight=true]").each(function (i,obj) {
        const shift = $(obj).data("autoheight-shift") !== undefined ? $(obj).data("autoheight-shift") : false;

        if (shift === true) {
            $(obj).keydown(function (e) {
                if (e.keyCode === 13 && !e.shiftKey) {
                    e.preventDefault();
                } else {
                    autosize($(obj));
                }
            });
        } else {
            autosize($(obj));
        }
    });

    //Jquery Validation Form
    $("form[data-service=ajax]").each(function (i, obj) {
        const formAction = $(obj).attr("action"),
            formMethod = $(obj)[0].method,
            formName = $(obj)[0].name,
            formTarget = $(obj).data("target"),
            formType = $(obj).data("type"),
            formRequest = $(obj).data("request"),
            formId = "#" + $(obj)[0].id,
            formModule = "#" + $(formId).parents(".mainContent")[0].id;

        $(obj).validate({
            submitHandler: function () {
                $(obj).find('[type=submit]').button('loading');
                const formData = $(obj).serialize();

                ajaxServices({
                    Module: formModule,
                    form: formId,
                    action: formAction,
                    method: formMethod,
                    target: formTarget,
                    request: formRequest,
                    type: formType,
                    data: formData
                });
            }
        });
    });
}

function loadDefaultSettings(){
    //Build App Info and Setting
    buildAppInfo();
    //Load Default Form Function
    contentAutoHeight({element: ".mainContent", scroll: true, drag: true, substract: 170, plugin: "slimScroll"});

}

$(function(){
    loadDefaultSettings();
});
