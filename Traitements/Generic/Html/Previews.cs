using System.Web;
using Admins.Traitements.Generic.Base;
using static Admins.Traitements.Generic.Html.Table;
using static Admins.Traitements.Generic.Util.Use;

namespace Admins.Traitements.Generic.Html
{
    public static class Previews
    {
        public static IHtmlString SimplePreview(BaseModel model)
        {
            var properties = model.Properties();
            var htmlString = string.Empty;
            
            foreach (var property in properties)
            {
                if(BaseModel.Hide(property))
                    continue;
                htmlString += Tr(Th(BaseModel.LabelChamp(property)) + Th(":") + Td(model.CustomValue(property, true)));
            }
            
            return new HtmlString(Tab(htmlString, ClTable));
        }
    }
}