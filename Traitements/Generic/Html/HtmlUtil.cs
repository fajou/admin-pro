namespace Admins.Traitements.Generic.Html
{
    public static class HtmlUtil
    {
        public static string Div(object content, string cl = "", string id = "")
        {
            return $"<div{Attr.Cl(cl)}{Attr.Id(id)}>{content}</div>";
        }
        public static string H1(object content, string cl = "", string id = "")
        {
            return $"<h1{Attr.Cl(cl)}{Attr.Id(id)}>{content}</h1>";
        }

        public static string Small(object content, string cl = "", string id = "")
        {
            return $"<small{Attr.Cl(cl)}{Attr.Id(id)}>{content}</small>";
        }

        public static string Section(object content, string cl = "", string id = "")
        {
            return $"<section{Attr.Cl(cl)}{Attr.Id(id)}>{content}</section>";
        }

        public static string Ol(object content, string cl = "", string id = "")
        {
            return $"<ol{Attr.Cl(cl)}{Attr.Id(id)}>{content}</ol>";
        }
        
        public static string Ul(object content, string cl = "", string id = "")
        {
            return $"<ul{Attr.Cl(cl)}{Attr.Id(id)}>{content}</ul>";
        }

        public static string Li(object content, string cl = "", string id = "")
        {
            return $"<li{Attr.Id(id)}{Attr.Cl(cl)}>{content}</li>";
        }

        public static string P(object content, string cl = "", string id = "")
        {
            return $"<p{Attr.Id(id)}{Attr.Cl(cl)}>{content}</p>";
        }
        
        public static string I(object content, string cl, string id = "")
        {
            return $"<i{Attr.Id(id)}{Attr.Cl(cl)}>{content}</i>";
        }
        
        public static string A(object content, string url, string cl = "", string id = "", string attrs = "")
        {
            return $"<a{Attr.Href(url)}{Attr.Cl(cl)}{Attr.Id(id)}{attrs}>{content}</a>";
        }
        
        public static string Span(object content, string cl = "", string id = "", string attrs = "")
        {
            return $"<span{Attr.Cl(cl)}{Attr.Id(id)}{attrs}>{content}</span>";
        }
    }
}