using System.Collections.Generic;
using Admins.Traitements.Generic.Util;
using static Admins.Traitements.Generic.Html.HtmlUtil;

namespace Admins.Traitements.Generic.Html
{
    public static class Input
    {
        public const string ClassName = "cl";

        /*public const string MenuParentName = "menu_";*/

        public const string IdName = "Id";

        public const string UrlNameApres = "UrlApres";

        public const string ChoixName = "Choix";

        public const string DateName = "Daty";

        public const string FileName = "Fichier";

        public const string BoxNameTitle = "BoxNameTitle";

        public const string BoxNameButton = "BoxNameButton";

        public const string PageIndex = "page";

        public const string ItemsMax = "items";

        public const string SearchName = "search";

        public const string MenuId = "menuId";

        public const string OrderByName = "OrderBy";

        public const string OrderTypeName = "OrderType";

        public const string DosName = "dos";

        private static string Select(string name, object value, string label = null, string cl = Use.ClFormControl,
            string id = null, string attrs = null, bool horiz = false)
        {
            var clHoriz = horiz ? Attr.Cl(Use.CONTROL_LABEL) : string.Empty;
            var title = string.IsNullOrEmpty(label) ? null : $"<label{Attr.For(id) + clHoriz}>{label}:</label>";
            var select = $"<select{Attr.Id(id) + Attr.Cl(cl) + Attr.Name(name)} {attrs}>{value}</select>";
            return horiz ? Div(title, Use.COL_SM_4) + Div(select, Use.COL_SM_8) : title + select;
        }

        private static string Option(string key, string value, bool selected = false)
        {
            return $"<option{Attr.Value(key) + Attr.Selected(selected)}>{value}</option>";
        }

        public static string Build(string type, string name, object value, string label = null,
            string cl = Use.ClFormControl, string id = null, string attrs = null, bool horiz = false)
        {
            var clHoriz = horiz ? Attr.Cl(Use.CONTROL_LABEL) : string.Empty;
            var title = string.IsNullOrEmpty(label) ? string.Empty : $"<label{Attr.For(id) + clHoriz}>{label}:</label>";
            var input = $"<input{Attr.Id(id) + Attr.Cl(cl) + Attr.Value(value, type) + Attr.Type(type) + Attr.Name(name)} {attrs}>";

            return horiz ? Div(title, Use.COL_SM_4) + Div(input, Use.COL_SM_8) : title + input;
        }

        public static string BuildWithBtn(string type, string name, object value, string bouttonHtml,
            string label = null, string id = null, string cl = Use.ClFormControl, string attrs = null, bool horiz = false)
        {
            var lbl = string.IsNullOrEmpty(label) ? null : $"<label{Attr.For(id)}>{label}:</label>";
            var input = $"<input{Attr.Id(id) + Attr.Cl(cl) + Attr.Value(value, type) + Attr.Type(type) + Attr.Name(name)} {attrs}>";

            return horiz
                ? Div(Div(lbl, "col-md-2") + Div(input, "col-md-8") + Div(bouttonHtml, "col-md-2"), "row form-group")
                : Div(Div(lbl, "col-md-12") + Div(input, "col-md-10") + Div(bouttonHtml, "col-md-2"), "row form-group");
        }

        public static string BuildFullOption(Dictionary<string, string> dicos, string name, string label = null, string selected = null, string cl = null, string id = null, string attrs = null, bool horiz = false)
        {
            var option = string.Empty;
            foreach (var dico in dicos)
                option += Option(dico.Key, dico.Value, Equals(dico.Key, selected));

            return Select(name, option, label, cl, id, attrs, horiz);
        }

        public static string BuildOption(Dictionary<string, string> dicos, string name, string label = null, string selected = null, string cl = null, string id = null, string attrs = null, bool horiz = false)
        {
            var option = Option(null, Use.OptionText);
            foreach (var dico in dicos)
                option += Option(dico.Key, dico.Value, Equals(dico.Key, selected));

            return Select(name, option, label, cl, id, attrs, horiz);
        }
    }
}