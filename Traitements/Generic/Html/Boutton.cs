namespace Admins.Traitements.Generic.Html
{
    public static class Boutton
    {
        public static string BouttonChoix(string id, string name, string url)
        {
            return $"<button{Attr.KeyValue("onclick",$"GetListe('{id}', '{name}', '{url}')")}type=\"button\" class=\"btn btn-default pull-right\" data-toggle=\"modal\" data-target=\"#modal-default\">...</button>";
        }

        public static string Build(string type, string text, string cl = "", string id = "")
        {
            return $"<button{Attr.Id(id)}{Attr.Type(type)}{Attr.Cl(cl)}>{text}</button>";
        }

        public static string BuildUrl(string url, string icon, string text, string cl = null)
        {
            return $"<a{Attr.Href(url)}{Attr.Cl(cl)}><i{Attr.Cl(icon)}></i>{text}</a>";
        }
    }
}    