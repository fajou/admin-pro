using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;
using Admins.Traitements.Generic.Context;
using Admins.Traitements.Generic.Util;
using Admins.Traitements.Services.Repository;
using static Admins.Traitements.Generic.Html.HtmlUtil;
using static Admins.Traitements.Generic.Util.Use;

namespace Admins.Traitements.Generic.Html
{
    public static class Table
    {
        public static string Th(object content, string cl = null, string id = null)
        {
            return $"<th{Attr.Id(id)}{Attr.Cl(cl)}>{content}</th>";
        }

        public static string Td(object content, string cl = null, string id = null)
        {
            return $"<td{Attr.Id(id)}{Attr.Cl(cl)}>{content}</td>";
        }

        public static string Tr(object content, string cl = null, string id = null)
        {
            return $"<tr{Attr.Id(id)}{Attr.Cl(cl)}>{content}</tr>";
        }

        public static string Tab(object content, string cl = null, string id = null)
        {
            return $"<table{Attr.Id(id)}{Attr.Cl(cl)}>{content}</table>";
        }

        private static string Head(IBaseModel bm, bool choix = false)
        {
            var infos = bm.Properties();
            var heads = bm.IsModelAction() ? Th("....") : string.Empty;
            heads += choix ? Th(Input.ChoixName) : string.Empty;
            
            foreach (var info in infos)
            {
                if (BaseModel.Hide(info))
                    continue;
                heads += Th(BaseModel.LabelChamp(info));
            }

            return Tr(heads, ClTr);
        }

        private static string Body(IReadOnlyList<BaseModel> modeles, bool choix = false)
        {
            var body = string.Empty;
            var typeBm = modeles[0].GetType();
            var editable = (ListEditable) typeBm.GetCustomAttribute(typeof(ListEditable));
            var deletable = (ListDeletable) typeBm.GetCustomAttribute(typeof(ListDeletable));
            var preview = (PreviewDetails) typeBm.GetCustomAttribute(typeof(PreviewDetails));
            
            foreach (var modele in modeles)
            {
                var properties = modele.Properties();
                var content = string.Empty;
                var action = string.Empty;
                
                if (preview != null)
                {
                    var typePreview = preview.TypeBaseModel ?? typeBm;
                    var url = UrlPreview(typePreview, modele.Id);
                    action += Boutton.BuildUrl(url, preview.Icon, string.Empty, preview.Class);
                }
                
                if (editable != null)
                {
                    var typeEditable = editable.TypeBaseModel ?? typeBm;
                    var url = UrlForm(typeEditable, modele.Id, MenusContext.Current);
                    action += Boutton.BuildUrl(url, editable.Icon, string.Empty, editable.Class);
                }

                if (deletable != null)
                {
                    var typeDeletable = deletable.TypeBaseModel ?? typeBm;
                    var url = UrlDelete(typeDeletable, modele.Id);
                    action += Boutton.BuildUrl(url, deletable.Icon, string.Empty, deletable.Class);
                }

                content += modeles[0].IsModelAction() ? Td(action) : string.Empty;
                content += choix ? Td(Input.Build("radio", modele.IdName(), modele.Id, null, "minimal", modele.Id)) : string.Empty;
                content = properties
                    .Where(field => !BaseModel.Hide(field))
                    .Aggregate(content, (current, field) => current + Td(modele.CustomValue(field, true)));

               
                body += Tr(content);
            }

            return body;
        }

        public static string Pagination(Type type, int index, int maxItems, int totalItems, bool onPage = true)
        {
            if (maxItems == 0 || totalItems <= maxItems)
                return string.Empty;

            index = 1 <= index ? index : 1;
            var listeLI = string.Empty;
            var page = totalItems / maxItems;
            if (0 < totalItems % maxItems)
                page++;

            var max = ConstInt.MaxPaging + index - 1;
            max = page < max ? page : max;
            var min = page <= ConstInt.MaxPaging ? 1 : max - ConstInt.MaxPaging + 1;
            var menu = MenusContext.Current;
            
            if (1 < index)
                listeLI += Li(A(Preve, onPage ? UrlPageListes(type, index - 1, maxItems, menu) : Scarp, attrs: !onPage
                    ? Attr.KeyValue("onclick", FuncPagingListeView(UrlListesView(type, index - 1, maxItems)))
                    : string.Empty));

            if (2 < min)
            {
                listeLI += Li(A(1, onPage ? UrlPageListes(type, 1, maxItems, menu) : Scarp, attrs: !onPage
                    ? Attr.KeyValue("onclick", FuncPagingListeView(UrlListesView(type, 1, maxItems)))
                    : string.Empty));

                listeLI += Li(A(More, onPage ? UrlPageListes(type, min - 1, maxItems, menu) : Scarp,
                    attrs: !onPage
                        ? Attr.KeyValue("onclick", FuncPagingListeView(UrlListesView(type, min - 1, maxItems)))
                        : string.Empty));
            }

            for (var i = min; i <= max; i++)
            {
                if (page < i) break;
                listeLI += Li(A(i, onPage ? UrlPageListes(type, i, maxItems, menu) : Scarp,
                    i == index ? $"{ClBlue}" : "", attrs: !onPage
                        ? Attr.KeyValue("onclick", FuncPagingListeView(UrlListesView(type, i, maxItems)))
                        : string.Empty));
            }

            if (max < page - 1)
            {
                listeLI += Li(A(More, onPage ? UrlPageListes(type, min + ConstInt.MaxPaging, maxItems, menu) : Scarp,
                    attrs: !onPage
                        ? Attr.KeyValue("onclick",
                            FuncPagingListeView(UrlListesView(type, min + ConstInt.MaxPaging, maxItems)))
                        : string.Empty));

                listeLI += Li(A(page, onPage ? UrlPageListes(type, page, maxItems, menu) : Scarp,
                    attrs: !onPage
                        ? Attr.KeyValue("onclick", FuncPagingListeView(UrlListesView(type, 1, maxItems)))
                        : string.Empty));
            }
            else if (max < page)
            {
                listeLI += Li(A(page, onPage ? UrlPageListes(type, page, maxItems, menu) : Scarp,
                    attrs: !onPage
                        ? Attr.KeyValue("onclick", FuncPagingListeView(UrlListesView(type, page, maxItems)))
                        : string.Empty));
            }

            if (index < page)
                listeLI += Li(A(Next, onPage ? UrlPageListes(type, index + 1, maxItems, menu) : Scarp,
                    attrs: !onPage
                        ? Attr.KeyValue("onclick", FuncPagingListeView(UrlListesView(type, index + 1, maxItems)))
                        : string.Empty));

            return Ul(listeLI, UlPaginationCl);
        }

        public static IHtmlString GetHead(BaseModel bm)
        {
            return new HtmlString(Head(bm));
        }

        public static IHtmlString GetBody(IReadOnlyList<BaseModel> modeles)
        {
            return new HtmlString(Body(modeles));
        }

        public static IHtmlString GetTable(List<BaseModel> modeles, bool choix = false, string cl = TableCl, string id = null)
        {
            var html = modeles.Count == 0
                ? Tab(P("La liste est vide", "text-center padding"), cl, id)
                : Tab(Head(modeles[0], choix) + Body(modeles, choix), cl, id);
            return new HtmlString(html);
        }
    }
}