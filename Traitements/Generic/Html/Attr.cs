using System;

namespace Admins.Traitements.Generic.Html
{
    public static class Attr
    {
        public static string KeyValue(string key, object value)
        {
            return $" {key}=\"{value}\"";
        }
        
        public static string Cl(string cl)
        {
            return string.IsNullOrEmpty(cl) ? "" : $" class=\"{cl}\"";
        }

        public static string Id(string id)
        {
            return string.IsNullOrEmpty(id) ? "" : $" id=\"{id}\"";
        }
        
        public static string For(string id)
        {
            return string.IsNullOrEmpty(id) ? "" : $" for=\"{id}\"";
        }
        
        public static string Href(string url)
        {
            return string.IsNullOrEmpty(url) ? "" : $" href=\"{url}\"";
        }
        
        public static string Src(string src)
        {
            return string.IsNullOrEmpty(src) ? "" : $" src=\"{src}\"";
        }

        public static string Name(string name)
        {
            return string.IsNullOrEmpty(name) ? "" : $" name=\"{name}\"";
        }

        public static string Type(string type)
        {
            return string.IsNullOrEmpty(type) ? "" : $" type=\"{type}\"";
        }

        public static string Selected(bool selected)
        {
            return selected ? " selected=\"selected\"" : "";
        }
        
        public static string Value(object value, string type = "")
        {
            if (string.IsNullOrEmpty(type)) 
                return $" value=\"{value}\"";
    
            var iCheck = type.Equals("checkbox", StringComparison.CurrentCultureIgnoreCase) || 
                         type.Equals("radio", StringComparison.CurrentCultureIgnoreCase);
            
            if (iCheck && Equals(value, true))
                return "checked";
            
            return $" value=\"{value}\"";
        }
    }
}