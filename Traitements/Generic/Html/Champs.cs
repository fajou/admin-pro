using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.HtmlControls;
using Admins.Traitements.Generic.Admins;
using Admins.Traitements.Generic.Algo.Implementation;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;
using Admins.Traitements.Generic.Context;
using Admins.Traitements.Generic.Search;
using Admins.Traitements.Generic.Util;
using Admins.Traitements.Services;
using Admins.Traitements.Services.Repository;
using static Admins.Traitements.Generic.Html.HtmlUtil;
using static Admins.Traitements.Generic.Html.Input;
using static Admins.Traitements.Generic.Util.Use;

namespace Admins.Traitements.Generic.Html
{
    public static class Champs
    {
        public static IHtmlString Build(BaseModel bm, bool update = false)
        {
            var div = update ? Input.Build(Hidden, IdName, bm.Id) : string.Empty;
            div += Input.Build(Hidden, ClassName, bm.GetType());

            div = bm.Properties().Aggregate(div, (current, property) => current + bm.BuildChamp(property, ClFormGroup,  false, true));
            return new HtmlString(div);
        }

        private static string OrderChamp(BaseModel model, string cl = null, string id = null)
        {
            //TODO : Filtrer les champs à ne pas rechercher ICI.
            var list = (Lists) model.GetType().GetCustomAttribute(typeof(Lists));
            if (list == null)
                return string.Empty;
            
            model.OrderBy.PropertyInfos = model.Properties();
            var optionColumn = BuildOption(model.OrderBy.DicoColumns(), OrderByName, list.OrderLabel, model.OrderBy.Columns, cl, id);
            var optionOders = BuildOption(OrderBy.DicoOrderType(), OrderTypeName, list.OrderTypeLabel, model.OrderBy.OrderType, cl, id);

            var html = Div(optionColumn, $"{ClFormGroup} {COL_XS_3}") + Div(optionOders, $"{ClFormGroup} {COL_XS_3}");
            return html;
        }

        public static MvcHtmlString ChampsItemsToShow(int items, string cl = null)
        {
            var dicoItems = new Dictionary<string, string>();

            for (int i = ConstInt.ItemMax; i <= 100; i += ConstInt.ItemMax)
            {
                dicoItems[i.ToString()] = i.ToString();
            }

            var itemsChamps = BuildFullOption(dicoItems, ItemsMax, ConstName.ITEMS_LABEL, items.ToString(), ClFormControl, null, null, true);
            return new MvcHtmlString(Div(itemsChamps, cl));
        }

        public static HtmlForm BuildSearch(BaseModel model)
        {
            var properties = model.Properties();
            var adds = Input.Build(Hidden, ClassName, model.GetType());
            adds += Input.Build(Hidden, SearchName, true);

            var html = properties.Aggregate(adds, (current, property) => current + model.BuildChamp(property, $"{ClFormGroup} {COL_XS_3}", true));

            html += OrderChamp(model, ClFormSelect2);
            return new HtmlForm
            {
                Action = URLPageListes,
                Method = MethodGET,
                InnerHtml = Div(html, Cl_ROW),
                DefaultButton = Boutton.Build("submit", "Rechercher", "btn btn-sm btn-primary btn-flat pull-left")
            };
        }

        private static string MenusContent(Menus menu)
        {
            var active = menu.IsActive ? ClActive : null;
            var keyValue = $"{Attr.KeyValue(KEY_DATA_URL, UrlMenu(menu))} {Attr.KeyValue(KEY_DATA_ID, $"{menu.Id}_Info_")} {Attr.KeyValue(KEY_DATA_DESC, menu.Label)}";
            //return Li(A($"{I(null, menu.IconClass)}{menu.Label}", UrlMenu(menu)), active);
            return Li(A($"{I(null, menu.IconClass)}{Span(menu.Label)}", null, ClSubMenu, menu.Id, keyValue), active);
        }

        private static string MenusParent(Menus parent, string menus)
        {
            var cl = string.Empty;
            var menu = I(null, parent.IconClass);
            menu += Span(parent.Label);
            if (Equals("#", parent.Url))
            {
                cl = ClTreeView;
                /*var i = I(null, "fa fa-angle-left pull-right");*/
                menu += Span(null /*i*/, "pull-right-container");
            }

            menu = A(menu, UrlMenu(parent));
            menu += menus;

            var clActive = parent.IsActive ? cl + " " + ClActive : cl;
            return Li(menu, clActive);
        }

        private static string MenusContent(IEnumerable<Menus> parents, IGraph<Menus> graph)
        {
            var menus = string.Empty;
            foreach (var parent in parents)
            {
                var fils = graph.NextsNodes(parent);
                if (fils == null || !fils.Any())
                    menus += MenusContent(parent);
                else
                    menus += MenusParent(parent, MenusContent(fils, graph));
            }

            return menus.Equals(string.Empty) ? string.Empty : Ul(menus, ClTreeViewMenu);
        }

        private static List<Menus> BreadCrumb(string menuId)
        {
            if(MenusContext.MenuGraph == null)
                return new List<Menus>();

            var current = MenusContext.MenuGraph.Nodes().FirstOrDefault(m => Equals(m.Id, menuId));
            if(current == null)
                return new List<Menus>();

            MenusContext.Current = current;
            current.IsActive = true;
            var parents = MenusContext.MenuGraph.PrevesNode(current);
            var crumbs = new List<Menus> {current};

            while (parents != null && parents.Length != 0)
            {
                current = parents.First(node => Equals(node.Node.Id, current.IdParent)).Node;
                parents = MenusContext.MenuGraph.PrevesNode(current);
                current.IsActive = true;
                crumbs.Insert(0, current);
            }

            return crumbs;
        }
        
        public static HtmlString BuildMenus(string menuId, int level = ConstInt.LevelsMax, string fileApp = null)
        {
            IMenusService service = new MenusService(Dbo);
            MenusContext.MenuGraph = service.GraphMenus(level);
            MenusContext.BreadCrumb = BreadCrumb(menuId ?? MenusContext.Current?.Id);

            var parents = MenusContext.MenuGraph.Nodes().Where(node => MenusContext.MenuGraph.PrevesNode(node) == null);
            var menus = string.Empty;

            foreach (var parent in parents)
            {
                var fils = MenusContext.MenuGraph.NextsNodes(parent);
                if (fils == null || !fils.Any())
                    menus += MenusParent(parent, null);
                else
                    menus += MenusParent(parent, MenusContent(fils, MenusContext.MenuGraph));
            }

            if (!string.IsNullOrEmpty(fileApp)) 
                service.GenerateAppJson(fileApp);
            
            return new HtmlString(menus);
        }

        public static HtmlString BuildBreadCrumb(string menuId = null, bool inTab = false)
        {
            var crumbs = MenusContext.BreadCrumb = BreadCrumb(menuId ?? MenusContext.Current?.Id);
            if(crumbs == null || crumbs.Count == 0)
                return new HtmlString(string.Empty);

            var breadCrumbs = string.Empty;
            var n = crumbs.Count;

            for (var i = 0; i < n; i++)
            {
                var active = i == n - 1 ? ClActive : null;
                breadCrumbs += $"{Li(A(I(string.Empty, crumbs[i].IconClass) + " " + crumbs[i].Label, UrlMenu(crumbs[i])), active)}";
            }

            var valueH1 = n == 1 ? crumbs[0].Label : $"{crumbs[0].Label} : {Small(crumbs[n - 1].Label)}";
            var bread = H1(valueH1) + Ol(breadCrumbs, CL_BREADCRUMB);

            var cl = inTab ? $"{CL_CONTENT_HEADER} {CL_CONTENT_HEADER_TAB}" : CL_CONTENT_HEADER;
            return new HtmlString(Section(bread, cl));
        }
    }
}