using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using Admins.Traitements.Services.Repository;

namespace Admins.Traitements.Generic.Base
{
    public class BaseXml : IBaseXml
    {
        private static BaseXml SerealizeBaseXml(string xmlText)
        {
            using(var stringReader = new StringReader(xmlText))
            {
                var serializer = new XmlSerializer(typeof(BaseXml));
                return serializer.Deserialize(stringReader) as BaseXml;
            }
        }
        public IBaseXml ReadBaseXml(string path)
        {
            var documentXml = new XmlDocument();
            documentXml.Load(path);
            return SerealizeBaseXml(documentXml.ToString());
        }

        public string ToXml()
        {
            throw new NotImplementedException();
        }

        public void SaveToXml(IBaseXml baseXml, string fileName)
        {
            using(var stringwriter = new StreamWriter(fileName))
            {
                var serializer = new XmlSerializer(baseXml.GetType());
                serializer.Serialize(stringwriter, baseXml);
                stringwriter.Close();
            }
        }
    }
}