using System;
using Admins.Traitements.Generic.Search;

namespace Admins.Traitements.Generic.Base
{
    public static class Query
    {
        public const string OR = "OR";
        public const string AND = "AND";
        public const string LIKE = "LIKE";
            
        public static string Paging(int index, int items, string order = OrderBy.ID_DESC)
        {
            if (string.IsNullOrEmpty(order))
                order = OrderBy.ID_DESC;
            
            if (index < 0 || items < 1)
                return "";
            
            var offset = (index - 1) * items;
            return $"order by {order} offset {offset} rows fetch next {items} rows only";
        }
        
        public static string SearchQuery(BaseModel model, int index = 0, int items = 0, string order = OrderBy.ID_DESC)
        {
            var properties = model.SearchFields;
            var values = string.Empty;
            var cond = model.IsAnd ? AND : OR;
            
            foreach (var info in properties)
            {
                var val = info.Value;
                if (info.PropertyInfo.PropertyType == typeof(string)) 
                    values += $"{info.PropertyInfo.Name} {LIKE} '%{val}%' {cond} ";
                
                else if(info.PropertyInfo.PropertyType == typeof(DateTime) || info.PropertyInfo.PropertyType == typeof(bool))
                    values += $"{info.PropertyInfo.Name} = '{val}' {cond} ";
                
                else
                    values += $"{info.PropertyInfo.Name} = {val} {cond} ";
            }
            
            return $"{values} 1=1 {Paging(index, items, order)}";
        }
    }
}