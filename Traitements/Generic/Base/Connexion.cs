﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using Admins.Traitements.Generic.Util;

namespace Admins.Traitements.Generic.Base
{
    internal static class Connexion
    {
        public static string InitialCatalog;
        private static readonly string StringConnection = ConfigurationManager.ConnectionStrings[Use.ACS].ConnectionString;

        public static void Close(SqlDataReader reader, SqlConnection connection)
        {
            reader?.Close();
            connection?.Close();
        }

        public static SqlConnection GetConnection()
        {
            try
            {
                var builder = new SqlConnectionStringBuilder(StringConnection);
                if (string.IsNullOrEmpty(InitialCatalog))
                    InitialCatalog = builder.InitialCatalog;
                else
                    builder.InitialCatalog = InitialCatalog;

                var conn = new SqlConnection(builder.ConnectionString);
                conn.Open();
                return conn;
            }
            catch (Exception e)
            {
                throw new Exception("Error de la connection. Cause :" + e.Message);
            }
        }

        public static SqlDataReader ResultSet(string query, SqlConnection con)
        {
            Console.WriteLine(query);
            var result = new SqlCommand(query, con).ExecuteReader();
            return result;
        }

        public static void Execute(string query, SqlConnection connection)
        {
            try
            {
                var cmd = new SqlCommand(query, connection);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception("Error lie à la Table Cause : " + ex.Message);
            }
        }

        public static void Execute(string query)
        {
            SqlConnection connection = null;
            try
            {
                connection = GetConnection();
                Execute(query, connection);
            }
            catch (Exception ex)
            {
                throw new Exception("Error lie à la Table Cause : " + ex.Message);
            }
            finally
            {
                connection?.Close();
            }
        }
    }
}