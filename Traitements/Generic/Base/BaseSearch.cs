using System.Collections.Generic;
using Admins.Traitements.Generic.Search;

namespace Admins.Traitements.Generic.Base
{
    public abstract class BaseSearch : BaseForms
    {
        public bool IsAnd { get; set; } = true;

        public bool InSearch { get; set; }
        
        public OrderBy OrderBy { get; set; } = new OrderBy();
        
        public List<SearchField> SearchFields { get; set; } = new List<SearchField>();
    }
}