﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Admins.Traitements.Services.Repository;

namespace Admins.Traitements.Generic.Base
{
    internal class GenericDBO : IDataBaseModel
    {
        public static string QueryDelete(BaseModel bm)
        {
            var query = $"DELETE {bm.GetTableName()} WHERE ID = '{bm.Id}'";
            Console.WriteLine(query);
            return query;
        }

        public static string QueryDeleteAll(BaseModel bm)
        {
            var query = $"DELETE {bm.GetTableName()}";
            Console.WriteLine(query);
            return query;
        }

        public static string QueryList(BaseModel bm, string where)
        {
            where = string.IsNullOrEmpty(where) ? "" : $"WHERE {where}";

            var query = $"SELECT * FROM {bm.GetTableName()} {where}";
            Console.WriteLine(query);
            return query;
        }

        public static string QuerySave(BaseModel bm)
        {
            var properties = bm.TableColumnProperties();
            var nomColone = "";
            var valueColone = "";
            foreach (var property in properties)
            {
                nomColone += $"{property.Name}, ";
                valueColone += $"@{property.Name}, ";
            }

            nomColone = nomColone.Remove(nomColone.Length - 2);
            valueColone = valueColone.Remove(valueColone.Length - 2);
            var query = $"INSERT INTO {bm.GetTableName()} (Id, {nomColone}) VALUES (@Id, {valueColone})";
            Console.WriteLine(query);
            return query;
        }

        public static string QueryUpdate(BaseModel bm)
        {
            var propertyInfos = bm.TableColumnProperties();
            var valueColonne = propertyInfos.Aggregate("",
                (current, property) => current + $"{property.Name} = @{property.Name}, ");

            valueColonne = valueColonne.Remove(valueColonne.Length - 2);
            var query = $"UPDATE {bm.GetTableName()} SET {valueColonne}  WHERE ID = '{bm.Id}'";
            Console.WriteLine(query);
            return query;
        }

        public void Delete(BaseModel bm)
        {
            SqlConnection scon = null;
            try
            {
                scon = Connexion.GetConnection();
                Delete(bm, scon);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                Connexion.Close(null, scon);
            }
        }

        public void DeleteAll(BaseModel bm, SqlConnection conn)
        {
            Connexion.Execute(QueryDeleteAll(bm), conn);
        }

        public void DeleteAll(BaseModel bm)
        {
            SqlConnection scon = null;
            try
            {
                scon = Connexion.GetConnection();
                DeleteAll(bm, scon);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                Connexion.Close(null, scon);
            }
        }

        public void Delete(BaseModel bm, SqlConnection conn)
        {
            Connexion.Execute(QueryDelete(bm), conn);
        }

        public void DeleteList(List<BaseModel> bm)
        {
            SqlConnection scon = null;
            try
            {
                scon = Connexion.GetConnection();
                DeleteList(bm, scon);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                Connexion.Close(null, scon);
            }
        }

        public void DeleteList(List<BaseModel> bms, SqlConnection conn)
        {
            if (bms == null || bms.Count == 0) return;

            foreach (var bm in bms)
            {
                Connexion.Execute(QueryDelete(bm), conn);
            }
        }

        public void Save(BaseModel bm)
        {
            SqlConnection connection = null;
            try
            {
                connection = Connexion.GetConnection();
                Save(bm, connection);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                Connexion.Close(null, connection);
            }
        }

        private static string GetRefTable(IBaseModel bm)
        {
            return bm.PredicasName();
        }

        private static string GetSeq(IBaseModel bm)
        {
            return bm.SequenceName();
        }

        public void Save(BaseModel bm, SqlConnection conn)
        {
            bm.SetId(GetRefTable(bm), GetSeq(bm), conn);
            var cmd = new SqlCommand(QuerySave(bm), conn);
            var properties = bm.TableColumnProperties();
            cmd.Parameters.Add(new SqlParameter("@Id", bm.Id));
            foreach (var property in properties)
            {
                var o = property.GetValue(bm);
                o = o ?? DBNull.Value;
                cmd.Parameters.Add(new SqlParameter(property.Name, o));
            }

            cmd.ExecuteNonQuery();
            cmd.Dispose();
        }

        public void Save(BaseModel bm, SqlConnection conn, SqlTransaction tr)
        {
            bm.SetId(GetRefTable(bm), GetSeq(bm), conn);
            var cmd = new SqlCommand(QuerySave(bm), conn) {Transaction = tr};
            var properties = bm.TableColumnProperties();
            cmd.Parameters.Add(new SqlParameter("@Id", bm.Id));
            foreach (var property in properties)
            {
                var o = property.GetValue(bm);
                o = o ?? DBNull.Value;
                cmd.Parameters.Add(new SqlParameter(property.Name, o));
            }

            cmd.ExecuteNonQuery();
            cmd.Dispose();
        }


        public void SaveList(List<BaseModel> modeles)
        {
            SqlConnection conn = null;
            try
            {
                conn = Connexion.GetConnection();
                SaveList(modeles, conn);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                Connexion.Close(null, conn);
            }
        }

        public void SaveList(List<BaseModel> modeles, SqlConnection conn)
        {
            if (modeles != null && modeles.Count != 0)
            {
                var sq = new SqlCommand(QuerySave(modeles[0]), conn);
                foreach (var modele in modeles)
                {
                    var properties = modele.TableColumnProperties();
                    modele.SetId(GetRefTable(modele), GetSeq(modele), conn);
                    sq.Parameters.Add(new SqlParameter("@Id", modele.Id));
                    foreach (var property in properties)
                    {
                        var o = property.GetValue(modele);
                        o = o ?? DBNull.Value;
                        sq.Parameters.Add(new SqlParameter(property.Name, o));
                    }

                    sq.ExecuteNonQuery();
                    sq.Parameters.Clear();
                }

                sq.Dispose();
            }
            else
            {
                throw new Exception("erreur save List");
            }
        }

        public void Update(BaseModel bm)
        {
            SqlConnection con = null;
            try
            {
                con = Connexion.GetConnection();
                Update(bm, con);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                Connexion.Close(null, con);
            }
        }


        public void Update(BaseModel bm, SqlConnection connection)
        {
            var propertyInfos = bm.TableColumnProperties();
            var sqlm = new SqlCommand(QueryUpdate(bm), connection);
            foreach (var property in propertyInfos)
            {
                var o = property.GetValue(bm);
                o = o ?? DBNull.Value;
                sqlm.Parameters.Add(new SqlParameter(property.Name, o));
            }

            sqlm.ExecuteNonQuery();
            sqlm.Dispose();
        }

        public void UpdateList(List<BaseModel> bm)
        {
            SqlConnection con = null;
            try
            {
                con = Connexion.GetConnection();
                UpdateList(bm, con);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                Connexion.Close(null, con);
            }
        }

        public void UpdateList(List<BaseModel> bms, SqlConnection con)
        {
            if (bms == null || bms.Count == 0) return;

            var sqlm = new SqlCommand(QueryUpdate(bms[0]), con);
            foreach (var bm in bms)
            {
                var propertyInfos = bm.TableColumnProperties();
                foreach (var property in propertyInfos)
                {
                    var o = property.GetValue(bm);
                    o = o ?? DBNull.Value;
                    sqlm.Parameters.Add(new SqlParameter(property.Name, o));
                }

                sqlm.ExecuteNonQuery();
                sqlm.Parameters.Clear();
            }

            sqlm.Dispose();
        }

        public List<BaseModel> Find(BaseModel bm, SqlConnection conn, string where)
        {
            SqlDataReader sqd = null;
            var lmModeles = new List<BaseModel>();
            try
            {
                var properties = bm.TableColumnProperties();
                Count(bm, conn);
                sqd = Connexion.ResultSet(QueryList(bm, where), conn);

                while (sqd.Read())
                {
                    var modele = (BaseModel) Activator.CreateInstance(bm.GetType());
                    modele.Id = sqd["Id"].ToString();
                    foreach (var property in properties)
                    {
                        if (sqd[property.Name].GetType() != typeof(DBNull))
                            property.SetValue(modele, sqd[property.Name]);
                    }

                    lmModeles.Add(modele);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                Connexion.Close(sqd, null);
            }

            return lmModeles;
        }

        public List<BaseModel> FindQuery(string requette, BaseModel bm, SqlConnection conn)
        {
            SqlDataReader sqd = null;
            var lmModeles = new List<BaseModel>();
            try
            {
                var properties = bm.TableColumnProperties();
                sqd = Connexion.ResultSet(requette, conn);
                Console.WriteLine(requette);

                while (sqd.Read())
                {
                    var modele = (BaseModel) Activator.CreateInstance(bm.GetType());
                    modele.Id = sqd["Id"].ToString();
                    foreach (var property in properties)
                    {
                        if (sqd[property.Name].GetType() != typeof(DBNull))
                            property.SetValue(modele, sqd[property.Name]);
                    }

                    lmModeles.Add(modele);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                Connexion.Close(sqd, null);
            }

            return lmModeles;
        }

        public List<BaseModel> Find(BaseModel bm, string where = "")
        {
            SqlConnection conn = null;
            try
            {
                conn = Connexion.GetConnection();
                return Find(bm, conn, where);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                throw;
            }
            finally
            {
                Connexion.Close(null, conn);
            }
        }

        public List<BaseModel> FindQuery(string requette, BaseModel bm)
        {
            SqlConnection conn = null;
            try
            {
                conn = Connexion.GetConnection();
                return FindQuery(requette, bm, conn);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                Connexion.Close(null, conn);
            }
        }

        public void Count(BaseModel bm, SqlConnection conn)
        {
            SqlDataReader reader = null;
            try
            {
                var where = bm.InSearch ?  $"where {Query.SearchQuery(bm)}" : string.Empty;
                var requette = $"SELECT count(*) as Nbrs FROM {bm.GetTableName()} {where}";
                Console.WriteLine(requette);
                reader = Connexion.ResultSet(requette, conn);

                if(reader.Read())
                    bm[nameof(bm.Size)] = reader["Nbrs"];
            }
            finally
            {
                reader?.Close();
            }
        }

        public BaseModel First(BaseModel model)
        {
            return FindQuery($"SELECT TOP 1 * FROM {model.GetTableName()}", model).FirstOrDefault();
        }

        public BaseModel FindById(BaseModel baseModel, string id)
        {
            var bms = FindQuery($"SELECT TOP 1 * FROM {baseModel.GetTableName()} WHERE ID='{id}'", baseModel);
            return bms.Count != 0 ? bms[0] : null;
        }
    }
}