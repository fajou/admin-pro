using Admins.Traitements.Generic.Admins;

namespace Admins.Traitements.Generic.Base
{
    public abstract class BaseForms
    {
        public abstract string UrlAfter(Menus menu = null);
    }
}