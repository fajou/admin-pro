﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using Admins.Traitements.Generic.Admins;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Html;
using Admins.Traitements.Generic.Util;
using Admins.Traitements.Services.Repository;
using static Admins.Traitements.Generic.Util.Use;
using static Admins.Traitements.Generic.Html.HtmlUtil;

namespace Admins.Traitements.Generic.Base
{
    public abstract class BaseModel : BaseSearch, IBaseModel
    {
        public string Id { set; get; }

        public int Rang { get; set; }
        
        public int Size { get; set; } = 0;

        public override string UrlAfter(Menus menus = null)
        {
            var type = GetType();
            var forms = (Forms) type.GetCustomAttribute(typeof(Forms));
            if (forms == null) 
                return UrlPageListes(type);
            
            return forms.SaveAndCreateAnOther ? UrlForm(type, Id, menus) : UrlPageListes(type, 1, ConstInt.ItemMax, menus);
        }

        public PropertyInfo[] Properties()
        {
            var properties = GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
            return (
                from property in properties
                where !(property.GetCustomAttribute(typeof(NotColumn)) != null && property.GetCustomAttribute(typeof(Champ)) == null)
                select property
            ).ToArray();
        }

        public PropertyInfo[] TableColumnProperties()
        {
            var properties = Properties();
            return (
                from property in properties
                where property.GetCustomAttribute(typeof(NotColumn)) == null
                select property
            ).ToArray();
        }

        public static bool Hide(PropertyInfo info)
        {
            return info.GetCustomAttribute(typeof(Hide)) != null;
        }

        public static bool HideInSearch(PropertyInfo property)
        {
            var hide = (Hide) property.GetCustomAttribute(typeof(Hide));
            return hide != null && hide.SearchInput;
        }
        
        public string IdName()
        {
            return $"Id{GetType().Name}";
        }

        public string IdChart(string unique)
        {
            return $"CHART_{unique}";
        }

        public string BaseTypeName()
        {
            return GetType().Name;
        }

        public string GetTableName()
        {
            var table = (TableInfo) GetType().GetCustomAttribute(typeof(TableInfo));
            return table == null ? BaseTypeName() : table.Name;
        }

        public string SequenceName()
        {
            var table = (TableInfo) GetType().GetCustomAttribute(typeof(TableInfo));
            return table == null ? $"id{GetTableName()}".ToLower() : table.Sequence;
        }

        public string PredicasName()
        {
            var table = (TableInfo) GetType().GetCustomAttribute(typeof(TableInfo));
            if (table != null/* && !string.IsNullOrWhiteSpace(table.Predicas)*/)
                return table.Predicas;

            var nomtable = GetTableName();
            return nomtable.Length <= 4 ? nomtable.ToUpper() : nomtable.Substring(0, 4).ToUpper();
        }

        public string BoxTitle()
        {
            var type = GetType();
            var forms = (Forms) type.GetCustomAttribute(typeof(Forms));
            if (forms == null) return type.Name;
            
            var title = string.IsNullOrEmpty(Id)
                ? forms.InsertTitle ?? forms.DefaultTitle
                : forms.UpdateTitle ?? forms.DefaultTitle;
                
            return string.IsNullOrEmpty(title) ? type.Name : title;
        }

        public string PreviewTitle()
        {
            var type = GetType();
            var details = (PreviewDetails) type.GetCustomAttribute(typeof(PreviewDetails));
            if (details == null) return $"Details {type.Name}";

            return details.BoxTitle ?? $"Details {type.Name}";
        }


        public string ListBoxTitle()
        {
            var type = GetType();
            var box = (Lists) type.GetCustomAttribute(typeof(Lists));
            return box != null ? box.Title : $"Liste {GetType().Name}";
        }

        public object CustomValue(PropertyInfo property, bool numberFormater = false, bool searchField = false, bool inputFeald = false)
        {
            var value = property.GetValue(this, null);
            if (property.PropertyType == typeof(DateTime))
            {
                var date = (DateTime) value;
                if (searchField)
                    return DateTime.MinValue == date ? DateTime.Now.ToString("yyyy-MM-dd") : date.ToString("yyyy-MM-dd");

                var format = (DateTimeFormat) property.GetCustomAttribute(typeof(DateTimeFormat));
                return format == null ? value : date.ToString(format.Format);
            }

            if (property.PropertyType != typeof(decimal))
                return value;
            {
                var val = (decimal) value;
                if (val == 0 && searchField)
                    return null;

                if (inputFeald)
                    return FormatMGA(val, true);
                
                if (!numberFormater)
                    return val;
                
                var format = (NumberFormat) property.GetCustomAttribute(typeof(NumberFormat));
                return format == null ? value : FormatMGA(val);
            }
        }

        public string BuildChamp(PropertyInfo property, string cl = ClFormGroup, bool searchFeald = false, bool inputFeald = false)
        {
            var isHForms = IsHorizontalForms();
            var champ = (Champ) property.GetCustomAttribute(typeof(Champ));
            if (champ == null || searchFeald && HideInSearch(property))
                return string.Empty;


            var name = NameChamp(property);
            var label = LabelChamp(property);
            
            if (champ.Choix)
            {
                var model = (BaseModel) Activator.CreateInstance(champ.TypeModel);
                var boutton = Boutton.BouttonChoix($"ID_{name}", model.IdName(), UrlListesView(champ.TypeModel));

                return Div(Input.BuildWithBtn(champ.Type, name, CustomValue(property, false, searchFeald, inputFeald), boutton, label, $"ID_{name}", champ.Class,null, isHForms), cl);;
            }

            if (champ.SelectDB)
            {
                var model = (BaseModel) Activator.CreateInstance(champ.TypeModel);
                var dico = Dbo.Find(model).ToDictionary(m => $"{m[champ.Option]}", m => $"{m[name]}");
                var key = $"{this[property.Name]}";

                return Div(Input.BuildOption(dico, name, label, key, champ.Class, champ.Id, null, isHForms), cl);
            }

            if (champ.SelectBM)
            {
                var model = (BaseModel) Activator.CreateInstance(champ.TypeModel);
                var dico = model.DicoForOption();
                var key = $"{this[property.Name]}";

                return Div(Input.BuildOption(dico, name, label, key, champ.Class, champ.Id, null, isHForms), cl);
            }

            var isFileType = Equals(Champ.FileIdentifier, champ.Type);
            var attrs = isFileType ? Attr.KeyValue("accept", champ.Accept) + " " + Attr.KeyValue("onchange", FuncImageUploader("this", BaseTypeName())) : null;
            var value = CustomValue(property, false, searchFeald, inputFeald);
            
            return Div(Input.Build(champ.Type, name, value, label, champ.Class, champ.Id, attrs, isHForms), cl);

        }

        public virtual Dictionary<string, string> DicoForOption()
        {
            throw new NotImplementedException();
        }

        public bool IsModelAction()
        {
            var attributes = GetType().GetCustomAttributes();
            return attributes.Select(attribute => attribute.GetType()).Any(type =>
                type == typeof(ListEditable) || type == typeof(ListDeletable) || type == typeof(PreviewDetails));
        }

        public bool IsDeletable()
        {
            return GetType().GetCustomAttribute(typeof(ListDeletable)) != null;
        }

        public bool IsEditable()
        {
            return GetType().GetCustomAttribute(typeof(ListEditable)) != null;
        }

        public bool IsSaveAndCreateAnOther()
        {
            var forms = (Forms) GetType().GetCustomAttribute(typeof(Forms));
            return forms != null && forms.SaveAndCreateAnOther;
        }

        public bool IsHorizontalForms()
        {
            var forms = (Forms) GetType().GetCustomAttribute(typeof(Forms));
            return forms != null && forms.HorizontalForms;
        }

        public Type TypeDeletable()
        {
            var baseType = GetType();
            var deletable = (ListDeletable) baseType.GetCustomAttribute(typeof(ListDeletable));
            return deletable?.TypeBaseModel ?? baseType;
        }

        public Type TypeEditable()
        {
            var baseType = GetType();
            var deletable = (ListEditable) baseType.GetCustomAttribute(typeof(ListEditable));
            return deletable?.TypeBaseModel ?? baseType;
        }

        public static Dictionary<string, string> DicoForOption(string attrValue, IEnumerable<BaseModel> modeles)
        {
            return modeles.ToDictionary(m => m.Id, m => $"{m[attrValue]}");
        }

        public static string NameChamp(PropertyInfo property)
        {
            var champ = (Champ) property.GetCustomAttribute(typeof(Champ));
            return champ?.Name ?? property.Name;
        }

        public static string LabelChamp(PropertyInfo property)
        {
            var champ = (Champ) property.GetCustomAttribute(typeof(Champ));
            return champ?.Label ?? property.Name;
        }
        
        public object this[string propertyName]
        {
            get
            {
                var properties = GetType().GetProperty(propertyName);
                return properties?.GetValue(this, null);
            }
            set
            {
                var properties = GetType().GetProperty(propertyName);
                if (properties == null) return;
                    
                properties.SetValue(this, Convert.ChangeType(value, properties.PropertyType), null);
            }
        }
        
        public void SetId(string refTable, string seq, SqlConnection conn)
        {
            SqlDataReader data = null;
            try
            {
                var requete = $"select next value for {seq}";
                data = Connexion.ResultSet(requete, conn);
                if (data.Read())
                {
                    Id = refTable + data[0];
                    Rang = Convert.ToInt32(data[0]);
                }
                else
                    throw new Exception($"sequence {seq} n'existe pas");
            }
            finally
            {
                Connexion.Close(data, null);
            }
        }

        public void SetId(string refTable, string seq)
        {
            SqlConnection conn = null;
            try
            {
                SetId(refTable, seq, conn = Connexion.GetConnection());
            }
            finally
            {
                Connexion.Close(null, conn);
            }
        }

        public Type GetBaseModelForms()
        {
            var forms = (Forms) GetType().GetCustomAttribute(typeof(Forms));

            if (forms != null)
                return forms.TypeBaseModel != null ? forms.TypeBaseModel : GetType();
            
            return GetType();
        }
    }
}