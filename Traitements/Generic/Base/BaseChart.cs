using System;
using System.Collections.Generic;
using System.Reflection;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Attributes.Chart;
using Admins.Traitements.Generic.Util;
using Admins.Traitements.Services.Repository;

namespace Admins.Traitements.Generic.Base
{
    [HideMenuAcces]
    public abstract class BaseChart : BaseModel, IBaseChart
    {
        private const string BackGroundDefault = "rgba(201, 203, 207, 0.6)";
        private const string BorderColorDefault = "rgb(201, 203, 207)";

        public bool Fill { get; set; }
        public object Data { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public object Labels { get; set; }
        public object BackgroundColors { get; set; }
        public object BorderColors { get; set; }
        public int BorderWidth { get; set; }

        public object DataChart(string format = null)
        {
            SetChar();
            SetChartData();
            return new
            {
                type = Type,
                data = new
                {
                    labels = Labels,
                    total = Use.FormatMGA(TotalDefault()),
                    datasets = new[]
                    {
                        new
                        {
                            label = Title,
                            data = Data,
                            fill = Fill,
                            backgroundColor = BackgroundColors,
                            borderColor = BorderColors,
                            borderWidth = BorderWidth
                        }
                    }
                },
                options = new
                {
                    //find chartjs. plugins
                }
            };
        }

        public abstract decimal TotalDefault();

        private void SetChar()
        {
            var chartType = (ChartType) GetType().GetCustomAttribute(typeof(ChartType));
            if (chartType == null) return;
            
            Fill = chartType.Fill;
            Type = chartType.Type;
            Title = chartType.Title ?? BaseTypeName();
            BorderWidth = chartType.BorderWith;
        }

        private void SetChartData()
        {
            var labels = new List<string>();
            var values = new List<object>();
            var backgroundColors = new List<string>();
            var borderColors = new List<string>();

            var poperties = Properties();
            foreach (var info in poperties)
            {
                if (Hide(info))
                    continue;

                var chart = (ChartInfo) info.GetCustomAttribute(typeof(ChartInfo));
                if (chart == null)
                    continue;

                values.Add(CustomValue(info));
                labels.Add(chart.Label ?? info.Name);

                borderColors.Add(chart.BorderColor);
                backgroundColors.Add(chart.BackgroundColor);

                /*labels.Add(info.Name);
                borderColors.Add(BorderColorDefault);
                backgroundColors.Add(BackGroundDefault);*/
            }

            Data = values.ToArray();
            Labels = labels.ToArray();
            BackgroundColors = backgroundColors.ToArray();
            BorderColors = borderColors.ToArray();
        }

        private void SetOption()
        {
            throw new NotImplementedException();
        }
    }
}