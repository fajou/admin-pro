using Admins.Traitements.Generic.Base;

namespace Admins.Traitements.Generic.Admins.PDF
{
    public class ExportData
    {
        private BaseModel _baseModel;
        private string _fileType;
        private string _baseURLHtml;

        

        public BaseModel BaseModel
        {
            get { return _baseModel; }
            set { _baseModel = value; }
        }

        public string FileType
        {
            get { return _fileType; }
            set { _fileType = value; }
        }

        public string BaseUrlHtml
        {
            get { return _baseURLHtml; }
            set { _baseURLHtml = value; }
        }
    }
}