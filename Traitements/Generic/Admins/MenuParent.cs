using System.Collections.Generic;
using System.Linq;
using Admins.Traitements.Generic.Admins.Dicos;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;
using Admins.Traitements.Generic.Util;

namespace Admins.Traitements.Generic.Admins
{
    [HideMenuAcces]
    public class MenuParent : BaseModel
    {
        public override Dictionary<string, string> DicoForOption()
        {
            return Use.Dbo.Find(new Menus(), $"TypeMenu={TypeMenus.Parent} and Url='#'")
                .ToDictionary(modele => modele.Id, modele => $"{modele[nameof(Menus.Label)]}");
        }
    }
}