namespace Admins.Traitements.Generic.Admins.Json
{
    public class ModuleJsonInfo
    {
        public string ModuleName { get; set; }
        
        public object ModuleData { get; set; } 
    }
}