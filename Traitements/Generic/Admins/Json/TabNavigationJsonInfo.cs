namespace Admins.Traitements.Generic.Admins.Json
{
    public class TabNavigationJsonInfo
    {
        public string TabDesc { get; set; }
        
        public string TabIcon { get; set; }
        
        public string TabId { get; set; }
        
        public string TabIndex { get; set; }
        
        public string TabStatus { get; set; }
        
        public string TabUrl { get; set; }
        
        public string TabTitle { get; set; }
        
        public string TabIdContent => $"{TabId}Content";
    }
}