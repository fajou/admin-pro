namespace Admins.Traitements.Generic.Admins.Json
{
    public class MenuJsonInfo
    {
        public string MainMenuId { get; set; }
        
        public string MainMenuTitle { get; set; }
        
        public string MainMenuIcon { get; set; }
        
        public string MainMenuUrl { get; set; }
        
        public string MainMenuDesc { get; set; }
        
        public bool MainMenuChild { get; set; }
        
        public object MainMenuContent { get; set; }
    }
}