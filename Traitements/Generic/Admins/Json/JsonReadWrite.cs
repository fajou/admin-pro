using System.IO;

namespace Admins.Traitements.Generic.Admins.Json
{
    public static class JsonReadWrite
    {
        public static string WWW_ROOT = "App_Data";

        public static string Read(string fileName, string location)
        {
            var path = Path.Combine(WWW_ROOT, location);
            Directory.CreateDirectory(path);
            path = Path.Combine(path, fileName);
            
            string jsonResult;
            using (var streamReader = new StreamReader(path))
            {
                jsonResult = streamReader.ReadToEnd();
            }
            return jsonResult;
        }
 
        public static void Write(string fileName, string location, string jsonString)
        {
            var path = Path.Combine(WWW_ROOT, location);
            Directory.CreateDirectory(path);
            path = Path.Combine(path, fileName);
            
            using (var writer = File.CreateText(path))
            {
                writer.Write(jsonString);
            }
        }
    }
}