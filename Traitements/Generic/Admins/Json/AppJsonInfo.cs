namespace Admins.Traitements.Generic.Admins.Json
{
    public class AppJsonInfo
    {
        public string AppName { get; set; }

        public string AppDesc { get; set; }

        public string AppVersion { get; set; }

        public string AppLink { get; set; }

        public string AppLogo { get; set; }

        public string AppWebsite { get; set; }

        public string AppAuthor { get; set; }

        public string AuthorWebsite { get; set; }
        
        public object DataContent { get; set; }
    }
}