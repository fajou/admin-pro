namespace Admins.Traitements.Generic.Admins.Json
{
    public class MenuChildJsonInfo
    {
        public string SubMenuId { get; set; }
        
        public string SubMenuTitle { get; set; }
        
        public string SubMenuIcon { get; set; }
        
        public string SubMenuUrl { get; set; }
        
        public string SubMenuDesc { get; set; }
        
    }
}