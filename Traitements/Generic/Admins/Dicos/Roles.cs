using System;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;

namespace Admins.Traitements.Generic.Admins.Dicos
{
    [ListEditable]
    [ListDeletable]
    [Lists("Listes roles disponible")]
    [Forms("Rôle d'autorisation")]
    public class Roles : BaseModel
    {
        private int _levels;
        private string _rolesName;
        
        [Champ(Label = "Niveau d'autorisation", Type = "numeric")]
        public int Levels
        {
            get { return _levels; }
            set
            {
                if(value == 0)
                    throw new Exception("Niveau d'autorisation 0 non autorisé");
                _levels = value;
            }
        }

        [Champ(Label = "Nom Rôles")]
        public string RolesName
        {
            get { return _rolesName; }
            set
            {
                if(string.IsNullOrEmpty(value))
                    throw new Exception("Nom Rôles vide, invalide");
                _rolesName = value;
            }
        }
    }
}