using System.Collections.Generic;
using Admins.Traitements.Generic.Base;

namespace Admins.Traitements.Generic.Admins.Dicos
{
    public class TypeMenus : BaseModel
    {
        public static readonly int Parent = 0;
        public static readonly int Saisie = 1;
        public static readonly int Listes = 2;
        public static readonly int Link = 3;
        
        public override Dictionary<string, string> DicoForOption()
        {
            return new Dictionary<string, string> {["0"] = "Parent", ["1"] = "Saisie", ["2"] = "Listes", ["3"] = "Url"};
        }
    }
}