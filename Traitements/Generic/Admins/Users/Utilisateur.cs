using System;
using Admins.Traitements.Generic.Admins.Dicos;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;
using Admins.Traitements.Generic.Util;
using static Admins.Traitements.Generic.Attributes.Champ;

namespace Admins.Traitements.Generic.Admins.Users
{
    [ListEditable]
    [ListDeletable]
    [Lists("Liste Utilisateurs")]
    [Forms("Nouveau Utilisateur", "Modification Utilisateur")]
    public class Utilisateur : BaseModel
    {

        private string _imagePath;
        private string _nom;
        private string _prenom;
        private string _email;
        private DateTime _dateNaissance;
        private string _pass;
        private int _levels;
        private string _nomComplet;

        [Hide(SearchInput = true)]
        [Champ(Label = "Image Profile", Accept = "image/*", Type = FileIdentifier)]
        public string ImagePath
        {
            get { return _imagePath ?? ConstName.DEFAULT_USER; }
            set { _imagePath = value; }
        }

        [Champ]
        public string Nom
        {
            get { return _nom; }
            set
            {
                if(string.IsNullOrEmpty(value))
                    throw new Exception("Nom utilisateur vide invalide");
                _nom = value;
            }
        }

        [Champ]
        public string Prenom
        {
            get { return _prenom; }
            set { _prenom = value; }
        }

        [Champ(Type = "email", Label = "E-mail")]
        public string Email
        {
            get { return _email; }
            set
            {
                if(string.IsNullOrEmpty(value))
                    throw new Exception("Email utilisateur vide invalide");
                _email = value;
            }
        }

        [DateTimeFormat("yyyy-MM-dd")]
        [Champ(Label = "Date Naissance", Type = "date")]
        public DateTime DateNaissance
        {
            get { return _dateNaissance; }
            set { _dateNaissance = value; }
        }

        [Champ(Label = "Mots de passe", Type = "password")]
        [Hide(SearchInput = true)]
        public string Pass
        {
            get { return _pass; }
            set
            {
                if(string.IsNullOrEmpty(value))
                    throw new Exception("Mots de passe vide invalide");
                _pass = value;
            }
        }

        [Champ(nameof(Roles.RolesName), "Levels", typeof(Roles), Option = nameof(Roles.Levels), SelectDB = true, Class = Use.ClFormSelect2)]
        public int Levels
        {
            get { return _levels; }
            set { _levels = value; }
        }

        [NotColumn]
        public string NomComplet
        {
            get { return _nom + " " + _prenom; }
            set { _nomComplet = value; }
        }
    }
}