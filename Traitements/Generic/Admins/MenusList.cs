using Admins.Traitements.Generic.Admins.Dicos;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;
using Admins.Traitements.Generic.Util;

namespace Admins.Traitements.Generic.Admins
{
    [ListEditable(TypeBaseModel = typeof(Menus))]
    [ListDeletable(TypeBaseModel = typeof(Menus))]
    [Lists("Liste Levels", typeof(MenusList))]
    public class MenusList : BaseModel
    {
        [Hide]
        public string IdParent { get; set; }
        
        [Champ(Label = "Parent menu")]
        public string LabelParent { get; set; }
        
        [Champ(Label = "Nom menu")]
        public string Label { get; set; }
        
        [Champ]
        public string Url { get; set; }
        
        [Champ(Label = "Icon class")]
        public string IconClass { get; set; }

        [Hide]
        [Champ(nameof(ClassName), "Nom Class", typeof(Menus), SelectBM = true, Class = Use.ClFormSelect2)]
        public string ClassName { get; set; }
        
        [Champ(nameof(TypeMenu), "Type Menu", typeof(TypeMenus), SelectBM = true, Class = Use.ClFormSelect2)]
        public int TypeMenu { get; set; }
        
        [Hide]
        public int Levels { get; set; }
        
        [Hide]
        public string IdAcces { get; set; }
        
        [Champ(Label = "Utilisateur")]
        public string RolesName { get; set; }
    }
}