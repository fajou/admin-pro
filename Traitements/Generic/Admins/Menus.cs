using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Admins.Traitements.Generic.Admins.Dicos;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;
using Admins.Traitements.Generic.Util;

namespace Admins.Traitements.Generic.Admins
{
    [ListEditable(TypeBaseModel = typeof(MenusList))]
    [ListDeletable(TypeBaseModel = typeof(MenusList))]
    [Lists("Liste Levels")]
    [Forms("Gestion menu Admins", InsertTitle = "Saisie Nouveau Menu", UpdateTitle = "Modification d'un menu")]
    public class Menus : BaseModel
    {
        private string _idParent;
        private string _label;
        private string _url;
        private string _iconClass;
        private string _className;
        private int _levels;
        private int _typeMenu = -1;

        [Champ(Label = "Url")]
        public string Url
        {
            get { return _url; }
            set { _url = value; }
        }
        
        [Hide]
        [Champ("ClassName", "Nom class", typeof(Menus), SelectBM = true, Class = Use.ClFormSelect2)]
        public string ClassName
        {
            get { return _className; }
            set
            {
                if(string.IsNullOrEmpty(value) && string.IsNullOrEmpty(_url))
                    throw new Exception("La classe correspond au menu est vide ou null");
                if(string.IsNullOrEmpty(_url))
                    _className = value; 
            }
        }
        
        [Champ(Label = "Nom menu")]
        public string Label
        {
            get { return _label; }
            set
            {
                if(string.IsNullOrEmpty(value))
                    throw new Exception("Le nom du menu est vide ou Null non valide");
                _label = value; 
            }
        }

        [Champ(Label = "Icon class")]
        public string IconClass
        {
            get { return _iconClass; }
            set { _iconClass = value; }
        }

        [Champ("TypeMenu", "Type menu", typeof(TypeMenus), SelectBM = true, Class = Use.ClFormSelect2)]
        public int TypeMenu
        {
            get { return _typeMenu; }
            set { _typeMenu = value; }
        }

        [Champ("RolesName", "Rôle d'autorisation", typeof(Roles), Option = nameof(Roles.Levels), SelectDB = true, Class = Use.ClFormSelect2)]
        public int Levels
        {
            get { return _levels; }
            set { _levels = value; }
        }
       
        [Champ("IdParent", "Parent menu", typeof(MenuParent), SelectBM = true, Class = Use.ClFormSelect2)]
        public string IdParent
        {
            get { return _idParent; }
            set { _idParent = value; }
        }

        [NotColumn]
        public bool IsActive { get; set; }

        public override Dictionary<string, string> DicoForOption()
        {
            return Assembly.GetAssembly(typeof(BaseModel)).GetTypes()
                .Where(type => type.BaseType == typeof(BaseModel) && type.GetCustomAttribute(typeof(HideMenuAcces)) == null)
                .ToDictionary(type => type.FullName, type => type.Name);
        }
    }
}