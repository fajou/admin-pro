using System.Collections.Generic;
using System.Web;
using Admins.Traitements.Generic.Admins;
using Admins.Traitements.Generic.Algo;
using Admins.Traitements.Generic.Util;

namespace Admins.Traitements.Generic.Context
{
    public static class MenusContext
    {
        public static HttpSessionStateBase SessionContext { get; set; }

        public static Menus Current
        {
            get { return SessionContext?[ConstName.CURR_MENU] as Menus; }
            set { SessionContext[ConstName.CURR_MENU] = value; }
        }

        //Modifier dans la fonction GetMenu
        public static Graph<Menus> MenuGraph
        {
            get { return SessionContext?[ConstName.CURR_GRAPH_MENU] as Graph<Menus>; }
            set { SessionContext[ConstName.CURR_GRAPH_MENU] = value; }
        }

        public static List<Menus> BreadCrumb
        {
            get { return SessionContext?[ConstName.CURR_BREAD_CRUMB] as List<Menus>; }
            set { SessionContext[ConstName.CURR_BREAD_CRUMB] = value; }
        }
    }
}