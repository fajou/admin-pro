using System.Reflection;

namespace Admins.Traitements.Generic.Search
{
    public class SearchField
    {
        public SearchField(PropertyInfo propertyInfo, object value)
        {
            PropertyInfo = propertyInfo;
            Value = value;
        }

        public SearchField()
        {
        }

        public PropertyInfo PropertyInfo { get; set; }
        
        public object Value { get; set; }
    }
}