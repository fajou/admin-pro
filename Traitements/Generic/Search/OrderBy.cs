using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Admins.Traitements.Generic.Base;

namespace Admins.Traitements.Generic.Search
{
    public class OrderBy
    {
        public const string FIELD_DESC = "Decroissant";
        public const string FIELD_ASC = "Croissant";
        public const string TYPE_DESC = "DESC";
        public const string TYPE_ASC = "ASC";
        public const string ID_DESC = "Id DESC";

        public string Columns { get; set; }
        public string OrderType { get; set; }
        
        public PropertyInfo[] PropertyInfos { get; set; }
        
        public Dictionary<string, string> DicoColumns()
        {
            
            var properties = PropertyInfos?.Where(info => !BaseModel.Hide(info));
            return properties?.ToDictionary(info => info.Name, BaseModel.LabelChamp);
        }

        public static Dictionary<string, string> DicoOrderType()
        {
            return new Dictionary<string, string>{{TYPE_ASC, FIELD_ASC}, {TYPE_DESC, FIELD_DESC}};
        }

        public string Query()
        {
            OrderType = String.IsNullOrEmpty(OrderType) ? TYPE_DESC : OrderType;
            if (!String.IsNullOrEmpty(Columns))
                return $"{Columns} {OrderType}";
            
            return String.IsNullOrEmpty(Columns) ? $"Id {OrderType}" : null;
        }
    }
}