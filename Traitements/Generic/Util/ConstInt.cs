namespace Admins.Traitements.Generic.Util
{
    public static class ConstInt
    {
        public const int MaxPaging = 10;

        public const int ItemMax = 10;

        public const int LevelsMax = 1000;
    }
}