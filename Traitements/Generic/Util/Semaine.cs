using System;
using System.Globalization;

namespace Admins.Traitements.Generic.Util
{
    public class Semaine
    {
        public static readonly DayOfWeek[] Weeks = {
            DayOfWeek.Sunday,
            DayOfWeek.Monday,
            DayOfWeek.Tuesday,
            DayOfWeek.Wednesday,
            DayOfWeek.Thursday,
            DayOfWeek.Friday,
            DayOfWeek.Saturday
        };

        public static readonly string[] Names = {"Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"};
        
        public DateTime Dimanche { get; set; }
        
        public DateTime Lundi { get; }
        
        public DateTime Mardi { get; }
        
        public DateTime Mercredi { get; }
        
        public DateTime Jeudi { get; }
        
        public DateTime Vendredi { get; }
        
        public DateTime Samedi { get; set; }

        public Semaine(DateTime date)
        {
            SetDates(date);
            Lundi = Dimanche.AddDays(1);
            Mardi = Dimanche.AddDays(2);
            Mercredi = Dimanche.AddDays(3);
            Jeudi = Dimanche.AddDays(4);
            Vendredi = Dimanche.AddDays(5);
        }

        public Semaine()
        {

        }
        
        private void SetDates(DateTime date)
        {
            var day = CultureInfo.CurrentCulture.Calendar.GetDayOfWeek(date);

            var j = 0;
            var k = 0;
            foreach (var week in Weeks)
                if (week < day)
                    k--;
                else if(day < week)
                    j++;

            Dimanche = date.AddDays(k);
            Samedi = date.AddDays(j);
        }
    }
}