using System;
using System.Globalization;
using System.Reflection;
using Admins.Traitements.Generic.Admins;
using Admins.Traitements.Generic.Admins.Dicos;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;
using Admins.Traitements.Generic.Html;
using Admins.Traitements.Services.Repository;

namespace Admins.Traitements.Generic.Util
{
    public static class Use
    {
        public static readonly IDataBaseModel Dbo = new GenericDBO();

        public const string HomeID = "home";

        public const string KEY_DATA_URL = "data-url";

        public const string KEY_DATA_DESC = "data-desc";

        public const string KEY_DATA_ID = "data-id";
        
        public const string CONTROL_LABEL = "control-label";

        public const string ImagePath = "/Content/Images";
        
        public const string TableCl = "table table-responsive table-bordered";
        
        public const string Cl_ROW = "row";

        public const string Scarp = "#";

        public const string COL_XS_2 = "col-xs-2";
        
        public const string COL_XS_3 = "col-xs-3";
        
        public const string COL_XS_4 = "col-xs-4";

        public const string COL_SM_3 = "col-sm-3";

        public const string COL_SM_4 = "col-sm-4";

        public const string COL_SM_8 = "col-sm-8";

        public const string COL_SM_9 = "col-sm-9";

        public const string CL_BREADCRUMB = "breadcrumb";

        public const string CL_CONTENT_HEADER = "content-header";
        
        public const string CL_CONTENT_HEADER_TAB = "content-header-tabed";
        

        public const string MethodGET = "GET";

        public const string MethodPOST = "POST";

        public const string UlPaginationCl = "pagination pagination-sm no-margin pull-right";
        
        public const string PKey = "";
        
        public const string More = "...";

        public const string ClBlue = "bg-light-blue";

        public const string ClBtn = "btn";

        public const string ClFormGroup = "form-group";

        public const string ClTreeView = "treeview";

        public const string ClActive = "active";

        public const string CL_CONTENT = "content";

        public const string Preve = "Prev.";

        public const string Next = "Suiv.";

        public const string ACS = "AdminsConnectionString";

        private const string UrlForms = "/Admins/Forms";

        private const string UrlPageForms = "/Admins/PageForms";

        public const string URLPageListes = "/Admins/PageListes";

        public const string URLListesView = "/Admins/ListesView";

        public const string UrlPreviewDetails = "/Admins/Preview";

        private const string URLDelete = "/Admins/Del";

        public const string ClTreeViewMenu = "treeview-menu";

        public const string OptionText = "Choisissez un element";

        public const string ClFormControl = "form-control";

        public const string ClSelect2 = "select2";

        public const string ClFormSelect2 = "form-control select2";
        
        public const string Hidden = "hidden";
        
        public const string ClTable = "table";

        public const string ClTr = "tr";
        
        public const string ClSubMenu = "subMenu";
        
        public const string ClReload = "reload";
            

        public static string FuncPagingListeView(string url)
        {
            return $"PagingListeView('{url}')";
        }

        public static string FuncImageUploader(string evt, string dos)
        {
            return $"ImageUploader({evt}, '{dos}')";
        }
        
        public static string FuncPagingSearchListeView(string url, string formId)
        {
            return $"PagingSearchListeView('{url}', '{formId}')";
        }

        public static string UrlMenu(Menus menu)
        {
            if (menu.TypeMenu == TypeMenus.Saisie)
                return string.IsNullOrEmpty(menu.Url) ? UrlForm(Type.GetType(menu.ClassName), string.Empty, menu) : menu.Url + $"?{Input.MenuId}={menu.Id}";
            
            if(menu.TypeMenu == TypeMenus.Listes)
                return string.IsNullOrEmpty(menu.Url) ? UrlPageListes(Type.GetType(menu.ClassName), 1, ConstInt.ItemMax, menu) : menu.Url + $"?{Input.MenuId}={menu.Id}";
            
            if(menu.TypeMenu == TypeMenus.Parent && !Equals("#", menu.Url))
                return menu.Url + $"?{Input.MenuId}={menu.Id}";
            
            if(menu.TypeMenu == TypeMenus.Link)
                return menu.Url + $"?{Input.MenuId}={menu.Id}";
            
            return menu.Url;
        }
        
        public static string UrlForm(Type type, string id = "", Menus menu = null)
        {
            var ID = string.IsNullOrEmpty(id) ? "" : $"{Input.IdName}={id}&";
            var MENU = menu == null ? "" : $"{Input.MenuId}={menu.Id}&";
            return $"{UrlForms}?{MENU}{ID}{Input.ClassName}={type}";
        }

        public static string UrlPageForm(Type type, string id = null)
        {
            var ID = string.IsNullOrEmpty(id) ? string.Empty : $"{Input.IdName}={id}&";
            return $"{UrlPageForms}?{ID}{Input.ClassName}={type}";
        }

        public static string UrlPageListes(Type type, int index = 1, int items = ConstInt.ItemMax, Menus menu = null)
        {
            var baseType = (Lists) type.GetCustomAttribute(typeof(Lists));
            var aType = baseType?.TypeBaseModel ?? type;

            var MENU = menu == null ? "" : $"{Input.MenuId}={menu.Id}&";
            return $"{URLPageListes}?{MENU}{Input.PageIndex}={index}&{Input.ItemsMax}={items}&{Input.ClassName}={aType}";
        }

        public static string UrlPreview(Type type, string id)
        {
            return $"{UrlPreviewDetails}?{Input.ClassName}={type}&{Input.IdName}={id}";
        }
        
        public static string UrlListesView(Type type, int index = 1, int items = ConstInt.ItemMax)    
        {
            return $"{URLListesView}?{Input.PageIndex}={index}&{Input.ItemsMax}={items}&{Input.ClassName}={type}";
        }

        public static string UrlDelete(Type type, string modeleId)
        {
            return $"{URLDelete}?{Input.IdName}={modeleId}&{Input.ClassName}={type}";
        }

        public static string UrlPartialView(string viewName)
        {
            return string.Empty;
        }

        public static string FileName(string fileName, string dos)
        {
            var res = string.IsNullOrEmpty(dos) ? dos : $"{dos}/";
            return $"{res}{fileName}";
        }

        public static string FormatMGA(decimal value, bool invariant = false)
        {
            if (value == 0)
                return string.Empty;
            var s = $"{value:C0}";
            return invariant ? value.ToString(CultureInfo.InvariantCulture) : $"{s.Remove(s.Length - 1)}MGA";
        }
    }
}