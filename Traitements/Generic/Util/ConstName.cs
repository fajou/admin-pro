namespace Admins.Traitements.Generic.Util
{
    public static class ConstName
    {
        public const string USER_START = "USER_START";

        public const string CURR_BREAD_CRUMB = "CURR_BREAD_CRUMB";

        public const string CURR_GRAPH_MENU = "CURR_GRAPH_MENU";

        public const string CURR_MENU = "CURR_MENU";

        public const string TAB = "tab";
        
        public const string CONTENT_ID = "ContentID";
        
        public const string ITEMS_LABEL = "Items";

        public const string USER_SESS = "USER_SESS";

        public const string DEFAULT_USER = "users.jpg";
    }
}