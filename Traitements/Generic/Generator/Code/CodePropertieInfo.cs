using System;
using System.CodeDom;

namespace Admins.Traitements.Generic.Generator.Code
{
    public class CodePropertieInfo
    {
        private readonly string _fieldName;
        private readonly string _propertyName;
        private readonly Type _typeProperty;
        private readonly CodeAttributeInfo[] _attributes;

        public CodePropertieInfo(string fieldName, Type typeProperty, CodeAttributeInfo[] attributes = null)
        {
            _typeProperty = typeProperty;
            _attributes = attributes;
            _propertyName = fieldName;
            _fieldName = GetFieldName();
            SetCodeMemberField();
            SetCodeMemberProperty();
        }

        public CodeMemberField CodeMemberField { get; private set; }

        public CodeMemberProperty CodeMemberProperty { get; private set; }


        private string GetFieldName()
        {
            if (_propertyName == null || _propertyName.Length <= 1)
                throw new Exception("The length of the attribute is very short");

            if(char.IsUpper(_propertyName[0]))
                return "m" + _propertyName;

            return $"m{char.ToLower(_propertyName[0]) + _propertyName.Substring(1)}";
        }

        private void SetCustomsAttributes(CodeTypeMember memberProperty)
        {
            if(_attributes == null)
                return;
            foreach (var attributte in _attributes)
                memberProperty.CustomAttributes.Add(attributte.CodeAttributeDeclaration);
        }

        private void SetCodeMemberField()
        {
            CodeMemberField = new CodeMemberField(_typeProperty, _fieldName)
            {
                Attributes = MemberAttributes.Private
            };
        }

        private void SetCodeMemberProperty()
        {
            var property = new CodeMemberProperty
            {
                Attributes = MemberAttributes.Public,
                Name = _propertyName,
                HasGet = true,
                HasSet = true,
                Type = new CodeTypeReference(_typeProperty)
            };
            SetCustomsAttributes(property);

            var right = new CodeVariableReferenceExpression("value");
            var left = new CodeVariableReferenceExpression($"{_fieldName}");

            property.GetStatements.Add(new CodeMethodReturnStatement(new CodeVariableReferenceExpression(_fieldName)));
            property.SetStatements.Add(new CodeAssignStatement(left, right));

            CodeMemberProperty = property;
        }
    }
}