using System;
using System.CodeDom;

namespace Admins.Traitements.Generic.Generator.Code
{
    public class CodeAttributeInfo
    {
        public CodeAttributeInfo(Type attribute)
        {
            SetCodeAttributeDeclaration(attribute);
        }

        public CodeAttributeInfo(string typeName)
        {
            SetCodeAttributeDeclaration(typeName);
        }

        private void SetCodeAttributeDeclaration(Type attribute)
        {

            /**
             *  attr.Arguments.Add(new CodeAttributeArgument(new CodeTypeOfExpression(typeof(decimal))));
                attr.Arguments.Add(new CodeAttributeArgument(new CodePrimitiveExpression("-922337203685477.5808")));
                attr.Arguments.Add(new CodeAttributeArgument(new CodePrimitiveExpression("922337203685477.5807")));
                attr.Arguments.Add(new CodeAttributeArgument("ErrorMessage", new CodePrimitiveExpression("")));
             */
            CodeAttributeDeclaration = new CodeAttributeDeclaration(new CodeTypeReference(attribute));
        }

        private void SetCodeAttributeDeclaration(string attribute)
        {

            /**
             *  attr.Arguments.Add(new CodeAttributeArgument(new CodeTypeOfExpression(typeof(decimal))));
                attr.Arguments.Add(new CodeAttributeArgument(new CodePrimitiveExpression("-922337203685477.5808")));
                attr.Arguments.Add(new CodeAttributeArgument(new CodePrimitiveExpression("922337203685477.5807")));
                attr.Arguments.Add(new CodeAttributeArgument("ErrorMessage", new CodePrimitiveExpression("")));
             */
            CodeAttributeDeclaration = new CodeAttributeDeclaration(new CodeTypeReference(attribute));
        }

        public CodeAttributeDeclaration CodeAttributeDeclaration { get; private set; }
    }
}