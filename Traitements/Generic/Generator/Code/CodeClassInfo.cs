using System.CodeDom;
using System.Collections.Generic;
using System.Reflection;
using Admins.Traitements.Generic.Base;

namespace Admins.Traitements.Generic.Generator.Code
{
    public class CodeClassInfo
    {
        private readonly string _className;

        private readonly string _nameSpace;

        private readonly string _tableName;


        public CodeClassInfo(string nameSpace, string className, string tableName)
        {
            _nameSpace = nameSpace;
            _className = className;
            _tableName = tableName;
            SetCodeNamespace();
            SetTypeDeclaration();
        }

        private void SetTypeDeclaration()
        {
            CodeTypeDeclaration = new CodeTypeDeclaration(_className)
            {
                IsClass = true,
                TypeAttributes = TypeAttributes.Public | TypeAttributes.Class,
                BaseTypes = {nameof(BaseModel)}
            };
        }

        private void SetCodeNamespace()
        {
            CodeNamespace = new CodeNamespace(_nameSpace);
            CodeNamespace.Imports.Clear();
        }

        public void SetCodePropertieInfo(IEnumerable<CodePropertieInfo> properties)
        {
            if (properties == null)
                return;
            foreach (var info in properties)
            {
                CodeTypeDeclaration.Members.Add(info.CodeMemberField);
                CodeTypeDeclaration.Members.Add(info.CodeMemberProperty);
            }
        }

        public void SetCodeAttributeInfo(IEnumerable<CodeAttributeInfo> attributes)
        {
            if (attributes == null)
                return;
            foreach (var info in attributes)
            {
                CodeTypeDeclaration.CustomAttributes.Add(info.CodeAttributeDeclaration);
            }
        }

        public CodeNamespace CodeNamespace { get; private set; }

        public CodeTypeDeclaration CodeTypeDeclaration { get; private set; }
    }
}