using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Attributes.Chart;
using Admins.Traitements.Generic.Base;

namespace Admins.Traitements.Generic.Generator.Xml
{
    public class XmlPropertie : BaseXml
    {
        public ChartInfo ChartInfo { get; set; }
        
        public Column Column { get; set; }
        
        public Hide Hide { get; set; }
        
        public Champ Champ { get; set; }
        
        public NumberFormat NumberFormat { get; set; }
        
        public DateTimeFormat DateTimeFormat { get; set; }
        
        public NotColumn NotColumn { get; set; }
    }
}