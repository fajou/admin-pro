using System.Collections.Generic;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Attributes.Chart;
using Admins.Traitements.Generic.Base;

namespace Admins.Traitements.Generic.Generator.Xml
{
    public class XmlClass : BaseXml
    {
        public TableInfo TableInfo { get; set; }

        public PreviewDetails PreviewDetails { get; set; }
        
        public Lists Lists { get; set; }
        
        public ListEditable ListEditable { get; set; }
        
        public ListDeletable ListDeletable { get; set; }
        
        public HideMenuAcces HideMenuAcces { get; set; }
        
        public Forms Forms { get; set; }
        
        public ChartType ChartType { get; set; }
        
        public List<XmlPropertie> XmlProperties { get; set; }
    }
}