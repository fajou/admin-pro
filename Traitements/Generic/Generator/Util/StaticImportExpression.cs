using System.CodeDom;

namespace Admins.Traitements.Generic.Generator.Util
{
    public static class StaticImportExpression
    {
        public static CodeNamespace UsingImports
        {
            get
            {
                var imports = new []
                {
                    new CodeNamespaceImport("Admins.Traitements.Generic.Base"),
                    new CodeNamespaceImport("Admins.Traitements.Generic.Attributes"),
                    new CodeNamespaceImport("Admins.Traitements.Generic.Attributes.Chart"),
                    new CodeNamespaceImport("Admins.Traitements.Generic.Util"),
                    new CodeNamespaceImport("static Admins.Traitements.Generic.Util.Use")
                };
                var space = new CodeNamespace();
                space.Imports.AddRange(imports);
                return space;
            }
        }
    }
}