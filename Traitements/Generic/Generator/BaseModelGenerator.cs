using System.CodeDom;
using System.CodeDom.Compiler;
using System.IO;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Generator.Code;
using Admins.Traitements.Generic.Generator.Util;
using Admins.Traitements.Models;

namespace Admins.Traitements.Generic.Generator
{
    public class BaseModelGenerator
    {
        private CodeClassInfo ClassInfo { get; set; }
        private readonly CodeCompileUnit CompileUnit;

        public BaseModelGenerator(CodeClassInfo classInfo)
        {
            ClassInfo = classInfo;
            SetCodeAttributeInfo();
            SetCodePropertieInfo();

            ClassInfo.CodeNamespace.Types.Add(ClassInfo.CodeTypeDeclaration);
            CompileUnit = new CodeCompileUnit
            {
                Namespaces = {StaticImportExpression.UsingImports, ClassInfo.CodeNamespace}
            };
        }

        private void SetCodeAttributeInfo()
        {
            ClassInfo.SetCodeAttributeInfo(new[] {new CodeAttributeInfo(nameof(ListEditable)), new CodeAttributeInfo(nameof(ListDeletable))});
        }

        private void SetCodePropertieInfo()
        {
            ClassInfo.SetCodePropertieInfo(new[]{new CodePropertieInfo("Nom", typeof(FormViewModel), new[] {new CodeAttributeInfo(nameof(Champ))})});
        }

        public void GenerateCSharpCode(string fileName)
        {
            var provider = CodeDomProvider.CreateProvider("CSharp");
            var options = new CodeGeneratorOptions {BracingStyle = "C"};
            using (var sourceWriter = new StreamWriter(fileName))
            {
                provider.GenerateCodeFromCompileUnit(CompileUnit, sourceWriter, options);
            }
        }
    }
}