using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;

namespace Admins.Traitements.Generic.Generator.Base
{
    [TableInfo("BASE_ATTRIBUTES", "ID_BASE_ATTRIBUTES", "A")]
    public class BaseAttribute : BaseModel
    {
        public string AttributeId { get; set; }

        public string AttributeName { get; set; }

        public string AttributeArgument { get; set; }

        public string AtritbuteData { get; set; }
    }
}
