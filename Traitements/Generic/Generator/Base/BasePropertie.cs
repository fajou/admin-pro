using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;
using Admins.Traitements.Generic.Util;
using Admins.Traitements.Models.Generator;

namespace Admins.Traitements.Generic.Generator.Base
{
    [ListDeletable]
    [ListEditable]
    [Lists("List Champs disponible")]
    [TableInfo("BASE_PROPERTIES", "ID_BASE_PROPERTIES", "P")]
    [Forms("Nouveau champ", "Modification champ", HorizontalForms = true)]
    public class BasePropertie : BaseModel
    {
        [Champ(Type = "hidden")]
        public string IdClass { get; set; }

        [Champ(Label = "Nom champ")]
        public string PropertieName { get; set; }

        [Champ(nameof(ListeTypes.TypeName), "Type", typeof(ListeTypes),  SelectDB = true, Class = Use.ClFormSelect2)]
        public string PropertieTypeName { get; set; }

        [Champ(Label = "Type du champ")]
        public string PropertieType { get; set; }
    }
}