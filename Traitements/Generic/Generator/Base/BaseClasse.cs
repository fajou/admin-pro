using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;

namespace Admins.Traitements.Generic.Generator.Base
{
    [ListDeletable]
    [ListEditable]
    [Lists("List forms disponible")]
    [TableInfo("BASE_CLASSES", "ID_BASE_CLASSES", "C")]
    [Forms("Nouveau forms", "Modification forms")]
    public class BaseClasse : BaseModel
    {
        [Champ(Label = "Nom de la classe")]
        public string ClassName { get; set; }

        [Champ(Label = "Nom du classement")]
        public string NameSpace { get; set; }

        [Champ(Label = "Nom de la table")]
        public string TableName { get; set; }
    }
}