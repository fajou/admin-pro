using System;

namespace Admins.Traitements.Generic.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ListDeletable : Attribute
    {
        public string Icon { get; set; } = "fa fa-trash";

        public string Text { get; set; } = "...";

        public string Class { get; set; } = "text-red action-margin";

        public string TdClass { get; set; } = "table-td-action";
        
        public Type TypeBaseModel { get; set; }

        public ListDeletable(){}

        public ListDeletable(string icon, string text)
        {
            Icon = icon;
            Text = text;
        }
    }
}