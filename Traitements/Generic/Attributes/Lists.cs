using System;

namespace Admins.Traitements.Generic.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class Lists : Attribute
    {
        public string Title { get; }

        public Type TypeBaseModel { get; }
        
        public string OrderLabel { get; set; } = "Trier Par";

        public string OrderTypeLabel { get; set; } = "Ordre";
        
        public bool OrderColumn { get; set; } = true;

        public Lists(){}
        
        
        public Lists(string title, Type model = null)
        {
            Title = title;
            TypeBaseModel = model;
        }
    }
}