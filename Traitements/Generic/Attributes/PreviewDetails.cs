using System;

//
//Cette attribute active une colonne pour voir en detail un model
//
namespace Admins.Traitements.Generic.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class PreviewDetails : Attribute
    {
        public Type TypeBaseModel { get; set; }

        public string BoxTitle { get; set; }

        public string Icon { get; set; } = "fa fa-eye";

        public string Text { get; set; } = "...";

        public string Class { get; set; } = "action-margin";
        
        public string TdClass { get; set; }
    }
}