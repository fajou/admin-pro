using System;

namespace Admins.Traitements.Generic.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class NumberFormat : Attribute
    {
        public string Format { get; }

        public NumberFormat(string format = "C0")
        {
            Format = format;
        }

        public NumberFormat()
        {
        }
    }
}