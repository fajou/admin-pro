using System;

namespace Admins.Traitements.Generic.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class DateTimeFormat : Attribute
    {
        public string Format { get; }
        
        public DateTimeFormat(){}
        
        public DateTimeFormat(string format = "s")
        {
            Format = format;
        }
    }
}