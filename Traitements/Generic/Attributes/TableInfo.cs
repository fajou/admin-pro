﻿using System;

namespace Admins.Traitements.Generic.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class TableInfo : Attribute
    {
        public string Name { get; }
        
        public string Sequence { get; }
        
        public string Predicas { get; }
        
        public TableInfo(){}
        
        public TableInfo(string name, string sequence, string predicas)
        {
            Name = name;
            Sequence = sequence;
            Predicas = predicas;
        }
    }
}