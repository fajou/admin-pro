using System;

namespace Admins.Traitements.Generic.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ListEditable : Attribute
    {
        public string Icon { get; set; } = "fa fa-pencil";

        public string Text { get; set; } = "...";

        public string Class { get; set; } = "action-margin";
        
        public Type TypeBaseModel { get; set; }
        
        public ListEditable(){}

        public ListEditable(string icon, string text)
        {
            Icon = icon;
            Text = text;
        }
    }
}