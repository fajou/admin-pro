using System;

namespace Admins.Traitements.Generic.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class Champ : Attribute
    {
        public Champ(){}

        public Champ(string name, string label, string type)
        {
            Name = name;    
            Type = type;
            Label = label;
        }
        
        public Champ(string name, string label, Type model)
        {
            Name = name;
            Label = label;
            TypeModel = model;
        }

        public string Id { get; set; }
        
        public string Label { get; set; }

        public string Type { get; set; } = "text";

        public string Name { get; set; }

        public string Option { get; set; } = "Id";

        public string Class { get; set; } = "form-control";
        
        //Type file to upload.
        public string Accept { get; set; }

        public bool Choix { get; set; }
        
        //Data from Database
        public bool SelectDB { get; set; }
        
        //Data from DicoForOption() on BaseModel
        public bool SelectBM { get; set; }
        
        public Type TypeModel { get; set; }
        
        //<input type="@Champ.DateTime"/>
        
        public const string Datetime = "datetime";

        public const string Numeric = "number";
        
        public const string Submit = "submit";

        public const string FileIdentifier = "file";
        
    }
}