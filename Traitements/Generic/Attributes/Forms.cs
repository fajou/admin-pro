using System;

namespace Admins.Traitements.Generic.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class Forms : Attribute
    {
        public Type TypeBaseModel { get; set; }
        
        public string UpdateTitle { get; set; }
        
        public string InsertTitle { get; set; }
        
        public string DefaultTitle { get; set; }
        
        public bool SaveAndCreateAnOther { get; set; }

        public bool HorizontalForms { get; set; }

        public Forms(){}
        
        public Forms(Type model)
        {
            TypeBaseModel = model;
        }
        
        public Forms(string title)
        {
            DefaultTitle = title;
        }

        public Forms(string insert, string update)
        {
            InsertTitle = insert;
            UpdateTitle = update;
        }
    }
}