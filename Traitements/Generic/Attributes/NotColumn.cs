﻿using System;

namespace Admins.Traitements.Generic.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class NotColumn : Attribute
    {}
}