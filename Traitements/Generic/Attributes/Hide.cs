using System;

namespace Admins.Traitements.Generic.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class Hide : Attribute
    {
        public bool SearchInput { get; set; }
    }
}