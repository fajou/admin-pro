using System;

namespace Admins.Traitements.Generic.Attributes.Chart
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ChartType : Attribute
    {
        public string Type { get; set; } = "bar";
        
        public string Title { get; set; }
        
        public bool Fill { get; set; }

        public int BorderWith { get; set; } = 1;
    }
}