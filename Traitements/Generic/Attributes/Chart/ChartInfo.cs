using System;

namespace Admins.Traitements.Generic.Attributes.Chart
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ChartInfo : Attribute
    {
        public string Label { get; set; }
        public string BackgroundColor { get; set; } = "rgba(201, 203, 207, 0.2)";
        public string BorderColor { get; set; } = "rgb(201, 203, 207)";
    }
}