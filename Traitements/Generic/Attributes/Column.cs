﻿using System;

namespace Admins.Traitements.Generic.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class Column : Attribute
    {
        public string Name { get; set; }
        
        public Column(){}

        public Column(string name)
        {
            Name = name;
        }
    }
}