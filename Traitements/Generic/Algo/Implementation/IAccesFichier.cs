namespace Admins.Traitements.Generic.Algo.Implementation
{
    public interface IAccesFichier<T>
    {
        void WriteGraph(Graph<T> graph, string path);

        Graph<T> ReadGraph(string path);
    }
}