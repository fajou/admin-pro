using System.Collections.Generic;
using Admins.Traitements.Generic.Algo.Json;
using Admins.Traitements.Generic.Algo.OtherNode;

namespace Admins.Traitements.Generic.Algo.Implementation
{
    public interface IGraph<T>
    {
        void Add(T node);

        void Add(T node, List<PreveNode<T>> preds);

        int DegreePlus(T node);

        int DegredMinus(T node);

        void AddArc(T node, T pred, double capacite, double flot);
        
        void AddArc(T node, PreveNode<T> preve);

        void Remove(T node);

        void SetCapacity(string propertyName);

        double Capacite(T node, T pred);

        T[] Nodes();

        PreveNode<T>[] PrevesNode(T node);

        T[] NextsNodes(T node);
        
        PreveNode<T>[] PrevesWhere(T node);

        PreveNode<T>[] Arcs();
        
        Elements ElementsData();

        T[] Adjacents(T node);

        bool IsAdjacents(T node, T node1);

        string Preds(T node);

        TreeNode<T, List<PreveNode<T>>> GetTreeNodes();
    }
}