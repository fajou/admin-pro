using System.Collections.Generic;
using Admins.Traitements.Generic.Algo.OtherNode;

namespace Admins.Traitements.Generic.Algo.Implementation
{
    public interface IParcours<T>
    {
        List<PreveNode<T>> LevelDecomposition();

        Dictionary<T, PreveNode<T>> SortByDijkstra(T node);

        List<PreveNode<T>> SortByBellmanFord(T node);

        Dictionary<T, T> Longue(T node);

        Dictionary<T, T> Large(T node);

        string ToJSON(T node, PreveNode<T> treeNode);
    }

}