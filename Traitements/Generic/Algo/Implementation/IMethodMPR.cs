using System.Collections.Generic;
using Admins.Traitements.Generic.Algo.Method;
using Admins.Traitements.Generic.Algo.OtherNode;

namespace Admins.Traitements.Generic.Algo.Implementation
{
    public interface IMethodMPR<T> where T : new()
    {
        MethodMPR<T> GraphMPR(string duration, string taskName);

        List<NodeTask<T>> CheminCritique();

        IEnumerable<NodeTask<T>> NodeTasks();
    }
}