using System.Collections.Generic;
using Admins.Traitements.Generic.Algo.OtherNode;

namespace Admins.Traitements.Generic.Algo.Implementation
{
    public interface IColoration<T>
    {
        IEnumerable<NodeColor<T>> Colors();

        IEnumerable<NodeColor<T>> OrderNodesByDegree();
    }
}