using System;
using Admins.Traitements.Generic.Attributes;

namespace Admins.Traitements.Generic.Algo.OtherNode
{
    [HideMenuAcces]
    public class PreveNode <T> : IComparable, ICloneable
    {
        public T Node { get; set; }
        
        public T Source { get; set; }

        public int Level { get; set; }

        [Hide]
        public int Color { get; set; } = -1;
        
        public double Flow { get; set; }

        public double Capacity { get; set; }
        
        public bool Visited { get; set; }

        public double FlowLeft()
        {
            return Capacity - Flow;
        }

        public bool Satured()
        {
            return Math.Abs(FlowLeft()) <= 0.01 /*|| Math.Abs(Flow) <= 0.01*/;
        }
        
        public object Clone()
        {
            return new PreveNode<T>
            {
                Node = Node, 
                Flow = Flow, 
                Capacity = Capacity, 
                Level = Level,
                Visited = Visited, 
                Source = Source
            };
        }

        public int CompareTo(object obj)
        {
            var o = (PreveNode<T>) obj;
            return Math.Abs(o.Capacity - Capacity) < 0.001 ? 0 : o.Capacity < Capacity ? 1 : -1;
        }
    }
}