using System.Collections.Generic;

namespace Admins.Traitements.Generic.Algo.OtherNode
{
    public class TreeNode<TKey, TValue> : Dictionary<TKey, TValue>
    {
        public void Set(TKey key, TValue value)
        {
            if (ContainsKey(key))
                if (base[key] == null)
                    Add(key, value);
                else
                    base[key] = value;
            else
                Add(key, value);
        }
    }
}