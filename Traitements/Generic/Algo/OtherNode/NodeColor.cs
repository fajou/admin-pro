using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;

namespace Admins.Traitements.Generic.Algo.OtherNode
{
    [HideMenuAcces]
    public class NodeColor<T> : BaseModel
    {
        [Champ(Label = "Noeud")]
        public T Node { get; set; }
        
        [Hide]
        public int Adjacents { get; set; }
        
        [Hide]
        public int Color { get; set; } = -1;
        
        [Champ(Label = "Color")]
        public string StyleColor { get; set; }
    }
}