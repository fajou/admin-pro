using System;
using System.Collections.Generic;

namespace Admins.Traitements.Generic.Algo.OtherNode
{
    public class Node<T>
    {
        public T Key { get; set; }
        
        public int Level { get; set; }

        public double Flow { get; set; }

        public double Capacity { get; set; }
        
        public bool Visited { get; set; }

        public List<Node<T>> Preves { get; set; }
        
        public List<Node<T>> Nexts { get; set; }
        
        public double FlowLeft()
        {
            return Capacity - Flow;
        }
        
        public bool Satured()
        {
            return Math.Abs(FlowLeft()) <= 0.01 /*|| Math.Abs(Flow) <= 0.01*/;
        }
        
        public int CompareTo(object obj)
        {
            var o = (PreveNode<T>) obj;
            return Math.Abs(o.Capacity - Capacity) < 0.001 ? 0 : o.Capacity < Capacity ? 1 : -1;
        }

        public int DegreePlus()
        {
            return Nexts?.Count ?? 0;
        }

        public int DegreeMinus()
        {
            return Preves?.Count ?? 0;
        }
    }
}