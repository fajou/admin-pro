using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;

namespace Admins.Traitements.Generic.Algo.OtherNode
{
    [HideMenuAcces]
    public class NodeLevel<T> : BaseModel
    {
        [Champ(Label = "Noeud")]
        public T Node { get; set; }
        
        [Champ(Label = "Niveau")]
        public int Level { get; set; }
    }
}