using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;

namespace Admins.Traitements.Generic.Algo.OtherNode
{
    [HideMenuAcces]
    public class NodeTask<T> : BaseModel
    {
       public T Node { get; set; }
       
       [Hide]
       public T[] PreveNode { get; set; }

       [Hide]
       public bool Satured { get; set; }
       
       public object Durration { get; set; }
       
       [Champ(Label = "Can Start")]
       public double CanStart { get; set; }
       
       [Champ(Label = "Must Satrt")]
       public double MustStart { get; set; }

       [Hide]
       public string Color { get; set; }
    }
}