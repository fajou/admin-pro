using System.Collections.Generic;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;

namespace Admins.Traitements.Generic.Algo.OtherNode
{
    public class NodeChains<T> : BaseModel
    {
        [Hide]
        public List<PreveNode<T>> Chains { get; set; }
        
        public double Flow { get; set; }
        
        public new int Rang { get; set; }
    }
}