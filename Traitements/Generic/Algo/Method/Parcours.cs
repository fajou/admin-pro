using System;
using System.Collections.Generic;
using System.Linq;
using Admins.Traitements.Generic.Algo.Implementation;
using Admins.Traitements.Generic.Algo.Json;
using Admins.Traitements.Generic.Algo.OtherNode;

namespace Admins.Traitements.Generic.Algo.Method
{
    public class Parcours<T> : IParcours<T>
    {
        public Parcours(Graph<T> graph)
        {
            Graph = graph;
        }

        public Graph<T> Graph { get; }

        public Dictionary<T, T> Longue(T node)
        {
            var dico = new Dictionary<T, T>();
            Longue(node, dico);
            return dico;
        }

        public Dictionary<T, T> Large(T node)
        {
            var dico = new Dictionary<T, T>();
            Large(node, dico);
            return dico;
        }

        public string ToJSON(T node, PreveNode<T> treeNode)
        {
            throw new NotImplementedException();
        }

        public Dictionary<T, PreveNode<T>> SortByDijkstra(T node)
        {
            var preves = new List<PreveNode<T>>();
            var dico = new TreeNode<T, PreveNode<T>>();

            var chemins = Graph.Nodes().Select(key => new PreveNode<T>
                {
                    Node = key,
                    Source = node,
                    Visited = Equals(node, key),
                    Capacity = Graph.Capacite(key, node)
                })
                .OrderBy(c => c.Capacity).ToList();

            while (chemins.Count(c => !double.IsInfinity(c.Capacity)) > 0)
            {
                var res = chemins.Where(chemin => !chemin.Visited).Min(chemin => chemin);

                if (!double.IsInfinity(res.Capacity))
                    if (dico.ContainsKey(res.Source))
                    {
                        var value = dico[res.Source];
                        if (value != null && value.Capacity > res.Capacity)
                            dico.Set(res.Source, res);
                    }
                    else
                    {
                        dico.Set(res.Source, res);
                    }
                else
                    break;

                foreach (var chemin in chemins)
                {
                    if (chemin.Visited)
                        continue;
                    var aPreve = (PreveNode<T>) chemin.Clone();
                    if (Equals(chemin.Node, res.Node))
                    {
                        aPreve.Visited = true;
                    }
                    else
                    {
                        aPreve.Capacity = res.Capacity + Graph.Capacite(chemin.Node, res.Node);
                        if (chemin.Capacity < aPreve.Capacity && !double.IsInfinity(aPreve.Capacity))
                        {
                            aPreve.Capacity = chemin.Capacity;
                            aPreve.Source = chemin.Source;
                        }
                        else
                        {
                            aPreve.Source = res.Node;
                        }
                    }

                    preves.Add(aPreve);
                }

                chemins = preves.OrderBy(c => c.Capacity).ToList();
                preves.Clear();
            }

            return dico;
        }

        public List<PreveNode<T>> LevelDecomposition()
        {
            var chemins = new List<PreveNode<T>>();
            var keys = Graph.Nodes().ToList();

            while (keys.Count != 0)
            {
                var keysLeft = new List<T>();
                foreach (var key in keys)
                {
                    if (Graph.Elements[key] == null)
                        chemins.Add(new PreveNode<T> {Node = key, Level = 1});
                    else
                    {
                        var temps = new List<PreveNode<T>>();
                        var preds = Graph.Elements[key].Select(preve => preve.Node).ToArray();
                        var nbrs = preds.Length;

                        foreach (var chemin in chemins)
                            if (preds.Contains(chemin.Node))
                            {
                                nbrs--;
                                temps.Add(chemin);
                            }

                        if (nbrs == 0)
                        {
                            var max = temps.Max(chemin => chemin.Level);
                            chemins.Add(new PreveNode<T> {Node = key, Level = max + 1});
                        }
                        else
                            keysLeft.Add(key);
                    }
                }
                keys = keysLeft;
            }

            return chemins.OrderBy(chemin => chemin.Level).ToList();
        }

        public List<PreveNode<T>> SortByBellmanFord(T node)
        {
            throw new NotImplementedException();
        }

        private void Longue(T node, IDictionary<T, T> dico)
        {
            var preds = Graph.Elements[node];
            if (preds == null)
                return;

            foreach (var preve in preds)
            {
                if (dico.ContainsKey(preve.Node))
                    continue;

                dico.Add(preve.Node, node);
                Longue(preve.Node, dico);
            }
        }


        private void Large(T node, IDictionary<T, T> dico)
        {
            var values = Graph.Elements[node];
            if (values == null)
                return;

            foreach (var value in values)
            {
                if (dico.ContainsKey(value.Node))
                    continue;
                dico.Add(value.Node, node);
            }

            foreach (var val in values)
                Large(val.Node, dico);
        }

        public Elements ElementsLevel()
        {
            var keys = Graph.Nodes();
            var nodes = new List<Data>();
            var levels = LevelDecomposition();
            
            var data = levels.Select(arg => new Data
            {
                group = "nodes",
                data = new Nodes
                {
                    id = $"{arg.Node}",
                    task = $"{arg.Node}-{arg.Level}",
                    nodecolor = "#0000FF"
                }
            }).ToArray();
            nodes.AddRange(data);

            foreach (var key in keys)
            {
                var values = Graph.Elements[key];
                if (values == null)
                    continue;

                foreach (var value in values)
                {
                    nodes.Add(new Data
                    {
                        group = "edges",
                        data = new Edges
                        {
                            id = $"{key}{value.Node}",
                            source = $"{value.Node}",
                            target = $"{key}",
                            values = $"[{value.Flow}|{value.Capacity}]",
                            weight = value.Capacity
                        }
                    });
                    Console.WriteLine(value);
                }
            }

            return new Elements {Data = nodes.ToArray()};
        }
    }
}