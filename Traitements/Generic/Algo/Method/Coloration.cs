using System;
using System.Collections.Generic;
using System.Linq;
using Admins.Traitements.Generic.Algo.Implementation;
using Admins.Traitements.Generic.Algo.Json;
using Admins.Traitements.Generic.Algo.OtherNode;

namespace Admins.Traitements.Generic.Algo.Method
{
    public class Coloration<T> : Graph<T>, IColoration<T>
    {
        public IEnumerable<NodeColor<T>> OrderNodesByDegree()
        {
            var nodeColors = Nodes().Select(arg => new NodeColor<T>
                {Node = arg, Adjacents = DegredMinus(arg) + DegreePlus(arg)});
            
            return nodeColors.OrderByDescending(arg => arg.Adjacents).ToList();
        }

        private NodeColor<T> GetNextNodeColor(int color, NodeColor<T> aNode, IReadOnlyCollection<NodeColor<T>> nodes)
        {
            var adjs = nodes.Where(nodeColor => nodeColor.Color == color).ToArray();
            foreach (var nodeColor in nodes)
            {
                var isAdj = false;
                
                foreach (var adj in adjs)
                {
                    isAdj = IsAdjacents(adj.Node, nodeColor.Node);
                    if(isAdj) break;
                }
                
                if(isAdj) continue;
                
                if (nodeColor.Color == -1 && !Equals(nodeColor, aNode)) 
                    return nodeColor;
            }

            return null;
        }

        public IEnumerable<NodeColor<T>> Colors()
        {
            var nodes = OrderNodesByDegree().ToList();
            if (nodes.Count == 0)
                return nodes;
            
            nodes[0].Color = 0;
            int color = 0, i = 0;
            
            var next = GetNextNodeColor(color, nodes[i], nodes);
            
            while (nodes.Count(nodeColor => nodeColor.Color == -1) != 0 && i < nodes.Count)
            {
                if (next == null)
                {
                    i++;
                    if (nodes[i].Color != -1) 
                        continue;
                    color++;
                    nodes[i].Color = color;
                    next = GetNextNodeColor(color, nodes[i], nodes);
                } 
                else
                {
                    next.Color = color;
                    next = GetNextNodeColor(color, next, nodes);
                }
            }
            SetPeves(nodes);
            return nodes;
        }

        public new Elements ElementsData()
        {
            var keys = Nodes();
            var nodes = new List<Data>();
            
            var data = Colors().Select(arg => new Data
            {
                group = "nodes",
                data = new Nodes {
                    id = $"{arg.Node}",
                    nodecolor = Color[arg.Color]
                }
            }).ToArray();
            nodes.AddRange(data);
            
            foreach (var key in keys)
            {
                var values = Elements[key];
                if (values == null)
                    continue;
                
                foreach (var value in values)
                {
                    nodes.Add(new Data
                    {
                        group = "edges",
                        data = new Edges
                        {
                            id = $"{key}{value.Node}",
                            source = $"{value.Node}",
                            target = $"{key}",
                            values = $"[{value.Flow}|{value.Capacity}]",
                            weight = value.Capacity
                        }
                    });
                    Console.WriteLine(value);
                }
            }

            return new Elements { Data = nodes.ToArray() };
        }
        
        private void SetPeves(IEnumerable<NodeColor<T>> colors)
        {
            foreach (var color in colors)
            {
                var preds = PrevesWhere(color.Node);
                if (preds == null)
                    continue;
                
                foreach (var pred in preds) 
                    pred.Color = color.Color;
            }
        }
    }
}