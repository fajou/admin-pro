using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Admins.Traitements.Generic.Algo.Implementation;
using Admins.Traitements.Generic.Algo.Json;
using Admins.Traitements.Generic.Algo.OtherNode;

namespace Admins.Traitements.Generic.Algo.Method
{
    public class MethodMPR<T> : Graph<T>, IMethodMPR<T> where T : new()
    {

        public T End { get; set; } = new T();
        public T Start { get; set; } = new T();
        
        public Parcours<T> Parcours { get; set; }
        public PreveNode<T>[] Levels { get; set; }
        public PropertyInfo Durration { get; set; }
        public PropertyInfo TaskName { get; set; }
        
        
        public MethodMPR<T> GraphMPR(string durationInfo, string taskNameInfo)
        {
            Build(durationInfo, taskNameInfo);
            return this;
        }

        private void SetDurration(string durration)
        {
            Durration = typeof(T).GetProperty(durration);
            if(Durration == null)
                throw new Exception($"Propertie {durration} Not Found In Object {typeof(T).FullName}");
        }

        private void SetTaskName(string taskName)
        {
            TaskName = typeof(T).GetProperty(taskName);
            if(TaskName == null)
                throw new Exception($"Propertie {taskName} Not Found In Object {typeof(T).FullName}");
        }
        
        private void SetCapacity(PropertyInfo durrationInfo)
        {
            var levels = Levels.OrderByDescending(node => node.Level);
            foreach (var level in levels)
            {
                var preds = PrevesNode(level.Node);
                var prede = PrevesWhere(level.Node).FirstOrDefault();
                
                if (preds == null || prede == null)
                    continue;

                foreach (var pred in preds)
                    pred.Capacity = prede.Flow - Convert.ToDouble(durrationInfo.GetValue(level.Node));
            }
        }
        
        public void Build(string durration, string taskName)
        {
            Parcours = new Parcours<T>(this);
            SetDurration(durration);
            SetTaskName(taskName);   
            
            var i = 1;
            Levels = Parcours.LevelDecomposition().ToArray();
            var levels = Levels.Where(node => node.Level == 1).ToArray();
            
            while (levels.Length != 0)
            {
                foreach (var level in levels)
                {
                    var value = Convert.ToDouble(Durration.GetValue(level.Node) ?? 0);
                    var preds = PrevesNode(level.Node);
                    var preves = PrevesWhere(level.Node);
                    
                    if (preds != null && preds.Length != 0) 
                        value += preds.Max(node => node.Flow);
                    
                    if (preves.Length == 0)
                    {
                        TaskName.SetValue(End, "End");
                        AddArc(End, new PreveNode<T>{Node = level.Node, Level = i, Capacity = value, Flow = value});
                        continue;
                    }
                    
                    foreach (var preve in preves)
                    {
                        preve.Level = i;
                        preve.Flow = value;
                    }
                }

                i++;
                levels = Levels.Where(node => node.Level == i).ToArray();
            }
            SetCapacity(Durration);
        }
       
        public IEnumerable<NodeTask<T>> NodeTasks()
        {
            var keys = Nodes();
            var task = new List<NodeTask<T>>();
            foreach (var key in keys)
            {
                var preds = PrevesWhere(key);

                if (preds.Length == 0)
                {
                    var max = PrevesNode(key).Max();
                    task.Add(new NodeTask<T>
                    {
                        Node = key, 
                        CanStart = max.Flow, 
                        MustStart = max.Capacity,
                        Durration = Durration.GetValue(max.Node)
                    });
                }
                
                foreach (var preve in preds)
                {
                    task.Add(new NodeTask<T>
                    {
                        Node = preve.Node, 
                        CanStart = preve.Flow, 
                        MustStart = preve.Capacity,
                        Durration = Durration.GetValue(preve.Node)
                    });
                    break;
                }
            }

            return task;
        }
        
        public List<NodeTask<T>> CheminCritique()
        {
            var result = new List<PreveNode<T>>();
            var preds = PrevesNode(End);
            
            while (preds != null)
            {
                var aPred = preds.FirstOrDefault(pred => pred.Satured());
                if (aPred != null)
                {
                    result.Add(aPred);
                    preds = PrevesNode(aPred.Node);
                }
                else break;
            }

            return result.Select(node => new NodeTask<T>
            {
                Node = node.Node, 
                CanStart = node.Flow, 
                MustStart = node.Capacity, 
                Durration = Durration.GetValue(node.Node)
            }).ToList();
        }
        
        public new Elements ElementsData()
        {
            var keys = Nodes();
            var nodes = new List<Data>();
            
            var tasks = NodeTasks();
            var critique = CheminCritique();
            
            var data = tasks.Select(task => new Data
            {
                group = "nodes",
                data = new Nodes {
                    id = $"{task.Node}",
                    task = $"{task.Node}[{task.CanStart}|{task.MustStart}]",
                    nodecolor = critique.FirstOrDefault(nodeTask => Equals(nodeTask.Node, task.Node)) != null ? "red" : "blue"
                }
            }).ToArray();
            
            nodes.AddRange(data);
            
            foreach (var key in keys)
            {
                var values = Elements[key];
                if (values == null) continue;
                
                foreach (var value in values)
                {
                    nodes.Add(new Data
                    {
                        group = "edges",
                        data = new Edges
                        {
                            id = $"{key}{value.Node}",
                            source = $"{value.Node}",
                            target = $"{key}",
                            values = $"{Durration?.GetValue(key)}",
                            weight = value.Capacity
                        }
                    });
                }
            }
            return new Elements { Data = nodes.ToArray() };
        }
    }
}