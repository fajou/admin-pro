using System;
using System.Collections.Generic;
using System.Linq;
using Admins.Traitements.Generic.Algo.OtherNode;

namespace Admins.Traitements.Generic.Algo.Method
{
    public class MaximisationFlow<T> : Graph<T>
    {
        private T Source { get; set; }
        
        private T Wood { get; set; }

        private bool Improving(T node, ICollection<PreveNode<T>> chains)
        {
            var preds = PrevesNode(node);
            if (preds == null)
                return chains.Count != 0 && Equals(node, Source);

            foreach (var pred in preds)
            {
                if (pred.Satured()) 
                    continue;    

                chains.Add(pred);
                if (!Improving(pred.Node, chains))
                    continue;
                
                return true;
            }
            chains.Clear();
            return false;
        }
        
        private List<PreveNode<T>> ImprovingChain(T wood)
        {
            var chains = new List<PreveNode<T>>();
            return Improving(wood, chains) ? chains : null;
        }

        public NodeChains<T> FindChains()
        {
            Wood = Nodes().FirstOrDefault(node => DegreePlus(node) == 0);
            Source = Nodes().FirstOrDefault(node => DegredMinus(node) == 0);

            if(Wood == null) 
                throw new Exception("Le Graph n'est un réseau");
            
            var chains = ImprovingChain(Wood);
            if (chains == null || chains.Count == 0) 
                return null;
            
            var min = chains.Min(node => node.FlowLeft());
            foreach (var chain in chains) 
                chain.Flow += min;

            return new NodeChains<T>
            {
                Flow = min,
                Chains = chains
            }; 
        }
    }
}