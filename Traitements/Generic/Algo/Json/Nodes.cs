﻿namespace Admins.Traitements.Generic.Algo.Json
{
    public class Nodes
    {
        public string id { get; set; }
        
        public string nodecolor { get; set; }
        
        public string task { get; set; }
    }
}