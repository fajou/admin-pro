﻿namespace Admins.Traitements.Generic.Algo.Json
{
    public class Data
    {
        public object css { get; set; }
        public object data { get; set; }
        public string group { get; set; }
    }
}