namespace Admins.Traitements.Generic.Algo.Json
{
    public class Edges
    {
        public string id { get; set; }
        
        public double weight { get; set; }
        
        public string values { get; set; }
        
        public string source { get; set; }
        
        public string target { get; set; }
    }
}