using System;
using System.Collections.Generic;
using System.Linq;
using Admins.Traitements.Generic.Algo.Implementation;
using Admins.Traitements.Generic.Algo.Json;
using Admins.Traitements.Generic.Algo.OtherNode;
using Newtonsoft.Json.Linq;

namespace Admins.Traitements.Generic.Algo
{
    public class Graph<T> : IGraph<T>, IAccesFichier<T>
    {
        internal TreeNode<T, List<PreveNode<T>>> Elements { get; set; } = new TreeNode<T, List<PreveNode<T>>>();

        protected static string[] Color => new[] {"#0000FF", "#800020", "#000000", "#0095B6", "#8A2BE2", "#DE5D83", "#CD7F32", "#964B00", "#702963", "#960018", "#DE3163", "#007BA7", "#F7E7CE"};


        public void Add(T node)
        {
            if(!Elements.ContainsKey(node))
                Elements.Add(node, null);
        }
    
        public void Add(T node, List<PreveNode<T>> preds)
        {
            if (Elements.ContainsKey(node))
                if (Elements[node] == null)
                    Elements[node] = preds;
                else
                    Elements[node].AddRange(preds);
            else
                Elements.Add(node, preds);
        }
    
        public void AddArc(T node, T pred, double capacite = 0, double flot = 0)
        {
            if(!Elements.ContainsKey(pred))
                Add(pred);
            Add(node, new List<PreveNode<T>> {new PreveNode<T> {Node = pred, Flow = flot, Capacity = capacite}});
        }

        public void AddArc(T node, PreveNode<T> preve)
        {
            Add(node, new List<PreveNode<T>> {preve});
        }
        
        public void Remove(T node)
        {
            Elements.Remove(node);
            var values = Elements.Values;
            foreach (var preds in values)
            {
                if(preds == null)
                    continue;
                foreach (var preve in preds)
                    if (Equals(preve.Node, node))
                    {
                        preds.Remove(preve);
                        break;
                    }
            }
        }

        public void SetCapacity(string propertyName)
        {
            var keys = Nodes();
            var property = typeof(T).GetProperty(propertyName);
            if(property == null)
                throw new Exception($"Propertie {propertyName} Not Found In Object {typeof(T).FullName}");
            
            foreach (var key in keys)
            {
                var preves = PrevesNode(key);
                if (preves == null)
                    continue;
                foreach (var preve in preves)
                {
                    preve.Capacity = (double) property.GetValue(preve.Node);
                }
            }
        }

        public double Capacite(T node, T pred)
        {
            if (Equals(node, pred))
                return 0.0;
            
            var preds = Elements[node];
            if (preds == null)
                return double.PositiveInfinity;

            foreach (var aPred in preds)
                if (Equals(aPred.Node, pred))
                    return aPred.Capacity;

            return double.PositiveInfinity;
        }

        public T[] Nodes()
        {
            return Elements.Keys.ToArray();
        }

        public PreveNode<T>[] PrevesNode(T node)
        {
            return Elements.ContainsKey(node) ? Elements[node]?.ToArray() : null;
        }

        public T[] NextsNodes(T node)
        {
            var keys = Nodes();
            var res = new List<T>();
            
            foreach (var key in keys)
            {
                var preds = PrevesNode(key);
                if(preds == null)
                    continue;
                if (preds.Any(preve => Equals(preve.Node, node))) 
                    res.Add(key);
            }

            return res.ToArray();
        }

        public PreveNode<T>[] PrevesWhere(T node)
        {
            var nodes = Nodes();
            var preves = new List<PreveNode<T>>();

            foreach (var n in nodes)
            {
                var preds = PrevesNode(n);
                if(preds == null)
                    continue;
                preves.AddRange(preds.Where(pred => Equals(pred.Node, node)));
            }
            return preves.ToArray();
        }

        public PreveNode<T>[] Arcs()
        {
            var preves = new List<PreveNode<T>>();
            var nodes = Nodes();
            
            foreach (var node in nodes)
            {
                var preds = Elements[node];
                if (preds == null)
                    continue;
                foreach (var pred in preds)
                {
                    pred.Source = node;
                    preves.Add(pred);
                }
            }
            return preves.ToArray();
        }

        public Elements ElementsData()
        {
            var keys = Nodes();
            var nodes = new List<Data>();
            
            var data = Nodes().Select(arg => new Data
            {
                group = "nodes",
                data = new Nodes {
                    id = $"{arg}",
                    nodecolor = "#0000FF"
                }
            }).ToArray();
            nodes.AddRange(data);
            
            foreach (var key in keys)
            {
                var values = Elements[key];
                if (values == null)
                    continue;
                
                foreach (var value in values)
                {
                    nodes.Add(new Data
                    {
                        group = "edges",
                        data = new Edges
                        {
                            id = $"{key}{value.Node}",
                            source = $"{value.Node}",
                            target = $"{key}",
                            values = $"[{value.Flow}|{value.Capacity}]",
                            weight = value.Capacity
                        }
                    });
                    Console.WriteLine(value);
                }
            }

            return new Elements { Data = nodes.ToArray() };
        }

        public T[] Adjacents(T node)
        {
            var adjs = new List<T>();
            var nodes = PrevesNode(node);
            
            if (nodes != null) 
                adjs.AddRange(nodes.Select(pred => pred.Node));

            var keys = Elements.Keys;
            foreach (var key in keys)
            {
                var preds = Elements[key];
                if (preds == null) 
                    continue;
                
                foreach (var preve in preds)
                    if (Equals(preve.Node, node) && !adjs.Contains(key))
                        adjs.Add(key);
            }

            return adjs.ToArray();
        }

        public bool IsAdjacents(T node, T node1)
        {
            return Adjacents(node).Contains(node1);
        }

        public string Preds(T node)
        {
            var preves = PrevesNode(node);
            return preves == null
                ? string.Empty
                : preves.Aggregate(string.Empty, (current, preve) => current + $"{preve.Node},");
        }

        public TreeNode<T, List<PreveNode<T>>> GetTreeNodes()
        {
            return Elements;
        }

        public int DegreePlus(T node)
        {
            var keys = Elements.Keys;
            var size = 0;
            foreach (var key in keys)
            {
                var preds = Elements[key];
                if (preds == null) 
                    continue;
                
                foreach (var preve in preds)
                    if (Equals(preve.Node, node))
                        size++;
            }

            return size;
        }

        public int DegredMinus(T node)
        {
            return Elements[node]?.Count ?? 0;
        }

        public void WriteGraph(Graph<T> graph, string path)
        {
            var json = JObject.FromObject(graph.Elements);
        }

        public Graph<T> ReadGraph(string path)
        {
            throw new NotImplementedException();
        }
    }
}