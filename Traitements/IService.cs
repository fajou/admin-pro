namespace Admins.Traitements
{
    /// <summary>
    /// Interface indicating that type is a service suitable to be managed by DI container.
    /// </summary>
    public interface IService
    {

    }
}