using Admins.Traitements.Generic.Admins;
using Admins.Traitements.Generic.Base;

namespace Admins.Traitements.Models
{
    public class FormViewModel
    {
        public string UrlAction { get; set; } = "/Admins/Traiter";
        
        public string UrlAfter { get; set; }
               
        public bool Update { get; set; }

        public bool IsPartial { get; set; }
        
        public string BoxTitle { get; set; }
        
        public string BtnText { get; set; } = "Valider";
        
        public string ErrorMessage { get; set; }
        
        public string SuccessMessage { get; set; }
        
        public BaseModel Objet { get; set; }

        public string MethodAction { get; set; } = "POST";

        public string BtnClCss { get; set; } = "btn btn-xs btn-primary";

        public string BtnTextNouveau { get; set; } = "Nouveau";
        
        public Menus CurrentMenus { get; set; }
    }
}