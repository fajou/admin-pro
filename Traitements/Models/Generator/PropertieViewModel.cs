using System.Collections.Generic;
using Admins.Traitements.Generic.Generator.Base;

namespace Admins.Traitements.Models.Generator
{
    public class PropertieViewModel : FormViewModel
    {
        public IEnumerable<BasePropertie> Properties { get; set; }

        public static string Url(string url, string id)
        {
            return $"{url}&Id={id}";
        }
    }
}