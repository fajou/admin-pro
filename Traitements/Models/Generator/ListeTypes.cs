using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;

namespace Admins.Traitements.Models.Generator
{
    [ListDeletable]
    [ListEditable]
    [Lists("Liste des types")]
    [Forms("Nouveau type", "Modification du type", SaveAndCreateAnOther = true)]
    [TableInfo("BASE_TYPES", "ID_BASE_TYPES", "")]
    public class ListeTypes : BaseModel
    {
        [Champ]
        public string TypeName { get; set; }

        [Champ]
        public string TypeBase { get; set; }
    }
}