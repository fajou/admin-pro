using System;
using System.Collections.Generic;
using System.Web;
using Admins.Traitements.Generic.Base;

namespace Admins.Traitements.Models
{
    public class ListViewModel
    {
        public Type BaseModelForms { get; set; }
        
        public int TotalItems { get; set; }

        public int PageIndex { get; set; } = 1;
        
        public int MaxItems { get; set; } = 10;
        
        public bool InPage { get; set; }

        public bool IsChoix { get; set; }
        
        public Type TypeModel { get; set; }
        
        public IHtmlString BoxTitle { get; set; }
        
        public List<BaseModel> Modeles { get; set; }
        
        public BaseModel SearchModel { get; set; }
    }
}