using Admins.Traitements.Generic.Base;

namespace Admins.Traitements.Models
{
    public class PreviewViewModel
    {
        public string TitleDetails { get; set; }
        
        public BaseModel BaseModel { get; set; }
        
        public string ErrorMessage { get; set; }
    }
}