using System;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;

namespace Admins.Traitements.Models
{
    [HideMenuAcces]
    [Forms("Changé la base de donnée")]
    public class DbModel : BaseModel
    {
        private string _dbName = "Graph";

        [Champ(Label = "Nom de la base de donnée")]
        public string DbName
        {
            get { return _dbName; }
            set
            {
                if(string.IsNullOrEmpty(value))
                    throw new Exception("Nom de la base de donnée ne peut pas être null ou vide");
                _dbName = value;
            }
        }
    }
}