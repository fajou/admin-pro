using System.Linq;
using Admins.Traitements.Generic.Admins.Users;
using Admins.Traitements.Services.Repository;

namespace Admins.Traitements.Services
{
    public class AccountService : IAccountService
    {
        private readonly IDataBaseModel _model;

        public AccountService(IDataBaseModel model)
        {
            _model = model;
        }

        public Utilisateur GetUtilisateur(string email, string pass)
        {
            return (Utilisateur) _model.Find(new Utilisateur(), $"Email='{email}' and Pass='{pass}'").FirstOrDefault();
        }
    }
}