﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Admins.Traitements.Generic.Base;

namespace Admins.Traitements.Services.Repository
{
    public interface IDataBaseModel : IRepository
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="bm"></param>
        /// <param name="conn"></param>
        void Save(BaseModel bm, SqlConnection conn);

        /// <summary>
        ///
        /// </summary>
        /// <param name="bm"></param>
        void Save(BaseModel bm);

        /// <summary>
        ///
        /// </summary>
        /// <param name="modeles"></param>
        /// <param name="conn"></param>
        void SaveList(List<BaseModel> modeles, SqlConnection conn);

        /// <summary>
        ///
        /// </summary>
        /// <param name="modeles"></param>
        void SaveList(List<BaseModel> modeles);

        /// <summary>
        ///
        /// </summary>
        /// <param name="bm"></param>
        /// <param name="connection"></param>
        void Update(BaseModel bm, SqlConnection connection);

        /// <summary>
        ///
        /// </summary>
        /// <param name="bm"></param>
        void Update(BaseModel bm);

        /// <summary>
        ///
        /// </summary>
        /// <param name="bm"></param>
        /// <param name="con"></param>
        void UpdateList(List<BaseModel> bm, SqlConnection con);

        /// <summary>
        ///
        /// </summary>
        /// <param name="bm"></param>
        void UpdateList(List<BaseModel> bm);

        /// <summary>
        ///
        /// </summary>
        /// <param name="bm"></param>
        /// <param name="conn"></param>
        void Delete(BaseModel bm, SqlConnection conn);

        /// <summary>
        ///
        /// </summary>
        /// <param name="bm"></param>
        /// <param name="conn"></param>
        void DeleteList(List<BaseModel> bm, SqlConnection conn);

        /// <summary>
        ///
        /// </summary>
        /// <param name="bm"></param>
        void DeleteList(List<BaseModel> bm);

        /// <summary>
        ///
        /// </summary>
        /// <param name="bm"></param>
        void Delete(BaseModel bm);

        /// <summary>
        ///
        /// </summary>
        /// <param name="bm"></param>
        /// <param name="conn"></param>
        void DeleteAll(BaseModel bm, SqlConnection conn);

        /// <summary>
        ///
        /// </summary>
        /// <param name="bm"></param>
        void DeleteAll(BaseModel bm);

        /// <summary>
        ///
        /// </summary>
        /// <param name="bm"></param>
        /// <param name="conn"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        List<BaseModel> Find(BaseModel bm, SqlConnection conn, string where = "");

        /// <summary>
        ///
        /// </summary>
        /// <param name="bm"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        List<BaseModel> Find(BaseModel bm, string where = "");

        /// <summary>
        ///
        /// </summary>
        /// <param name="bm"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        BaseModel FindById(BaseModel bm, string id);

        /// <summary>
        ///
        /// </summary>
        /// <param name="requette"></param>
        /// <param name="bm"></param>
        /// <returns></returns>
        List<BaseModel> FindQuery(string requette, BaseModel bm);

        /// <summary>
        ///
        /// </summary>
        /// <param name="requette"></param>
        /// <param name="bm"></param>
        /// <param name="conn"></param>
        /// <returns></returns>
        List<BaseModel> FindQuery(string requette, BaseModel bm, SqlConnection conn);

        /// <summary>
        ///
        /// </summary>
        /// <param name="bm"></param>
        /// <param name="conn"></param>
        void Count(BaseModel bm, SqlConnection conn);

        /// <summary>
        ///
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        BaseModel First(BaseModel model);
    }
}