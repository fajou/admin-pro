using System;
using System.Collections.Generic;
using System.Reflection;
using Admins.Traitements.Generic.Util;

namespace Admins.Traitements.Services.Repository
{
    public interface IBaseModel
    {
        PropertyInfo[] Properties();

        PropertyInfo[] TableColumnProperties();

        string IdName();
        
        string IdChart(string unique);

        string BaseTypeName();

        string GetTableName();

        string SequenceName();

        string PredicasName();

        string BoxTitle();

        string PreviewTitle();

        string ListBoxTitle();

        object CustomValue(PropertyInfo property, bool numberFormater = false, bool searchField = false, bool inputFeald = false);

        string BuildChamp(PropertyInfo property, string cl = Use.ClFormGroup, bool searchInput = false, bool inputFeald = false);
        
        Dictionary<string, string> DicoForOption();

        bool IsModelAction();

        bool IsDeletable();

        bool IsEditable();

        bool IsSaveAndCreateAnOther();

        bool IsHorizontalForms();

        Type TypeDeletable();

        Type TypeEditable();

        Type GetBaseModelForms();
        
    }
}