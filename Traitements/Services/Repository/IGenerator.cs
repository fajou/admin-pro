using System.Collections.Generic;
using Admins.Traitements.Generic.Generator.Base;

namespace Admins.Traitements.Services.Repository
{
    public interface IGenerator : IRepository
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="classId"></param>
        /// <returns></returns>
        BaseClasse GetBaseClasse(string classId);

        /// <summary>
        ///
        /// </summary>
        /// <param name="classId"></param>
        /// <returns></returns>
        IEnumerable<BasePropertie> GetProperties(string classId);

        /// <summary>
        ///
        /// </summary>
        /// <param name="propertieId"></param>
        /// <returns></returns>
        IEnumerable<BaseAttribute> GetAttributes(string propertieId);
    }
}