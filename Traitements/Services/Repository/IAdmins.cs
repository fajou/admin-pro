using Admins.Traitements.Generic.Base;

namespace Admins.Traitements.Services.Repository
{
    public interface IAdmins : IRepository
    {
        void UpdateModel(BaseModel model);

        void DeleteModel(BaseModel model);

        int CountModel(BaseModel model);
    }
}