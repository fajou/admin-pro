using Admins.Traitements.Generic.Admins.Users;

namespace Admins.Traitements.Services.Repository
{
    public interface IAccountService : IRepository
    {
        Utilisateur GetUtilisateur(string email, string pass);
    }
}