namespace Admins.Traitements.Services.Repository
{
    public interface IBaseXml : IRepository
    {
        /// <summary>
        /// permet de convertir un objet de couche IBaseXml en string.
        /// </summary>
        /// <param name="baseXml"></param>
        /// <param name="path"></param>
        /// <returns>string</returns>
        void SaveToXml(IBaseXml baseXml, string path);
        
        /// <summary>
        /// Lecture d'un ficiher xml à partir d'un chemin specifier.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        IBaseXml ReadBaseXml(string path);

        /// <summary>
        /// 
        /// </summary>
        /// <returns>string format xml</returns>
        string ToXml();
    }
}