namespace Admins.Traitements.Services.Repository
{
    public interface IBaseChart : IRepository
    {
        object DataChart(string format);

        decimal TotalDefault();
    }
}