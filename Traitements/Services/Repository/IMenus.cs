using Admins.Traitements.Generic.Admins;
using Admins.Traitements.Generic.Algo;

namespace Admins.Traitements.Services.Repository
{
    public interface IMenusService : IService
    {
        Graph<Menus> GraphMenus(int level);

        void GenerateAppJson(string path);
    }
}