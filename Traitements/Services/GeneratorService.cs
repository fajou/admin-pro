using System.Collections.Generic;
using System.Linq;
using Admins.Traitements.Generic.Generator.Base;
using Admins.Traitements.Services.Repository;

namespace Admins.Traitements.Services
{
    public class GeneratorService : IGenerator
    {
        private readonly IDataBaseModel _model;

        public GeneratorService(IDataBaseModel model)
        {
            _model = model;
        }

        public BaseClasse GetBaseClasse(string classId)
        {
            return (BaseClasse) _model.FindById(new BaseClasse(), classId);
        }

        public IEnumerable<BasePropertie> GetProperties(string classId)
        {
            return _model.Find(new BasePropertie(), $"IdClass='{classId}'")?.Cast<BasePropertie>();
        }

        public IEnumerable<BaseAttribute> GetAttributes(string propertieId)
        {
            return _model.Find(new BasePropertie(), $"PropertieId='{propertieId}'").Cast<BaseAttribute>();
        }
    }
}