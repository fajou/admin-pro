using System.Collections.Generic;
using System.Linq;
using System.Web.Helpers;
using Admins.Traitements.Generic.Admins;
using Admins.Traitements.Generic.Admins.Json;
using Admins.Traitements.Generic.Algo;
using Admins.Traitements.Generic.Algo.Implementation;
using Admins.Traitements.Generic.Context;
using Admins.Traitements.Generic.Util;
using Admins.Traitements.Services.Repository;

namespace Admins.Traitements.Services
{
    public class MenusService : IMenusService
    {
        private readonly IDataBaseModel _model;

        public MenusService(IDataBaseModel model)
        {
            _model = model;
        }

        public Graph<Menus> GraphMenus(int level)
        {
            var menus = _model.Find(new Menus(), $"{level} <= Levels").Cast<Menus>().ToArray();
            var graph = new Graph<Menus>();
            foreach (var menu in menus)
            {
                //Trouver le parent d'un menu.
                var  parent = menu.IdParent == null ? null : menus.FirstOrDefault(m => m.Id == menu.IdParent);
                if (parent == null)
                    graph.Add(menu);
                else
                    graph.AddArc(menu, parent);
            }

            return graph;
        }

        private static MenuJsonInfo MenuParentInfo(Menus parent, object content)
        {
            return new MenuJsonInfo
            {
                MainMenuChild = content != null,
                MainMenuContent = content,
                MainMenuDesc = parent.Label,
                MainMenuIcon = parent.IconClass,
                MainMenuId = parent.Id,
                MainMenuTitle = parent.Label,
                MainMenuUrl = Use.UrlMenu(parent)
            };
        }
        
        private static MenuChildJsonInfo MenuChildInfo(Menus parent)
        {
            return new MenuChildJsonInfo
            {
                SubMenuDesc = parent.Label,
                SubMenuIcon = parent.IconClass,
                SubMenuId = parent.Id,
                SubMenuTitle = parent.Label,
                SubMenuUrl = Use.UrlMenu(parent)
            };
        }

        private static List<object> MenuChildInfo(IEnumerable<Menus> parents, IGraph<Menus> graph)
        {
            var infos = new List<object>();
            foreach (var parent in parents)
            {
                var fils = graph.NextsNodes(parent);
                if (fils == null || !fils.Any())
                    infos.Add(MenuChildInfo(parent));
                else
                    infos.Add(MenuParentInfo(parent, MenuChildInfo(fils, MenusContext.MenuGraph)));
            }

            return infos;
        }
        
        public void GenerateAppJson(string path)
        {
            var menus = MenusContext.MenuGraph;
            if(menus == null)
                return;

            var parents = MenusContext.MenuGraph.Nodes().Where(node => MenusContext.MenuGraph.PrevesNode(node) == null);
            var module = new ModuleJsonInfo
            {
                ModuleName = "Liste menus disponible"
            };
            
            var moduleContents = new List<object>();
            foreach (var parent in parents)
            {
                var fils = MenusContext.MenuGraph.NextsNodes(parent);
                if (fils == null || !fils.Any())
                    moduleContents.Add(MenuChildInfo(parent));
                else
                    moduleContents.Add(MenuParentInfo(parent, MenuChildInfo(fils, MenusContext.MenuGraph)));
            }

            module.ModuleData = moduleContents;
            JsonReadWrite.WWW_ROOT = path;
            JsonReadWrite.Write("app.json", "", Json.Encode(new[]
            {
                new AppJsonInfo
                {
                    AppAuthor = "Olivier",
                    AppDesc = "Admins application",
                    AppLink = "/",
                    AppLogo = "faveicon.ico",
                    AppName = "Next Admins",
                    AppVersion = "242",
                    AppWebsite = "admins.somee.com",
                    AuthorWebsite = "Admin",
                    DataContent = new[] {module}
                }
            }));
        }
    }
}