using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Admins.Traitements.Generic.Base;
using Admins.Traitements.Generic.Html;
using Admins.Traitements.Generic.Search;
using Admins.Traitements.Generic.Util;
using Admins.Traitements.Models;

namespace Admins.Traitements.Action
{
    public class BaseController : Controller
    {
        protected ActionResult ViewProtected(ActionResult view)
        {
            return Session?[ConstName.USER_SESS] == null ? RedirectToAction("Login", "Account") : view;
        }

        protected void ActionProtected()
        {
            if(Session?[ConstName.USER_SESS] == null)
            {
                Response.Redirect("/Account/Login");
            }
        }

        protected ActionResult ViewError(string message)
        {
            return View("Pages/Generic/_ErrorType", (object) message);
        }

        protected ActionResult ViewForms(FormViewModel model)
        {
            return View("Pages/Generic/_Forms", model);
        }

        protected PartialViewResult PartialViewForms(FormViewModel model)
        {
            return PartialView("Pages/Generic/Forms/_FormsBox", model);
        }

        protected ActionResult ViewLists(ListViewModel model)
        {
            return View("Pages/Generic/_Listes", model);
        }

        protected PartialViewResult PartialViewLists(ListViewModel model)
        {
            return PartialView("Pages/Generic/_Listes", model);
        }

        protected PartialViewResult PartialViewError(string message)
        {
            return PartialView("Pages/Generic/_ErrorType", message);
        }

        protected ActionResult PreviewDetails(PreviewViewModel preview)
        {
            return View("Pages/Generic/_Preview", preview);
        }
        
        /*
        * Set The Data Base Model Dynamic, With Control, Froms POST or GET Request.
        */
        protected void SetBaseModel(BaseModel model)
        {
            var properties = model.Properties();
            foreach (var property in properties)
            {
                var name = BaseModel.NameChamp(property);
                if(name == null) continue;
                model[property.Name] = Request.Form[name];
            }
        }
        
        /*
         * @return BaseModele For Search or An Instance
         */
        protected BaseModel GetBaseModele()
        {
            var cl = Request[Input.ClassName];
            var type = Type.GetType(cl);
                
            if (type == null)
                throw new Exception("Impossible de charger les données");
            var model = (BaseModel) Activator.CreateInstance(type);
            return model;
        }

        /**
         * Set all field searchable in basemodel properties *
         */
        protected void SetSearchFieldBaseModel(BaseModel model)
        {
            if (string.IsNullOrEmpty(Request.QueryString[Input.SearchName]))
                return;
            try
            {
                /* Block For Search */
                model.OrderBy.Columns = Request[Input.OrderByName];
                model.OrderBy.OrderType = Request[Input.OrderTypeName];
                var properties = model.Properties().Where(info => !BaseModel.Hide(info));
                foreach (var property in properties)
                {
                    try
                    {
                        var name = BaseModel.NameChamp(property);
                        var query = Request.QueryString[name];

                        if(string.IsNullOrEmpty(query))
                            continue;
                        model[property.Name] = query;
                        model.SearchFields.Add(new SearchField(property, query));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }

                model.InSearch = true;
            }
            catch (Exception e)
            {
                throw e.GetBaseException();
            }
        }

        public ActionResult Redierct(string toUrl)
        {
            var index = toUrl.IndexOf('?');
            string queryString = null;
            var url = toUrl;
            if (index != -1)
            {
                url = toUrl.Substring(0, index);
                queryString = toUrl.Substring(index + 1);
            }

            var request = new HttpRequest(null, url, queryString);
            var response = new HttpResponse(new StringWriter());
            var httpContext = new HttpContext(request, response);
            var routeData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(httpContext));

            var values = routeData?.Values;
            var controllerName = values?["controller"];
            var actionName = values?["action"];
            var areaName = values?["area"];
            return RedirectToAction((string) actionName, (string) controllerName, areaName);
        }

    }
} 