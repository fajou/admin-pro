using System.Web.Mvc;
using Admins.Traitements.Generic.Admins.Users;
using Admins.Traitements.Generic.Util;
using Admins.Traitements.Services.Repository;

namespace Admins.Traitements.Action
{
    public class AccountController : Controller
    {
        private readonly IAccountService _mAccountService;

        public AccountController(IAccountService mAccountService)
        {
            _mAccountService = mAccountService;
        }

        // GET
        public ActionResult Login()
        {
            return Session?[ConstName.USER_SESS] != null
                ? (ActionResult) RedirectToAction("Index", "Admins")
                : View("Pages/Utilisateur/_Login");
        }

        [ValidateAntiForgeryToken]
        public ActionResult Utilisateur(Utilisateur model)
        {
            var utilisateur = _mAccountService.GetUtilisateur(model.Email, model.Pass);
            if (utilisateur != null)
            {
                Session[ConstName.USER_SESS] = utilisateur;
                ApplicationContextInitializing.Initialize(Session);
                return RedirectToAction("Index", "Admins");
            }

            ViewBag.ErreurLogin = "Invalide Adresse E-mail ou Mots de Passe";
            return Login();
        }

        public ActionResult Deconnexion()
        {
            Session?.Clear();
            return Login();
        }
    }
}