using System;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Admins.Traitements.Generic.Base;
using Admins.Traitements.Generic.Context;
using Admins.Traitements.Generic.Html;
using Admins.Traitements.Generic.Util;
using Admins.Traitements.Models;
using Admins.Traitements.Services.Repository;
using static Admins.Traitements.Generic.Util.Use;

namespace Admins.Traitements.Action
{
    public class AdminsController : BaseController
    {
        private readonly IDataBaseModel _model;

        public AdminsController(IDataBaseModel model)
        {
            _model = model;
        }

        public ActionResult Index()
        {
            ActionProtected();
            return View("Pages/Apps/_AccessContentInfo", Session[ConstName.USER_SESS]);
        }

        public ActionResult Home()
        {
            if (Session?[ConstName.USER_SESS] != null)
                return View("Pages/Apps/_AccessContentInfo", Session[ConstName.USER_SESS]);
            return RedirectToAction("Login", "Account");
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Db()
        {
            try
            {
                ActionProtected();
                var model = GetBaseModele();
                SetBaseModel(model);
                Connexion.InitialCatalog = ((DbModel) model).DbName;
                Connexion.GetConnection();
                
                Session?.Clear();
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                var mes = e is SqlException ? $"Erreur de la connexion à la base de donnée {Connexion.InitialCatalog}" : e.GetBaseException().Message;
                return ViewError(mes);
            }
        }
        
        public ActionResult ChangeDB()
        {
            ActionProtected();
            var model = new DbModel
            {
                DbName = Connexion.InitialCatalog
            };
            return ViewForms(new FormViewModel
            {
                UrlAction = Url.Action("Db"),
                Objet = model,
                BoxTitle = model.BoxTitle(),
                BtnText = "Changer",
                BtnClCss = "btn btn-danger"
            });
        }

        private ListViewModel GetListViewModel(bool inPage, bool isChoix)
        {
            var itemsMax = Request[Input.ItemsMax];
            var pageIndex = Request[Input.PageIndex];
            int items;
            if (!int.TryParse(itemsMax, out items))
                items = ConstInt.ItemMax;


            var index = string.IsNullOrEmpty(pageIndex) ? 1 : int.Parse(pageIndex);
            var model = GetBaseModele();
            SetSearchFieldBaseModel(model);
            var reqWhere = model.InSearch 
                ? Query.SearchQuery(model, index, items, model.OrderBy.Query()) 
                : $"1=1 {Query.Paging(index, items, model.OrderBy.Query())}";
            
            var models = _model.Find(model, reqWhere);
            
            return new ListViewModel
            {
                BoxTitle = new HtmlString(model.ListBoxTitle()),
                Modeles = models,
                TotalItems = model.Size,
                TypeModel = model.GetType(),
                MaxItems = items,
                PageIndex = index,
                InPage = inPage, 
                IsChoix = isChoix,
                SearchModel = model, 
                BaseModelForms = model.GetBaseModelForms()
            };
        }

        private FormViewModel GetFormViewModel()
        {
            ActionProtected();
            var id = Request.QueryString[Input.IdName];
            var model = GetBaseModele();

            if (!string.IsNullOrEmpty(id))
                model = _model.FindById(model, id) ?? model;

            return new FormViewModel
            {
                UrlAfter = model.UrlAfter(MenusContext.Current),
                Objet = model,
                Update = !string.IsNullOrEmpty(model.Id),
                BoxTitle = model.BoxTitle()
            };
        }

        public ActionResult Preview()
        {
            try
            {
                var model = GetBaseModele();
                model.Id = Request[Input.IdName];
                var result = _model.FindById(model, model.Id);

                var preview = new PreviewViewModel();
                if (result == null)
                {
                    preview.TitleDetails = model.PreviewTitle();
                    preview.BaseModel = model;
                    preview.ErrorMessage = "Impossible de trouver l'information que vous cherchiez";
                }
                else
                {
                    preview.TitleDetails = result.PreviewTitle();
                    preview.BaseModel = result;
                }

                return ViewProtected(PreviewDetails(preview));
            }
            catch (Exception e)
            {
                return ViewError(e.GetBaseException().Message);
            }
        }
        
        /*
         * Return Liste BaseModel For Modal POP UP
         */
        public PartialViewResult ListesView()
        {
            try
            {
                return PartialViewLists(GetListViewModel(false, true));
            }
            catch (Exception e)
            {
                return PartialViewError(e.GetBaseException().Message);
            }
        }

        /*
         * Return Liste BaseModel For Page Liste.
         */
        public ActionResult PageListes()
        {
            try
            {
                return ViewProtected(ViewLists(GetListViewModel(true, false)));
            }
            catch (Exception e)
            {
                return ViewError(e.GetBaseException().Message);
            }
        }

        /*
         * TODO : Add AntiForgeryToken
         */
        public ActionResult Del()
        {
            try
            {
                ActionProtected();
                var model = GetBaseModele();
                model.Id = Request[Input.IdName];
                _model.Delete(model);
                return PageListes();
            }
            catch (Exception e)
            {
                return ViewError(e.Message);
            }
        }

        public ActionResult Forms()
        {
            try
            {
                return ViewForms(GetFormViewModel());
            }
            catch (Exception e)
            {
                return ViewError(e.Message);
            }
        }

        public ActionResult PageForms()
        {
            try
            {
                return PartialViewForms(GetFormViewModel());
            }
            catch (Exception e)
            {
                return PartialViewError(e.Message);
            }
        }
        
        [ValidateAntiForgeryToken]
        public ActionResult Traiter() 
        {
            BaseModel model = null;
            try
            {
                ActionProtected();
                model = GetBaseModele();
                SetBaseModel(model);
                
                if(string.IsNullOrEmpty(Request.Form[Input.IdName]))
                    _model.Save(model);
                else
                {
                    model.Id = Request.Form[Input.IdName];
                    _model.Update(model);    
                }

                if (model.IsSaveAndCreateAnOther())
                {
                    return ViewForms(new FormViewModel
                    {
                        UrlAfter = Request.Form[Input.UrlNameApres],
                        Objet = model,
                        SuccessMessage = "Donnée sauvegarder avec succées",
                        Update = !string.IsNullOrEmpty(model.Id),
                        BoxTitle = Request.Form[Input.BoxNameTitle],
                        BtnText = Request.Form[Input.BoxNameButton],
                        CurrentMenus = MenusContext.Current
                    });
                }
                return Redirect(Request.Form[Input.UrlNameApres]);
            }
            catch (Exception e)
            {
                if (model == null)  return ViewError(Request.Form[Input.ClassName]);

                return ViewForms(new FormViewModel
                {
                    UrlAfter = Request.Form[Input.UrlNameApres],
                    Objet = model,
                    ErrorMessage = e.GetBaseException().Message,
                    Update = !string.IsNullOrEmpty(model.Id),
                    BoxTitle = Request.Form[Input.BoxNameTitle],
                    BtnText = Request.Form[Input.BoxNameButton],
                    CurrentMenus = MenusContext.Current
                });
            }
        }
        
        public string RenderRazorViewToString(string viewName, BaseModel model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ImageUploader()
        {
            try
            {
                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file];
                    if (fileContent == null || fileContent.ContentLength <= 0)
                        continue;
                    
                    var stream = fileContent.InputStream;
                    var fileName = Path.GetFileName(file);
                    var path = Path.Combine(Server.MapPath(ImagePath), FileName(fileName, Request[Input.DosName]));

                    Directory.CreateDirectory(Path.Combine(Server.MapPath(ImagePath), Request[Input.DosName]));
                    
                    using (var fileStream = System.IO.File.Create(path))
                    {
                        stream.CopyTo(fileStream);
                    }
                }
            }
            catch (Exception e)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json($"Upload failed {e.Message}");
            }

            return Json("File uploaded successfully");
        }
    }
}
