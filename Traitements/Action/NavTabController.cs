using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Admins.Traitements.Generic.Admins.Json;
using Newtonsoft.Json;

namespace Admins.Traitements.Action
{
    public class NavTabController : BaseController
    {
        private string RequestBody()
        {
            var request = Request.InputStream;
            request.Seek(0, SeekOrigin.Begin);
            return new StreamReader(request).ReadToEnd();
        }
        public PartialViewResult MainTab()
        {
            try
            {
                //var data = Request[ConstName.TAB];
                var json = RequestBody();
                var tabs = string.IsNullOrEmpty(json)
                    ? new TabNavigationJsonInfo[0]
                    : JsonConvert.DeserializeObject<TabNavigationJsonInfo[]>(json).ToArray();

                return PartialView("Pages/Generic/Collapse/_NavTabMain", tabs);
            }
            catch (Exception e)
            {
                return PartialViewError(e.Message);
            }
        }

        public PartialViewResult NavTab(TabNavigationJsonInfo info)
        {
            info = info ?? JsonConvert.DeserializeObject<TabNavigationJsonInfo>(RequestBody());
            return PartialView("Pages/Generic/Collapse/_NavTabHeader", info);
        }

        public PartialViewResult NavTabContent(TabNavigationJsonInfo info)
        {
            info = info ?? JsonConvert.DeserializeObject<TabNavigationJsonInfo>(RequestBody());
            return PartialView("Pages/Generic/Collapse/_NavTabContent", info);
        }
    }
}