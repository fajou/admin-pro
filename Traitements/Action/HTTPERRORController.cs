using System.Web.Mvc;

namespace Admins.Traitements.Action
{
    public class HTTPERRORController : Controller
    {
        // GET
        public ActionResult PageNotFound()
        {
            return View("Pages/HTTPError/_PageNotFound");
        }

        public ActionResult PageError500()
        {
            return View("Pages/HTTPError/_InternalError");
        }
    }
}