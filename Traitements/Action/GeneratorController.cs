using System;
using System.Web.Mvc;
using Admins.Traitements.Generic.Context;
using Admins.Traitements.Generic.Generator.Base;
using Admins.Traitements.Models;
using Admins.Traitements.Models.Generator;
using Admins.Traitements.Services.Repository;

namespace Admins.Traitements.Action
{
    public class GeneratorController : BaseController
    {
        private readonly IGenerator _generator;
        private readonly IDataBaseModel _model;
        public GeneratorController(IGenerator generator, IDataBaseModel model)
        {
            _generator = generator;
            _model = model;
        }

        // GET
        public ActionResult Index()
        {
            ActionProtected();
            var model = new BaseClasse();
            return ViewForms(new FormViewModel
            {
                Objet = model,
                BtnText = "Suivant",
                UrlAction = Url.Action("SaveBaseClass"),
                CurrentMenus = MenusContext.Current,
                BtnTextNouveau = string.Empty,
                Update = !string.IsNullOrEmpty(model.Id),
                BoxTitle = model.BoxTitle()
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveBaseClass()
        {
            try
            {
                var model = GetBaseModele();
                SetBaseModel(model);
                _model.Save(model);
                return RedirectToAction("BaseFields", new {clId = model.Id});
            }
            catch (Exception e)
            {
                return ViewError(e.Message);
            }
        }

        public ActionResult BaseFields(string clId)
        {
            var model = new BasePropertie {IdClass = clId};

            return View("Pages/Generator/BaseFields", new PropertieViewModel
            {
                UrlAfter = Request.Url?.ToString(),
                Objet = model,
                BtnText = "Sauver",
                CurrentMenus = MenusContext.Current,
                BtnTextNouveau = "Nouveau",
                Update = !string.IsNullOrEmpty(model.Id),
                BoxTitle = model.BoxTitle(),
                Properties = _generator.GetProperties(clId)
            });
        }
    }
}