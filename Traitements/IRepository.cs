namespace Admins.Traitements
{
    /// <summary>
    /// Interface indicating that type is a repositorie suitable to be managed by DI container.
    /// </summary>
    public interface IRepository
    {
        
    }
}