using System.Web;
using Admins.Traitements.Generic.Context;

namespace Admins
{
    public static class ApplicationContextInitializing
    {
        public static void Initialize(HttpSessionStateBase session)
        {
            MenusContext.SessionContext = session;
        }
    }
}