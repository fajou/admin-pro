using System;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;

namespace Admins.Models.Gestions.ViewData
{
    public class ViewPointages : BaseModel
    {
        public string Nom { get; set; }
        
        [Hide]
        public string IdPointage { get; set; }
        
        [Hide]
        public DateTime DebutDimanche { get; set; }
        
        [Hide]
        public DateTime FinSamedi { get; set; }
        
        [Hide]
        public string IdEmploye { get; set; }
        
        public string Dimanche { get; set; }
        
        public string Lundi { get; set; }
        
        public string Mardi { get; set; }
        
        public string Mercredi { get; set; }
        
        public string Jeudi { get; set; }
        
        public string Vendredi { get; set; }
        
        public string Samedi { get; set; }
        
        public string Total { get; set; }
    }
}