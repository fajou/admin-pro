using System;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;

namespace Admins.Models.Gestions
{
    [Forms("Saisie Pointage")]
    public class Pointages : BaseModel
    {

        [Champ(Id = "ChoixEmployeId", Choix = true, TypeModel = typeof(Employe), Label = "Choisir un(e) employe")]
        public string IdEmploye { get; set; }
        
        [Champ(Id = "IdDebutDimanche", Type = "date", Label = "Debut Semaine")]
        public DateTime DebutDimanche { get; set; }
        
        [Champ(Id = "IdFinSamedi", Type = "date", Label = "Fin Semaine")]
        public DateTime FinSamedi { get; set; }
        
        [Champ(Label = "Dimanche")]
        public string Dimanche { get; set; }
        
        [Champ(Label = "Lundi")]
        public string Lundi { get; set; }
        
        [Champ(Label = "Mardi")]
        public string Mardi { get; set; }
        
        [Champ(Label = "Mercredi")]
        public string Mercredi { get; set; }
        
        [Champ(Label = "Jeudi")]
        public string Jeudi { get; set; }
        
        [Champ(Label = "Vendredi")]
        public string Vendredi { get; set; }
        
        [Champ(Label = "Samedi")]
        public string Samedi { get; set; }
        
        [Champ(Label = "Total")]
        public string Total { get; set; }
    }
}