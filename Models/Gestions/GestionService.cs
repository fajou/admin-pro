using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using Admins.Models.Gestions.Conf;
using Admins.Models.Gestions.Util;
using Admins.Models.Gestions.ViewData;
using Admins.Traitements.Generic.Base;
using Admins.Traitements.Generic.Util;
using Admins.Traitements.Services.Repository;

namespace Admins.Models.Gestions
{
    public class GestionService : IGestionService
    {
        private readonly IDataBaseModel _dbo;

        public GestionService(IDataBaseModel dbo)
        {
            _dbo = dbo;
        }

        public void Save(BaseModel bm)
        {
            _dbo.Save(bm);
        }

        public List<BaseModel> Employes()
        {
            return _dbo.Find(new Employe());
        }

        public List<ViewPointages> LViewPointages(DateTime date)
        {
            var requette =
                $"select e.Id as IdPointage, e.Nom, p.* from Employe e left join (select p.* from Pointages p where 0 <= DATEDIFF(DAY, DebutDimanche, '{date:u}') and 0 <= DATEDIFF(DAY, '{date:u}', FinSamedi)) as p on p.IdEmploye = e.Id";
            
            return _dbo.FindQuery(requette, new ViewPointages()).Cast<ViewPointages>().ToList();
        }

        public void SaveListe(List<ViewPointages> modeles)
        {
            SqlConnection conn = null;
            try
            {
                conn = Connexion.GetConnection();
                foreach (var modele in modeles)
                {
                    var pointage = new Pointages
                    {
                        IdEmploye = modele.IdPointage,
                        DebutDimanche = modele.DebutDimanche,
                        Dimanche = modele.Dimanche,
                        FinSamedi = modele.FinSamedi,
                        Lundi = modele.Lundi,
                        Mardi = modele.Mardi,
                        Mercredi = modele.Mercredi,
                        Jeudi = modele.Jeudi,
                        Vendredi = modele.Vendredi,
                        Samedi = modele.Samedi,
                        Total = modele.Total
                    };

                    if (string.IsNullOrEmpty(modele.IdEmploye))
                        _dbo.Save(pointage, conn);
                    else
                    {
                        pointage.Id = modele.Id;
                        _dbo.Update(pointage, conn);
                    }
                }
            }
            finally
            {
                Connexion.Close(null, conn);
            }
        }
        
        
        public List<Pointage> DetailesPointage(ViewPointages pointages)
        {
            var listes = new List<Pointage>();
            var i = 0;
            foreach (var name in Semaine.Names)
            {
                var pointage = new Pointage
                {
                    NomEmploye = pointages.Nom,
                    Jour = pointages.DebutDimanche.AddDays(i),
                    IdEmploye = pointages.IdPointage
                };
                var durre = (string) pointages[name];
                pointage.SetDuree(durre ?? "");
                
                listes.Add(pointage);
                i++;
            }

            var total = new Pointage {NomEmploye = pointages.Nom, IdEmploye = pointages.IdPointage};
            
            total.SetDuree(pointages.Total ?? "");
            listes.Add(total);
            
            return listes;
        }

        public BaseModel FindById(BaseModel baseModel, string id)
        {
            return _dbo.FindById(baseModel, id);
        }

        public void Update(BaseModel baseModel)
        {
            _dbo.Update(baseModel);
        }

        public List<BaseModel> Find(BaseModel baseModel)
        {
            return _dbo.Find(baseModel);
        }

        public double TauxHoraire(Salaire salaire)
        {
            return salaire.Valeur / salaire.TotalHeur;
        }

        public double TauxHeureSup(long heureSup, SqlConnection conn)
        {
            var modele = _dbo.Find(new HeureSup(), conn, $"Min <= {heureSup} and {heureSup} < Max");
            if (modele == null || modele.Count == 0) return 1;
            
            var taux = (double) modele[0]["Pourcentage"];
            return taux / 100d + 1;
        }

        public long HeureSup(Pointage pointage, Salaire salaire)
        {
            if (pointage.Heure == -1) 
                pointage.Heure = salaire.HeureParSemaine();
            
            return  pointage.Heure == -1 ? 0 : pointage.Heure - salaire.HeureParSemaine();
        }

        public Pointage SalaireHebdomadaire(string idEmploye, DateTime date, SqlConnection conn)
        {
            var employe = (Employe) _dbo.FindById(new Employe(), idEmploye);
            
            if(employe == null) 
                throw new Exception("L'employe est introuvable.");

            var listPointage = _dbo.Find(new ViewPointages(), conn,
                $"idEmploye='{idEmploye}' and 0 <= DATEDIFF(DAY, DebutDimanche, '{date:u}') and 0 <= DATEDIFF(DAY, '{date:u}', FinSamedi)");
            
            
            if (listPointage.Count == 0) 
                return new Pointage{NomEmploye = employe.Nom, IdEmploye = employe.Id};

            var salaires = _dbo.Find(new Salaire(), conn, $"idEmploye='{idEmploye}' order by valeur desc");
            var pointages = (ViewPointages) listPointage[0];
            var pointage = TotalPointage(pointages);

            if (salaires.Count == 0) return pointage;
            var salaire = (Salaire) salaires[0];
            
            var tauxH = TauxHoraire(salaire);
            var heureSup = HeureSup(pointage, salaire);
            var tauxSup = TauxHeureSup(heureSup, conn);
            
            var heure = heureSup < 0 ? salaire.HeureParSemaine() + heureSup : salaire.HeureParSemaine();
            heureSup = heureSup < 0 ? 0 : heureSup;

            var majorationNuit = employe.Type == EmployeType.NON_SECURITE ? pointage.Nuit * Config.TauxN : 0;
            var majorationFerier = pointage.Ferier * Config.TauxF;
            var majorationSpe = pointage.Speciale * Config.TauxS;
            
            var majorationSup = heureSup * tauxSup;

            pointage.Salaire = tauxH * (heure + majorationSup + majorationNuit + majorationFerier + majorationSpe);
            pointage.HeureTotal = salaire.HeureParSemaine();
            
            return pointage;
        }

        public Pointage TotalPointage(ViewPointages pointages)
        {
            var total = new Pointage {NomEmploye = pointages.Nom, IdEmploye = pointages.IdPointage};
            total.SetDuree(pointages.Total ?? "");
            return total;
        }

        public List<Pointage> GetEtatPointage(DateTime date)
        {
            SqlConnection conn = null;
            try
            {
                conn = Connexion.GetConnection();
                var pointages = new List<Pointage>();
                var employes = _dbo.Find(new Employe(), conn);
                foreach (var employe in employes)
                {
                    pointages.Add(SalaireHebdomadaire(employe.Id, date, conn));
                }
                var total = new Pointage
                {
                    NomEmploye = "Total",
                    Salaire = pointages.Sum(item => item.Salaire),
                    Ferier = pointages.Sum(item => item.Ferier),
                    Heure = pointages.Sum(item => item.Heure),
                    Nuit = pointages.Sum(item => item.Nuit),
                    Speciale = pointages.Sum(item => item.Speciale),
                    HeureTotal = pointages.Sum(item => item.HeureTotal)
                };

                pointages.Add(total);
                return pointages;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                Connexion.Close(null, conn);
            }
        }

        public void ImportPointages(string path)
        {
            FileStream stream = null;
            SqlConnection conn = null;
            try
            {
                conn = Connexion.GetConnection();
                stream = new FileStream(path, FileMode.Open, FileAccess.Read);

                var modeles = new List<BaseModel>();
                
                using (var reader = new StreamReader(stream, Encoding.Default))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        var i = 0;
                        var lines = line.Split(';');
                        var modele = new Pointages();
                        var properties = modele.Properties();

                        foreach (var property in properties)
                        {
                            modele[property.Name] = lines[i];
                            i++;
                        }
                        modeles.Add(modele);
                        Console.WriteLine(line);
                    }
                }
                _dbo.SaveList(modeles, conn);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                stream?.Close();
                conn?.Close();
            }
        }
        
        private void GenerateFile()
        {
            var i = 0;
            var stream = new FileStream(@"D:\Projet\M1\ProgTa\Pointage\Pointages.csv", FileMode.Open, FileAccess.ReadWrite);
            var date = DateTime.Now;
            using (var writer = new StreamWriter(stream, Encoding.UTF8))
            {
                while (i < 25000)
                {
                    var semaine = new Semaine(date);
                    writer.WriteLine($"EMPL1;{semaine.Dimanche:dd/MM/yyyy};{semaine.Samedi:dd/MM/yyyy};;8;8;8;8;8;;-1");
                    i++;
                    date = date.AddDays(8);
                }
                i = 0;
                date = DateTime.Now;
                while (i < 25000)
                {
                    var semaine = new Semaine(date);
                    writer.WriteLine($"EMPL2;{semaine.Dimanche:dd/MM/yyyy};{semaine.Samedi:dd/MM/yyyy};;8;8;8;8;8;;-1");
                    i++;
                    date = date.AddDays(8);
                }
                i = 0;
                date = DateTime.Now;
                while (i < 25000)
                {
                    var semaine = new Semaine(date);
                    writer.WriteLine($"EMPL3;{semaine.Dimanche:dd/MM/yyyy};{semaine.Samedi:dd/MM/yyyy};;8;8;8;8;8;;-1");
                    i++;
                    date = date.AddDays(8);
                }
                i = 0;
                date = DateTime.Now;
                while (i < 25000)
                {
                    var semaine = new Semaine(date);
                    writer.WriteLine($"EMPL4;{semaine.Dimanche:dd/MM/yyyy};{semaine.Samedi:dd/MM/yyyy};;8;8;8;8;8;;-1");
                    i++;
                    date = date.AddDays(8);
                }
            }
            stream?.Close();
        }
    }
}    