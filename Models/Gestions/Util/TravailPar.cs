namespace Admins.Models.Gestions.Util
{
    public static class TravailPar
    {
        public static readonly long HEURE = 0;
        public static readonly long JOUR = 1;
        public static readonly long SEMAINE = 2;
        public static readonly long MOIS = 3;
        public static readonly long ANNEE = 4;
    }
}