namespace Admins.Models.Gestions.Util
{
    public static class EmployeType
    {
        public static readonly long NON_SECURITE = 1;
        public static readonly long SECURITE = 2;
    }
}