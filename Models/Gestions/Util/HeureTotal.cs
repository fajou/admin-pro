namespace Admins.Models.Gestions.Util
{
    public static class HeureTotal
    {
        public static readonly long PAR_JOUR = 8;
        public static readonly long PAR_SEMAINE = 40;
        public static readonly long PAR_MOIS = 160;
        public static readonly long PAR_ANNEE = 1920;
    }
}