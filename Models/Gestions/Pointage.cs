using System;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;

namespace Admins.Models.Gestions
{
    public class Pointage : BaseModel
    {
        private DateTime jour;
        private string nomEmploye;
        private string idEmploye;
        private string durree;
        private long heureTotal;
        private long heure;
        private long nuit;
        private long ferier;
        private long speciale;
        private double salaire;

        [Hide]
        public DateTime Jour
        {
            get { return jour; }
            set { jour = value; }
        }
        
        [Champ(Label = "Employe")]
        public string NomEmploye
        {
            get
            {
                return nomEmploye;
            }
            set
            {
                nomEmploye = value;
            }
        }

        [Hide]
        public string IdEmploye
        {
            get { return idEmploye; }
            set { idEmploye = value; }
        }

        [Champ(Label = "Heure/Semaine")]
        public long HeureTotal
        {
            get { return heureTotal; }
            set { heureTotal = value; }
        }

        public string Durree
        {
            get { return durree; }
            set { durree = value; }
        }

        public long Heure
        {
            get { return heure; }
            set { heure = value; }
        }

        public long Nuit
        {
            get { return nuit; }
            set { nuit = value; }
        }

        public long Ferier
        {
            get { return ferier; }
            set { ferier = value; }
        }
        
        public long Speciale
        {
            get { return speciale; }
            set { speciale = value; }
        }

        public double Salaire
        {
            get { return salaire; }
            set { salaire = value; }
        }

        public void SetDuree(string durres)
        {
            durres = durres ?? "";
            var heures = durres.Trim().Split('/');
               
            foreach (var h in heures)
            {
                var temp = h.Trim();                    
                if(temp.Contains("H") || durres.Equals("-1"))
                    Heure = long.Parse(temp.Replace("H", ""));
                
                if (durres.Equals("-1"))
                {
                    Durree = "Complet"; 
                    return;
                }

                if (temp.Contains("N"))
                    Nuit = long.Parse(temp.Replace("N", ""));

                else if (temp.Contains("F"))
                    Ferier = long.Parse(temp.Replace("F", ""));

                else if (temp.Contains("S"))
                    Speciale = long.Parse(temp.Replace("S", ""));
            }
            Durree = durres;
        }
    }
}