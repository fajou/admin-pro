using System.Collections.Generic;
using Admins.Models.Gestions.ViewData;

namespace Admins.Models.Gestions.ViewModel
{
    public class PointagesViewModel
    {
        public List<ViewPointages> LPointages { get; set; }
    }
}