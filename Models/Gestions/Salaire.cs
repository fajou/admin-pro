using System;
using Admins.Models.Gestions.Util;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;
using Admins.Traitements.Generic.Util;

namespace Admins.Models.Gestions
{
    [ListEditable]
    [ListDeletable]
    [Lists("Liste salaires")]
    [Forms("Saisie Salaire")]
    public class Salaire : BaseModel
    {
        private double _valeur;
        private long _totalHeur;

        [Champ("Nom", "Employé", typeof(Employe), Choix = true)]
        public string IdEmploye { get; set; }
        

        [Champ("Par", "Travail Par", typeof(TravailType), SelectDB = true, Class = Use.ClFormSelect2)]
        public long TravailPar { get; set; } = Util.TravailPar.MOIS;
        
        [Champ(Type = "Numerc", Label = "Heures")]
        public long TotalHeur
        {
            get
            {
                return _totalHeur;
            }
            set
            {
                if(value <= 0)
                    throw new Exception("Total en Heure negatif ou zero invalide");
                
                if (IsPar(Util.TravailPar.MOIS) && HeureTotal.PAR_MOIS < value)
                    throw new Exception($"Le Total en Heure par Mois est invalide. Max :{HeureTotal.PAR_MOIS}");
                
                if (IsPar(Util.TravailPar.JOUR)  && HeureTotal.PAR_JOUR < value)
                    throw new Exception($"Le Total en Heure par Jour est invalide. Max :{HeureTotal.PAR_JOUR}");
                
                if (IsPar(Util.TravailPar.ANNEE)  && HeureTotal.PAR_SEMAINE < value)
                    throw new Exception($"Le Total en Heure par Semaine est invalide. Max :{HeureTotal.PAR_SEMAINE}");
                
                if (IsPar(Util.TravailPar.ANNEE)  && HeureTotal.PAR_ANNEE < value)
                    throw new Exception($"Le Total en Heure par Année est invalide. Max :{HeureTotal.PAR_ANNEE}");
                
                _totalHeur = value;
            }
        }
        
        [Champ(Type = "Numerc", Label = "Salaire")]
        public double Valeur
        {
            get
            {
                return _valeur;
            }
            set
            {
                if(value <= 0)
                    throw new Exception("La valeur du montant entré est invalide.");
                _valeur = value;
            }
        }

        public long HeureParSemaine()
        {
            if (IsPar(Util.TravailPar.MOIS))
                return TotalHeur / 4;
            if (IsPar(Util.TravailPar.JOUR))
                return TotalHeur * 5;
            if (IsPar(Util.TravailPar.ANNEE))
                return TotalHeur / 48;
            return TotalHeur + 34;
        }
        
        public bool IsPar(long par)
        {
            return TravailPar == par;
        }
    }
}