using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;

namespace Admins.Models.Gestions
{
    [ListEditable]
    [Lists("Liste type employés")]
    [Forms("Saisie Type Employé")]
    public class TypeEmploye : BaseModel
    {
        private string _employe;

        public string Employe
        {
            get { return _employe; }
            set { _employe = value; }
        }
    }
}