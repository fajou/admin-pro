using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Admins.Models.Gestions.ViewData;
using Admins.Traitements;
using Admins.Traitements.Generic.Base;

namespace Admins.Models.Gestions
{
    public interface IGestionService : IService
    {
        void Save(BaseModel bm);
        
        List<BaseModel> Employes();

        List<ViewPointages> LViewPointages(DateTime date);

        void SaveListe(List<ViewPointages> modeles);

        BaseModel FindById(BaseModel baseModel, string id);

        List<Pointage> DetailesPointage(ViewPointages pointages);

        void Update(BaseModel baseModel);

        List<BaseModel> Find(BaseModel baseModel);

        double TauxHoraire(Salaire salaire);

        double TauxHeureSup(long heureSup, SqlConnection conn);

        long HeureSup(Pointage pointage, Salaire salaire);

        Pointage SalaireHebdomadaire(string idEmploye, DateTime date, SqlConnection conn);

        Pointage TotalPointage(ViewPointages pointages);

        List<Pointage> GetEtatPointage(DateTime date);

        void ImportPointages(string path);
    }
}