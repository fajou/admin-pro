using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;
using Admins.Traitements.Generic.Util;

namespace Admins.Models.Gestions
{
    [ListEditable]
    [Lists("Liste des employé")]
    [Forms("Saisie Employé")]
    public class Employe : BaseModel
    {
        [Champ(Id = "nomId", Name = "nom", Label = "Nom")]
        public string Nom { get; set; }

        [Champ(Id = "mailId", Label = "Mail")]
        public string Mail { get; set; }

        [Champ(Id = "metierId", Label = "Metier")]
        public string Metier { get; set; }

        [Champ("Employe", "Type Employé", typeof(TypeEmploye), SelectDB = true, Class = Use.ClFormSelect2)]
        public long Type { get; set; }
    }
}