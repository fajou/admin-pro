using Admins.Traitements.Generic.Base;

namespace Admins.Models.Gestions
{
    public class TravailType : BaseModel
    {
        private string _par;

        public string Par
        {
            get { return _par;}
            set { _par = value; }
        }
    }
}