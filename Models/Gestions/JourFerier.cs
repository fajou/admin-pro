using System;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;

namespace Admins.Models.Gestions
{
    [ListDeletable]
    [ListEditable]
    [Forms("Nouveau Jour Ferié", "Edition Jour Fierié")]
    public class JourFerier : BaseModel
    {
        [Champ(Type = "date", Label = "Jour Fierié")]
        public DateTime Jour { get; set; }

        [Champ(Type = "checkbox", Class = "minimal", Label = "Demi-Journée ")]
        public bool Demi { get; set; }
    }
}    