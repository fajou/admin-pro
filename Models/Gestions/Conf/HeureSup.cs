using System;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;

namespace Admins.Models.Gestions.Conf
{
    [Forms("Configuration Heure Suplementaire")]
    public class HeureSup : BaseModel
    {
        private long min;
        private long max;
        private double pourcentage;

        [Champ(Type = "number", Label = "Heure Min")]
        public long Min
        {
            get { return min; }
            set
            {
                if(value <= 0 && 20 <= value) 
                    throw new Exception("L'heure suplementaire minimum n'est pas valide");
                min = value;
            }
        }

        [Champ(Type = "number", Label = "Heure Max")]
        public long Max
        {
            get { return max; }
            set
            {
                if(value <= 0 && 20 <= value) 
                    throw new Exception("L'heure suplementaire maximum n'est pas valide");
                if(value < Min)
                    throw new Exception("L'heure suplementaire maximum doit superieur à la minimum");

                max = value;
            }
        }

        [Champ(Type = "number", Label = "Pourcentage Sup")]
        public double Pourcentage
        {
            get { return pourcentage; }
            set
            {
                if(value <= 0 && 200 <= value) 
                    throw new Exception("Le pourcentage suplementaire n'est pas valide");
                pourcentage = value;
            }
        }
    }
}