namespace Admins.Models.Gestions.Conf
{
    public static class Config
    {
        public static readonly string[] TabPointage = {
            "IdEmploye", "DebutDimanche", "FinSamedi", "Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi",
            "Samedi", "Total"
        };
        

        public static double TauxN = 0.3;
        public static double TauxF = 0.5;
        public static double TauxS = 0.7;
    }
}