namespace Admins.Models.Budget
{
    public class PorteFeuilleBudget
    {
        private decimal _hebdomadaire;
        private decimal _mensuel;
        private decimal _porteFeuille;

        public decimal Hebdomadaire
        {
            get { return _hebdomadaire; }
            set { _hebdomadaire = value; }
        }

        public decimal Mensuel
        {
            get { return _mensuel; }
            set { _mensuel = value; }
        }

        public decimal PorteFeuille
        {
            get { return _porteFeuille; }
            set { _porteFeuille = value; }
        }
    }
}