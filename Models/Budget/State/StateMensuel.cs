using System;
using Admins.Traitements.Generic.Base;

namespace Admins.Models.Budget.State
{
    public class StateMensuel : BaseChart
    {
        public decimal Semaine1 { get; set; }

        public decimal Semaine2 { get; set; }

        public decimal Semaine3 { get; set; }

        public decimal Semaine4 { get; set; }

        public override decimal TotalDefault()
        {
            throw new NotImplementedException();
        }
    }
}