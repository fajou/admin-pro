using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Attributes.Chart;
using Admins.Traitements.Generic.Base;

namespace Admins.Models.Budget.State
{
    [ChartType(Title = "Depense - Ariary")]
    [Lists("Depense mensuel")]
    public class StateAnnuel : BaseChart
    {
        [Champ]
        public string Designation { get; set; }

        [ChartInfo]
        [NumberFormat]
        public decimal Janvier { get; set; }

        [ChartInfo]
        [NumberFormat]
        public decimal Fevrier { get; set; }

        [ChartInfo]
        [NumberFormat]
        public decimal Mars { get; set; }

        [ChartInfo]
        [NumberFormat]
        public decimal Avril { get; set; }

        [ChartInfo]
        [NumberFormat]
        public decimal Mai { get; set; }

        [ChartInfo]
        [NumberFormat]
        public decimal Juin { get; set; }

        [ChartInfo]
        [NumberFormat]
        public decimal Juillet { get; set; }

        [ChartInfo]
        [NumberFormat]
        public decimal Aout { get; set; }

        [ChartInfo]
        [NumberFormat]
        public decimal Septembre { get; set; }

        [ChartInfo]
        [NumberFormat]
        public decimal Octobre { get; set; }

        [ChartInfo]
        [NumberFormat]
        public decimal Novembre { get; set; }

        [ChartInfo]
        [NumberFormat]
        public decimal Decembre { get; set; }

        [NotColumn]
        [NumberFormat]
        [Champ(Label = "Total")]
        public decimal Total { get; set; }

        public override decimal TotalDefault()
        {
            return Janvier + Fevrier + Mars + Avril + Mai + Juin + Juillet + Aout + Septembre + Octobre + Novembre + Decembre;
        }
    }
}