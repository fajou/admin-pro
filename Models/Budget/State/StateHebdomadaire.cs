using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Attributes.Chart;
using Admins.Traitements.Generic.Base;

namespace Admins.Models.Budget.State
{
    [ChartType(Title = "Depense - Ariary")]
    [Lists("Depense hebdomadaire")]
    public class StateHebdomadaire : BaseChart
    {
        [Champ]
        public string Designation { get; set; }

        [ChartInfo]
        [NumberFormat]
        public decimal Dimanche { get; set; }

        [ChartInfo]
        [NumberFormat]
        public decimal Lundi { get; set; }

        [ChartInfo]
        [NumberFormat]
        public decimal Mardi { get; set; }

        [ChartInfo]
        [NumberFormat]
        public decimal Mercredi { get; set; }

        [ChartInfo]
        [NumberFormat]
        public decimal Jeudi { get; set; }

        [ChartInfo]
        [NumberFormat]
        public decimal Vendredi { get; set; }

        [ChartInfo]
        [NumberFormat]
        public decimal Samedi { get; set; }

        [NotColumn]
        [NumberFormat]
        [Champ(Label = "Total/Semaine")]
        public decimal Total { get; set; }

        [NotColumn]
        [NumberFormat]
        [Champ(Label = "Max/Semaine")]
        public decimal Maximum { get; set; }


        public override decimal TotalDefault()
        {
            return Dimanche + Lundi + Mardi + Mercredi + Jeudi + Vendredi + Samedi;
        }
    }
}