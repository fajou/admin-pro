using System;

namespace Admins.Models.Budget.Model
{
    public class StateChartViewModel
    {
        public DateTime Date { get; set; }

        public TypeDepense Global { get; set; } = new TypeDepense
        {
            Id = "_All",
            Designation = "Global"
        };

        public TypeDepense[] TypeDepenses { get; set; }
    }
}