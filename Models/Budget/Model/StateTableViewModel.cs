using System;
using Admins.Traitements.Models;

namespace Admins.Models.Budget.Model
{
    public class StateTableViewModel : ListViewModel
    {
        public DateTime Date { get; set; }
    }
}