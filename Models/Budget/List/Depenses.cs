using System;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;

namespace Admins.Models.Budget.List
{
    [PreviewDetails(TypeBaseModel = typeof(Depenses))]
    [ListDeletable(TypeBaseModel = typeof(Depense))]
    [ListEditable(TypeBaseModel = typeof(Depense))]
    [Lists("Liste des Depenses")]
    [Forms(typeof(Depense))]
    public class Depenses : BaseModel
    {
        private string _idTypeDepense;
        private string _designation;
        private string _description;
        private decimal _valeurMax;
        private decimal _valeurMin;
        private decimal _valeur;
        private DateTime _dateAjout;
        private decimal _parJour;

        [Hide]
        public string IdTypeDepense
        {
            get { return _idTypeDepense; }
            set { _idTypeDepense = value; }
        }

        [Champ]
        public string Designation
        {
            get { return _designation; }
            set { _designation = value; }
        }

        [Champ]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [Champ(Label = "Valeurs", Type = "numeric")]
        [NumberFormat]
        public decimal Valeur
        {
            get { return _valeur; }
            set { _valeur = value; }
        }

        [Champ(Label = "Date", Type = "date")]
        [DateTimeFormat("D")]
        public DateTime DateAjout
        {
            get { return _dateAjout; }
            set { _dateAjout = value; }
        }

        [NumberFormat]
        [Champ(Label = "Par jour", Type = "numeric")]
        public decimal ParJour
        {
            get { return _parJour; }
            set { _parJour = value; }
        }

        [NumberFormat]
        [Champ(Label = "Min", Type = "numeric")]
        public decimal ValeurMin
        {
            get { return _valeurMin; }
            set { _valeurMin = value; }
        }
        
        [NumberFormat]
        [Champ(Label = "Max", Type = "numeric")]
        public decimal ValeurMax
        {
            get { return _valeurMax; }
            set { _valeurMax = value; }
        }
    }
}