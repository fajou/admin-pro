using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;

namespace Admins.Models.Budget
{
    [Forms("Ajouter au porte feuille")]
    [TableInfo("PORTE_FEUILLE", "PORTE_FEUILLE_ID","")]
    public class PorteFeuille : BaseModel
    {
        public decimal Valeur { get; set; }
    }
}