using System;
using Admins.Models.Budget.List;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;
using Admins.Traitements.Generic.Util;

namespace Admins.Models.Budget
{
    [ListEditable]
    [ListDeletable(TypeBaseModel = typeof(Depenses))]
    [Forms("Nouveau dépense", "Modification d'une depense", SaveAndCreateAnOther = true)]
    [Lists("Liste des dépense", typeof(Depenses))]
    public class Depense : BaseModel
    {
        private string _idTypeDepense;
        private string _description;
        private decimal _valeur;
        private DateTime _dateAjout = DateTime.Now;
        
        [Champ(nameof(TypeDepense.Designation), "Depense", typeof(TypeDepense), SelectDB = true, Class = Use.ClFormSelect2)]
        public string IdTypeDepense
        {
            get { return _idTypeDepense; }
            set
            {
                if(string.IsNullOrEmpty(value))
                    throw new Exception("Mentionné le type de votre depense");
                _idTypeDepense = value;
            }
        }

        [Champ(Label = nameof(Description))]
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        [Champ(Label = nameof(Valeur), Type = Champ.Numeric)]
        [NumberFormat]
        public decimal Valeur
        {
            get { return _valeur; }
            set
            {
                if(value <= 0)
                    throw new Exception("Valeur du dépense negatif ou zéro invalide");
                _valeur = value;
            }
        }

        [Champ(Label = "Date", Type = Champ.Datetime)]
        [DateTimeFormat("d")]
        public DateTime DateAjout
        {
            get { return _dateAjout; }
            set { _dateAjout = value; }
        }
    }
}