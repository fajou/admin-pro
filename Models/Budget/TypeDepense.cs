using System;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;

namespace Admins.Models.Budget
{
    [ListEditable]
    [Lists("Differente type de dépense")]
    [Forms("Nouveau Type de dépense", "Modification d'un Type de dépense")]
    public class TypeDepense : BaseModel
    {
        private string _designation;
        private decimal _valeurMin;
        private decimal _valeurMax;
        private decimal _parJour;
        private decimal _parSemaine;
        private decimal _parMois;

        [Champ(Label = "Désignation")]
        public string Designation
        {
            get { return _designation; }
            set { _designation = value; }
        }

        [Champ(Label = "Par jour", Type = Champ.Numeric)]
        [NumberFormat]
        public decimal ParJour
        {
            get { return _parJour; }
            set
            {
                if(value < 0)
                    throw new Exception("Valeur `Par jour` negaitve à zero invalide.");
                _parJour = value;
            }
        }

        [Champ(Label = "Par semaine", Type = Champ.Numeric)]
        [NumberFormat]
        public decimal ParSemaine
        {
            get { return _parSemaine; }
            set
            {
                if(value < 0)
                    throw new Exception("Valeur `Par semaine negaitve à zero invalide.");
                _parSemaine = value;
            }
        }

        [Champ(Label = "Par mois", Type = Champ.Numeric)]
        [NumberFormat]
        public decimal ParMois
        {
            get { return _parMois; }
            set
            {
                if(value < 0)
                    throw new Exception("Valeur `Par mois` negaitve à zero invalide.");
                _parMois = value;
            }
        }

        [Champ(Label = "Valeur min.", Type = Champ.Numeric)]
        [NumberFormat]
        public decimal ValeurMin
        {
            get
            {
                return _valeurMin;
            }
            set
            {
                if(value < 0)
                    throw new Exception("Valeur min negaitve à zero invalide.");
                _valeurMin = value;
            }
        }

        [Champ(Label = "Valeur max.", Type = Champ.Numeric)]
        [NumberFormat]
        public decimal ValeurMax
        {
            get { return _valeurMax; }
            set
            {
                if(value < 0)
                    throw new Exception("Valeur max negaitve à zero invalide.");
                if(value <= _valeurMin)
                    throw new Exception("Valeur max doit superieur à la valeur min.");
                _valeurMax = value;
            }
        }
    }
}