using System;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;

namespace Admins.Models.Taches
{
    [ListEditable]
    [ListDeletable]
    [Forms("Nouveau tâche", "Modifier cette tâche")]
    [Lists("Liste des tâches")]
    public class Sommet : BaseModel, IComparable
    {
        private string _nom = "--";
        private string _contraintes = "";
        private double _durree;

        [Champ(Label = "Nom Tâche")]
        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }

        [Champ(Label = "Tâches Sous Contraintes")]
        public string Contraintes
        {
            get { return _contraintes; }
            set
            {
                if(!string.IsNullOrEmpty(value))
                    _contraintes += "," + value;
            }
        }

        [Champ(Label = "Durrée Tâche")]
        public double Durree
        {
            get { return _durree; }
            set { _durree = value; }
        }

        public int CompareTo(object obj)
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return Nom;
        }
    }
}