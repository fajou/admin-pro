using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Base;

namespace Admins.Models.Taches
{
    [ListEditable]
    [Forms("Nouveau graph", "Modification d'un graph")]
    public class PathGraph : BaseModel
    {
        private string _fileName;
        private string _graph;

        [Champ(Label = "Nom graph")]
        public string Graph
        {
            get { return _graph; }
            set { _graph = value; }
        }

        [Champ(Label = "Chemin graph", Type = Champ.FileIdentifier)]
        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }
    }
}