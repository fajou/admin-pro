create function TotalDepense(@Id varchar(10), @aDate date) returns DECIMAL(12, 2)
BEGIN
    return (select Total from TotalParJour where DATEDIFF(DAY, @aDate, DATEAJOUT) = 0 and IdType = @Id);
end
go

