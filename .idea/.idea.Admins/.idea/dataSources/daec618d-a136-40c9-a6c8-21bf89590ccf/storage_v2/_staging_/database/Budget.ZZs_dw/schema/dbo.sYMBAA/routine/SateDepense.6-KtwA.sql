create function SateDepense(@Id varchar(10), @dim date, @lun date, @mar date, @mer date, @jeu date, @ven date,
                            @sam date)
  returns @StateDepense TABLE
                        (
                          Id       varchar(10),
                          Dimanche decimal(12, 2),
                          Lundi    decimal(12, 2),
                          Mardi    decimal(12, 2),
                          Mercredi decimal(12, 2),
                          Jeudi    decimal(12, 2),
                          Vendredi decimal(12, 2),
                          Samedi   decimal(12, 2)
                        ) as
BEGIN
  insert into @StateDepense values (@Id,
                                    [dbo].TotalDepense(@Id, @dim),
                                    [dbo].TotalDepense(@Id, @lun),
                                    [dbo].TotalDepense(@Id, @mar),
                                    [dbo].TotalDepense(@Id, @mer),
                                    [dbo].TotalDepense(@Id, @jeu),
                                    [dbo].TotalDepense(@Id, @ven),
                                    [dbo].TotalDepense(@Id, @sam)
    );
  return;
end
go


