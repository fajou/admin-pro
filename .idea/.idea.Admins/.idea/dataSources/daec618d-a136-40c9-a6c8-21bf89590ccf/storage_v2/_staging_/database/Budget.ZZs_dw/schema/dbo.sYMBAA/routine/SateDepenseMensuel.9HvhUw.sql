create function SateDepenseMensuel(@Id varchar(10), @Date date)
    returns @SateDepenseMensuel TABLE
                          (
                              Id          varchar(10),
                              Designation varchar(100),
                              Janvier varchar(200),
                              Fevrier    decimal(12, 2),
                              Mars       decimal(12, 2),
                              Avril       decimal(12, 2),
                              Mai    decimal(12, 2),
                              Juin       decimal(12, 2),
                              Juillet    decimal(12, 2),
                              Aout      decimal(12, 2),
                              Septembre      decimal(12, 2),
                              Octobre      decimal(12, 2),
                              Novembre      decimal(12, 2),
                              Decembre      decimal(12, 2)
                          ) as
BEGIN
    declare
        @Designation varchar(200);
    set @Designation = (select Designation from TypeDepense where Id = @Id)
    insert into @SateDepenseMensuel
    values (@Id,
            @Designation,
            [dbo].TotalMensuel(@Id, 1, @Date),
            [dbo].TotalMensuel(@Id, 2, @Date),
            [dbo].TotalMensuel(@Id, 3, @Date),
            [dbo].TotalMensuel(@Id, 4, @Date),
            [dbo].TotalMensuel(@Id, 5, @Date),
            [dbo].TotalMensuel(@Id, 6, @Date),
            [dbo].TotalMensuel(@Id, 7, @Date),
            [dbo].TotalMensuel(@Id, 8, @Date),
            [dbo].TotalMensuel(@Id, 9, @Date),
            [dbo].TotalMensuel(@Id, 10, @Date),
            [dbo].TotalMensuel(@Id, 11, @Date),
            [dbo].TotalMensuel(@Id, 12, @Date));
    return;
end
go

