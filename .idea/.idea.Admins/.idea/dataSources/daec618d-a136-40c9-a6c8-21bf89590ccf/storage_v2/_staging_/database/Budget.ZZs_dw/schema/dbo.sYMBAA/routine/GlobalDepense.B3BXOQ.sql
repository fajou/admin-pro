create function GlobalDepense(@aDate date) returns DECIMAL(12, 2)
BEGIN
  declare @total decimal(12,2);
  set @total = (select SUM(Total) as Total from TotalParJour t where DATEAJOUT = @aDate);
  return @total;
end
go

