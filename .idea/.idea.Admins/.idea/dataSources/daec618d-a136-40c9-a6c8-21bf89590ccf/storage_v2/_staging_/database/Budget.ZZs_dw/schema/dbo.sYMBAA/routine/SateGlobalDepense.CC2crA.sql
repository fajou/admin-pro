create function SateGlobalDepense(@dim date, @lun date, @mar date, @mer date, @jeu date, @ven date, @sam date)
  returns @SateGlobalDepense TABLE
                        (
                          Id varchar(10),
                          Dimanche decimal(12, 2),
                          Lundi    decimal(12, 2),
                          Mardi    decimal(12, 2),
                          Mercredi decimal(12, 2),
                          Jeudi    decimal(12, 2),
                          Vendredi decimal(12, 2),
                          Samedi   decimal(12, 2)
                        ) as BEGIN
  insert into @SateGlobalDepense values ('_All',
                                    [dbo].GlobalDepense(@dim),
                                    [dbo].GlobalDepense(@lun),
                                    [dbo].GlobalDepense(@mar),
                                    [dbo].GlobalDepense(@mer),
                                    [dbo].GlobalDepense(@jeu),
                                    [dbo].GlobalDepense(@ven),
                                    [dbo].GlobalDepense(@sam)
    );
  return;
end
go

create function TotalDepense(@Id varchar(10), @aDate date) returns DECIMAL(12, 2)
BEGIN
  return (select Total from TotalParJour where DATEDIFF(DAY, @aDate, DATEAJOUT) = 0 and IdType = @Id);
end
go
