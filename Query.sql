WITH TempEmp (Name, duplicateRecCount)
         AS
         (
             SELECT Id,
                    ROW_NUMBER() OVER (PARTITION by Id, Valeur ORDER BY Id)
                        AS duplicateRecCount
             FROM Depense
         )

DELETE FROM TempEmp
WHERE duplicateRecCount > 1; 