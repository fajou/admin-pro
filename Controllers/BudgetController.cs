using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Admins.Models.Budget;
using Admins.Models.Budget.Model;
using Admins.Models.Budget.State;
using Admins.Services.Budget;
using Admins.Traitements.Action;
using Admins.Traitements.Services.Repository;

namespace Admins.Controllers
{
    public class BudgetController : BaseController
    {
        private readonly IStatistiqueBudgetService _service;
        private readonly IDataBaseModel _model;

        public BudgetController(IStatistiqueBudgetService service, IDataBaseModel model)
        {
            _service = service;
            _model = model;
        }

        public ActionResult StateChart(DateTime? date)
        {
            return ViewProtected(View(new StateChartViewModel
            {
                Date = date ?? DateTime.Now,
                TypeDepenses = _model.Find(new TypeDepense()).Cast<TypeDepense>().ToArray()
            }));
        }

        public ActionResult StateTable(DateTime? date)
        {
            try
            {
                var state = new StateHebdomadaire();
                return ViewProtected(View(new StateTableViewModel
                {
                    BoxTitle = new HtmlString(state.ListBoxTitle()),
                    Modeles = _service.Hebdomadaire(date ?? DateTime.Now),
                    Date = date ?? DateTime.Now
                }));
            }
            catch (Exception e)
            {
                return ViewError(e.GetBaseException().Message);
            }
        }

        public ActionResult StateTableAnnuel(DateTime? date)
        {
            try
            {
                var state = new StateAnnuel();
                return ViewProtected(View(new StateTableViewModel
                {
                    BoxTitle = new HtmlString(state.ListBoxTitle()),
                    Modeles = _service.Annuel(date ?? DateTime.Now),
                    Date = date ?? DateTime.Now
                }));
            }
            catch (Exception e)
            {
                return ViewError(e.GetBaseException().Message);
            }
        }

        public JsonResult JsonState(string typeId, DateTime? date)
        {
            var state = _service.Get(typeId, date ?? DateTime.Now);
            return Json(state.DataChart("C"), JsonRequestBehavior.AllowGet);
        }

        public JsonResult JsonGlobalState(DateTime? date)
        {
            var state = _service.GetGlobal(date ?? DateTime.Now);
            return Json(state.DataChart("C"), JsonRequestBehavior.AllowGet);
        }
    }
}