﻿using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Admins.Models.Gestions;
using Admins.Models.Gestions.ViewModel;
using Admins.Traitements.Action;
using Admins.Traitements.Generic.Base;
using Admins.Traitements.Generic.Html;
using Admins.Traitements.Generic.Util;
using Admins.Traitements.Models;

namespace Admins.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IGestionService _gestionService;

        public HomeController(IGestionService gestionService)
        {
            _gestionService = gestionService;
        }

        public ActionResult Index()
        {
            var date = Request.QueryString[Input.DateName];
            var listes = _gestionService.LViewPointages(!string.IsNullOrEmpty(date) ? Convert.ToDateTime(date) : DateTime.Now);
            
            return View(new PointagesViewModel{LPointages = listes});
        }

        public void Pointer()
        {
            var daty = DateTime.Now;
            var date = Request.QueryString[Input.DateName];
            var listes = _gestionService.LViewPointages(!string.IsNullOrEmpty(date) ? daty = Convert.ToDateTime(date) : daty);
            var semaine = new Semaine(daty);
            
            foreach (var modele in listes)
            {
                modele.DebutDimanche = semaine.Dimanche;
                modele.FinSamedi = semaine.Samedi;
                
                var infos = modele.Properties();
                foreach (var info in infos)
                {
                    if (BaseModel.Hide(info)) continue;
                    
                    var name = $"{info.Name}{modele["IdPointage"]}";
                    modele[info.Name] = Request.Form[name];
                }
            }
            _gestionService.SaveListe(listes);
            Response.Redirect("/");
        }

        public ActionResult EtatPointage()
        {
            var date = Request.QueryString[Input.DateName];
            var listes = _gestionService.GetEtatPointage(!string.IsNullOrEmpty(date) ? Convert.ToDateTime(date) : DateTime.Now);
            return View(listes.Cast<BaseModel>().ToList());
        }

        public ActionResult Import()
        {
            return View(new FormViewModel
            {
                BoxTitle = "Import en (.CSV)",
                BtnText = "Importer",
                UrlAfter = "/"
            });
        }

        [HttpPost]
        public ActionResult DoImport()
        {
            try
            {
                var file = Request.Files[0];
                
                if (file == null || file.ContentLength <= 0) 
                    return RedirectToAction("Index");
                
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/App_Data/Pointages"), fileName ?? DateTime.Now.ToString("O"));
                file.SaveAs(path);
                
                _gestionService.ImportPointages(path);
                return RedirectToAction("Index");  

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return View("Import", new FormViewModel
                {
                    BoxTitle = "Import en (.CSV)",
                    BtnText = "Importer",
                    UrlAfter = Url.Action("Index", "Home"),
                    ErrorMessage = e.Message
                });
            }
        }
    }
}