using System.Web.Mvc;

namespace Admins.Controllers.Immobilier
{
    public class ImmobilierController : Controller
    {
        // GET
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Achat()
        {
            return View();
        }

        public ActionResult Emploi()
        {
            return View();
        }

        public ActionResult Location()
        {
            return View();
        }

        public ActionResult Maison()
        {
            return View();
        }

        public ActionResult Personne()
        {
            return View();
        }

        public ActionResult Terrain()
        {
            return View();
        }

        public ActionResult Vente()
        {
            return View();
        }
        
    }
}