create sequence IdMenus increment by 1 start with 200;
create sequence IdRoles increment by 1 start with 200;
create sequence IdUtilisateur increment by 1 start with 200;


create table Roles
(
    Id        varchar(10) primary key,
    Levels    int unique,
    RolesName varchar(50)
);

create table Menus
(
  Id varchar(10) primary key,
  IdParent varchar(10),
  Label varchar(100),
  Url varchar(255),
  IconClass varchar(100),
  ClassName varchar(255),
  TypeMenu int,
  Levels int references Roles(Levels)
);

create table Utilisateur
(
  Id varchar(10) primary key,
  Nom varchar(100),
  Email varchar(100),
  Prenom varchar(100),
  DateNaissance DATE,
  Pass varchar(200),
  ImagePath varchar(200),
  Levels int references Roles(Levels)
);

create sequence ID_BASE_PROPERTIES increment by 1 start with 1;
create sequence ID_BASE_CLASSES increment by 1 start with 1;
create sequence ID_BASE_ATTRIBUTES increment by 1 start with 1;
create sequence ID_BASE_TYPES increment by 1 start with 1;

create table BASE_CLASSES
(
    Id varchar(10) primary key,
    Namespace varchar(100),
    ClassName varchar(50),
    TableName varchar(100),
);

create table BASE_PROPERTIES
(
    Id varchar(10) primary key ,
    IdClass varchar(10),
    PropertieName varchar(50),
    PropertieTypeName varchar(50),
    PropertieType varchar(100),
);

create table BASE_ATTRIBUTES
(
    Id                varchar(10) primary key,
    AttributeId       varchar(10),
    AttributeName     varchar(50),
    AttributeArgument nvarchar(max),
    AtritbuteData     nvarchar(max),
    foreign key (AttributeId) references BASE_CLASSES (Id),
    foreign key (AttributeId) references BASE_PROPERTIES(Id)
);

create table BASE_TYPES
(
    Id       varchar(10) primary key,
    TypeName varchar(100),
    TypeBase varchar(100),
    SQLType varchar(100),
);





INSERT INTO Roles (Id, Levels, RolesName)VALUES ('MENU10198', 100, 'Admins');
INSERT INTO Roles (Id, Levels, RolesName)VALUES ('MENU10199', 110, 'Normal');
INSERT INTO Roles (Id, Levels, RolesName)VALUES ('MENU10200', 120, 'Basics');

INSERT INTO dbo.Menus (Id, IdParent, Label, Url, IconClass, ClassName, TypeMenu, Levels) VALUES ('MENU102', '', 'Gestion Menu', '#', 'fa fa-bars', null, 0, 100);
INSERT INTO dbo.Menus (Id, IdParent, Label, Url, IconClass, ClassName, TypeMenu, Levels) VALUES ('MENU103', 'MENU102', 'Nouveau menu', '', 'fa  fa-plus-circle', 'Admins.Traitements.Generic.Admins.Menus', 1, 100);
INSERT INTO dbo.Menus (Id, IdParent, Label, Url, IconClass, ClassName, TypeMenu, Levels) VALUES ('MENU104', 'MENU102', 'Listes menus', '', 'fa fa-list-alt', 'Admins.Traitements.Generic.Admins.MenusList', 2, 100);
INSERT INTO dbo.Menus (Id, IdParent, Label, Url, IconClass, ClassName, TypeMenu, Levels) VALUES ('MENU105', 'MENU102', 'Nouveau rôle', '', 'fa fa-lock', 'Admins.Traitements.Generic.Admins.Dicos.Roles', 1, 100);
INSERT INTO dbo.Menus (Id, IdParent, Label, Url, IconClass, ClassName, TypeMenu, Levels) VALUES ('MENU106', 'MENU102', 'Liste rôles', '', 'fa fa-list-alt', 'Admins.Traitements.Generic.Admins.Dicos.Roles', 2, 100);
INSERT INTO dbo.Menus (Id, IdParent, Label, Url, IconClass, ClassName, TypeMenu, Levels) VALUES ('MENU107', 'MENU102', 'Change DB', '/Admins/ChangeDB', 'fa fa-database', null, 3, 100);

INSERT INTO Utilisateur (Id, Nom, Email, Prenom, DateNaissance, Pass, Levels) VALUES ('UTIL10001', 'Olivier Jean', 'olivier.aps.p2@gmail.com', 'FANOMEZANTSOA Andonianana', '1996-11-25', 'root', null);

create view MenusList as
select menu.Id,
       menu.IdParent,
       menu.Label,
       par.Label as LabelParent,
       menu.Url,
       menu.IconClass,
       menu.ClassName,
       menu.TypeMenu,
       menu.Levels,
       me.Id   as IdAcces,
       RolesName
from Menus menu
         left outer join Roles me on menu.Levels = me.Levels
         left outer join Menus par on menu.IdParent = par.Id
go

       
       