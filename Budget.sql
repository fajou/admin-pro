create sequence idTypeDepense as int start with 1000 increment by 1;
create sequence idDepense as int start with 1000 increment by 1;
create sequence porte_feuille_id as int start with 1 increment by 1;

create table TypeDepense
(
  Id          varchar(10) primary key,
  Designation varchar(100),
  ParJour decimal(12,2),
  ParSemaine decimal(12,2),
  ParMois decimal(12, 2),
  ValeurMax   decimal(12, 2),
  ValeurMin   decimal(12, 2)
);

create table Depense
(
  Id            varchar(10) primary key ,
  IdTypeDepense varchar(10) references TypeDepense (Id),
  Description   varchar(200),
  Valeur        decimal(12, 2),
  DateAjout     DATETIME,
);

create table PORTE_FEUILLE
(
    Id     varchar(10) primary key,
    Valeur decimal(12, 2)
);


create view Depenses as
select dep.id,
       idtypedepense,
       valeur,
       dateajout,
       designation,
       ParJour,
       valeurmax,
       valeurmin,
       Description
from Depense dep
       join TypeDepense TD on dep.IdTypeDepense = TD.Id;

create view TotalParJour as (
  select idtypedepense as IdType, Designation, DATEAJOUT, SUM(Valeur) as Total
  from Depenses
  group by idtypedepense, Designation, DATEAJOUT);


create function TotalDepense(@Id varchar(10), @aDate date) returns DECIMAL(12, 2)
BEGIN
  return (select Total from TotalParJour where DATEDIFF(DAY, @aDate, DATEAJOUT) = 0 and IdType = @Id);
end
go

create function TotalMensuel(@IdType varchar(10), @aMonth int, @aDate date) returns DECIMAL(12, 2)
BEGIN
    return (select SUM(valeur) as Total from Depenses where DATEDIFF(YEAR , @aDate, dateajout) = 0 and MONTH(dateajout) = @aMonth and idTypeDepense = @IdType)
END go


create function GlobalDepense(@aDate date) returns DECIMAL(12, 2)
    BEGIN
        declare
            @total decimal(12, 2);
        set @total = (select SUM(Total) as Total from TotalParJour t where DATEAJOUT = @aDate);
        return @total;
    end
go


create function SateDepense(@Id varchar(10), @dim date, @lun date, @mar date, @mer date, @jeu date, @ven date,
                            @sam date)
    returns @StateDepense TABLE
                          (
                              Id          varchar(10),
                              Designation varchar(200),
                              Dimanche    decimal(12, 2),
                              Lundi       decimal(12, 2),
                              Mardi       decimal(12, 2),
                              Mercredi    decimal(12, 2),
                              Jeudi       decimal(12, 2),
                              Vendredi    decimal(12, 2),
                              Samedi      decimal(12, 2)
                          ) as
BEGIN
    declare
        @Designation varchar(200);
    set @Designation = (select Designation from TypeDepense where Id = @Id)
    insert into @StateDepense
    values (@Id,
            @Designation,
            [dbo].TotalDepense(@Id, @dim),
            [dbo].TotalDepense(@Id, @lun),
            [dbo].TotalDepense(@Id, @mar),
            [dbo].TotalDepense(@Id, @mer),
            [dbo].TotalDepense(@Id, @jeu),
            [dbo].TotalDepense(@Id, @ven),
            [dbo].TotalDepense(@Id, @sam));
    return;
end
go

create function SateDepenseMensuel(@Id varchar(10), @Date date)
    returns @SateDepenseMensuel TABLE
                                (
                                    Id          varchar(10),
                                    Designation varchar(100),
                                    Janvier     decimal(12, 2),
                                    Fevrier     decimal(12, 2),
                                    Mars        decimal(12, 2),
                                    Avril       decimal(12, 2),
                                    Mai         decimal(12, 2),
                                    Juin        decimal(12, 2),
                                    Juillet     decimal(12, 2),
                                    Aout        decimal(12, 2),
                                    Septembre   decimal(12, 2),
                                    Octobre     decimal(12, 2),
                                    Novembre    decimal(12, 2),
                                    Decembre    decimal(12, 2)
                                ) as
BEGIN
    declare
        @Designation varchar(200);
    set @Designation = (select Designation from TypeDepense where Id = @Id)
    insert into @SateDepenseMensuel
    values (@Id,
            @Designation,
            [dbo].TotalMensuel(@Id, 1, @Date),
            [dbo].TotalMensuel(@Id, 2, @Date),
            [dbo].TotalMensuel(@Id, 3, @Date),
            [dbo].TotalMensuel(@Id, 4, @Date),
            [dbo].TotalMensuel(@Id, 5, @Date),
            [dbo].TotalMensuel(@Id, 6, @Date),
            [dbo].TotalMensuel(@Id, 7, @Date),
            [dbo].TotalMensuel(@Id, 8, @Date),
            [dbo].TotalMensuel(@Id, 9, @Date),
            [dbo].TotalMensuel(@Id, 10, @Date),
            [dbo].TotalMensuel(@Id, 11, @Date),
            [dbo].TotalMensuel(@Id, 12, @Date));
    return;
end
go




create function SateGlobalDepense(@dim date, @lun date, @mar date, @mer date, @jeu date, @ven date, @sam date)
    returns @SateGlobalDepense TABLE
                               (
                                   Id          varchar(10),
                                   Designation varchar(200),
                                   Dimanche    decimal(12, 2),
                                   Lundi       decimal(12, 2),
                                   Mardi       decimal(12, 2),
                                   Mercredi    decimal(12, 2),
                                   Jeudi       decimal(12, 2),
                                   Vendredi    decimal(12, 2),
                                   Samedi      decimal(12, 2)
                               ) as
BEGIN
    insert into @SateGlobalDepense
    values ('_All',
            'Globlal',
            [dbo].GlobalDepense(@dim),
            [dbo].GlobalDepense(@lun),
            [dbo].GlobalDepense(@mar),
            [dbo].GlobalDepense(@mer),
            [dbo].GlobalDepense(@jeu),
            [dbo].GlobalDepense(@ven),
            [dbo].GlobalDepense(@sam));
    return;
end;


select * from SateGlobalDepense('2019-04-07', '2019-04-08', '2019-04-09', '2019-04-10', '2019-04-11', '2019-04-12', '2019-04-13')
