﻿using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Admins.Traitements.Action;
using Admins.Traitements.Generic.Context;

namespace Admins
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            DependencyResolverConfig.Register();
        }

        protected void Session_Start()
        {

        }

        protected void Application_Error()
        {
            Handle404Error();
        }

        private void Handle404Error()
        {
            var error = Server.GetLastError();
            if ((error as HttpException)?.GetHttpCode() == 404)
            {
                Server.ClearError();
                Response.StatusCode = 404;
                Response.Clear();
                var routeData = new RouteData
                {
                    Values =
                    {
                        ["action"] = nameof(HTTPERRORController.PageNotFound),
                        ["controller"] = nameof(HTTPERRORController)
                    }
                };

                IController c = new HTTPERRORController();
                c.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
                return;
            }

            if ((error as HttpException)?.GetHttpCode() == 500)
            {
                Server.ClearError();
                Response.StatusCode = 500;
                Response.Clear();
                var routeData = new RouteData
                {
                    Values =
                    {
                        ["action"] = nameof(HTTPERRORController.PageError500),
                        ["controller"] = nameof(HTTPERRORController)
                    }
                };

                IController c = new HTTPERRORController();
                c.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
            }
        }
    }
}