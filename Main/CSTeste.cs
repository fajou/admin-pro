//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Attributes.Chart;
using Admins.Traitements.Generic.Base;
using Admins.Traitements.Generic.Util;
using Admins.Traitements.Models;
using static Admins.Traitements.Generic.Util.Use;


namespace Teste
{
    
    
    [ListEditable()]
    [ListDeletable()]
    public class TesteClass : BaseModel
    {
        
        private FormViewModel mNom;
        
        [Champ()]
        public virtual FormViewModel Nom
        {
            get
            {
                return mNom;
            }
            set
            {
                mNom = value;
            }
        }
    }
}
