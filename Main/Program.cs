﻿using System;
using System.Xml;
using Admins.Models.Budget;
using Admins.Traitements.Generic.Algo;
using Admins.Traitements.Generic.Algo.Method;
using Admins.Traitements.Generic.Attributes;
using Admins.Traitements.Generic.Attributes.Chart;
using Admins.Traitements.Generic.Base;
using Admins.Traitements.Generic.Generator.Code;
using Admins.Traitements.Generic.Generator.Xml;
using Admins.Traitements.Generic.Util;
using Admins.Traitements.Models;
using Admins.Traitements.Services;

namespace Main
{
    public static class Program
    {
        private static void Main(string[] args)
        {
//            var model = new BaseModelGenerator(new CodeClassInfo("Teste", "TesteClass", "TableName"));
//            model.GenerateCSharpCode("CSTeste.cs");

            var s = Use.FormatMGA(200000, true);
            Console.WriteLine(s);
            return;
            var xmlClass = new XmlClass
            {
                ChartType =  new ChartType(),
                Lists = new Lists(),
                Forms = new Forms(),
                ListDeletable = new ListDeletable()
            };
            //xmlClass.SaveToXml(xmlClass, "XmlClasseteste.xml");
            var obj = (XmlClass) xmlClass.ReadBaseXml("XmlClasseteste.xml");
            
            Console.WriteLine(obj.ListEditable.Text);

            Console.WriteLine();
            
            var g = new Graph<string>();
            g.Add("A");
            g.Add("B");
            g.Add("C");
            g.Add("D");
            g.Add("E");
            g.Add("F");
            g.Add("G");
            g.Add("H");
            g.Add("I");
            g.Add("J");   
            
            g.AddArc("C", "A", 3);
            g.AddArc("E", "A", 2);
            g.AddArc("D", "A", 2);
            g.AddArc("D", "B", 3);
            
            g.AddArc("F", "C", 3);
            g.AddArc("H", "E", 2);
            g.AddArc("G", "D", 1);
            g.AddArc("I", "D", 4);
            
            g.AddArc("G", "F", 1);
            g.AddArc("J", "H", 3);
            g.AddArc("I", "G", 4);
            g.AddArc("J", "I", 3);
        
           
            /*var method = new MaximisationFlow<Tache>();
            var a = new Tache{Nom = "A", Durree = 2};
            var b = new Tache{Nom = "B", Durree = 3};
            var c = new Tache{Nom = "C", Durree = 4};
            var d = new Tache{Nom = "D", Durree = 1};
            var e = new Tache{Nom = "E", Durree = 3};
            
            method.Add(a);
            method.Add(b);
            method.Add(c);
            method.Add(d);
            method.Add(e);
            method.AddArc(c, a, 5, 2);
            method.AddArc(c, b, 4, 1);
            method.AddArc(a, b, 3, 2);
            
            method.AddArc(d, c, 3, 2);
            method.AddArc(e, d, 4, 2);
            method.AddArc(e, c, 1, 0);
            
            //method.Build(typeof(Tache), "Durree");
            //var preds = method.PrevesNode(method.End);
            //method.Build();
            //var arcs = method.Arcs();*/
            
            var parcours = new Parcours<string>(g);
            
            var result = parcours.SortByDijkstra("B");
            foreach (var arc in result)
            {
                Console.WriteLine(arc.Value.Source + " " + arc.Value.Node);
            }
        }
    }
}