using System;
using System.Collections.Generic;
using Admins.Models.Budget;
using Admins.Models.Budget.State;
using Admins.Traitements;
using Admins.Traitements.Generic.Base;

namespace Admins.Services.Budget
{
    public interface IStatistiqueBudgetService : IService
    {
        /// <summary>
        /// Retourne la statistique selon id type du depense, et la date donnée.
        /// </summary>
        /// <param name="typeId"></param>
        /// <param name="date"></param>
        /// <returns>StateHebdomadaire</returns>
        StateHebdomadaire Get(string typeId, DateTime date);

        /// <summary>
        /// Retourne la statistique de la semaine du budget.
        /// </summary>
        /// <param name="date"></param>
        /// <returns>StateHebdomadaire</returns>
        StateHebdomadaire GetGlobal(DateTime date);

        /// <summary>
        /// Liste de la statistique du budget par semaine à une date donnée.
        /// </summary>
        /// <param name="date"></param>
        /// <returns>List</returns>
        List<BaseModel> Hebdomadaire(DateTime date);

        /// <summary>
        /// Liste de la statistique du buget annuel à une date donnée.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        List<BaseModel> Annuel(DateTime date);

        /// <summary>
        /// Retourne la status du porte Feuille personalisée.
        /// </summary>
        /// <returns></returns>
        PorteFeuilleBudget PorteFeuille();
    }
}