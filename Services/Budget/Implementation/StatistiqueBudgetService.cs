using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Admins.Models.Budget;
using Admins.Models.Budget.State;
using Admins.Traitements.Generic.Base;
using Admins.Traitements.Generic.Util;
using Admins.Traitements.Services.Repository;

namespace Admins.Services.Budget.Implementation
{
    public class StatistiqueBudgetService : IStatistiqueBudgetService
    {
        private readonly IDataBaseModel _model;

        public StatistiqueBudgetService(IDataBaseModel model)
        {
            _model = model;
        }

        public StateHebdomadaire Get(string typeId, DateTime date)
        {
            var s = new Semaine(date);
            var requete = $"select * from SateDepense('{typeId}', '{s.Dimanche:u}', '{s.Lundi:u}', '{s.Mardi:u}', '{s.Mercredi:u}', '{s.Jeudi:u}', '{s.Vendredi:u}', '{s.Samedi:u}')";
            return (StateHebdomadaire) _model.FindQuery(requete, new StateHebdomadaire()).First();
        }

        public StateHebdomadaire GetGlobal(DateTime date)
        {
            var s = new Semaine(date);
            var requete = $"select * from SateGlobalDepense('{s.Dimanche:u}', '{s.Lundi:u}', '{s.Mardi:u}', '{s.Mercredi:u}', '{s.Jeudi:u}', '{s.Vendredi:u}', '{s.Samedi:u}')";
            return (StateHebdomadaire) _model.FindQuery(requete, new StateHebdomadaire()).First();
        }

        public List<BaseModel> Hebdomadaire(DateTime date)
        {
            SqlConnection conn = null;
            try
            {
                var s = new Semaine(date);
                var depenses = new List<StateHebdomadaire>();
                conn = Connexion.GetConnection();
                var types = _model.Find(new TypeDepense(), conn).Cast<TypeDepense>().ToArray();

                foreach (var type in types)
                {
                    var requete = $"select * from SateDepense('{type.Id}', '{s.Dimanche:u}', '{s.Lundi:u}', '{s.Mardi:u}', '{s.Mercredi:u}', '{s.Jeudi:u}', '{s.Vendredi:u}', '{s.Samedi:u}')";
                    var deps = _model.FindQuery(requete, new StateHebdomadaire(), conn);
                    var stateDepense = (StateHebdomadaire) deps.First();
                    stateDepense.Total = stateDepense.TotalDefault();
                    stateDepense.Maximum = type.ParSemaine;
                    depenses.Add(stateDepense);
                }

                var stateTotal = new StateHebdomadaire
                {
                    Designation = "Total",
                    Dimanche = depenses.Sum(depense => depense.Dimanche),
                    Lundi = depenses.Sum(depense => depense.Lundi),
                    Mardi = depenses.Sum(depense => depense.Mardi),
                    Mercredi = depenses.Sum(depense => depense.Mercredi),
                    Jeudi = depenses.Sum(depense => depense.Jeudi),
                    Vendredi = depenses.Sum(depense => depense.Vendredi),
                    Samedi = depenses.Sum(depense => depense.Samedi),
                    Maximum = depenses.Sum(depense => depense.Maximum)
                };
                stateTotal.Total = stateTotal.TotalDefault();
                depenses.Add(stateTotal);

                return depenses.Cast<BaseModel>().ToList();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                conn?.Close();
            }
        }

        public List<BaseModel> Annuel(DateTime date)
        {
            SqlConnection conn = null;
            try
            {
                conn = Connexion.GetConnection();
                var depenses = new List<StateAnnuel>();
                var types = _model.Find(new TypeDepense(), conn).Cast<TypeDepense>();

                foreach (var type in types)
                {
                    var requete = $"select * from SateDepenseMensuel('{type.Id}', '{date:u}')";
                    var mensuel = (StateAnnuel) _model.FindQuery(requete, new StateAnnuel(), conn).First();
                    mensuel.Total = mensuel.TotalDefault();
                    depenses.Add(mensuel);
                }

                var totalGeneral = new StateAnnuel
                {
                    Designation = "Total",
                    Janvier = depenses.Sum(mensuel => mensuel.Janvier),
                    Fevrier = depenses.Sum(mensuel => mensuel.Fevrier),
                    Mars = depenses.Sum(mensuel => mensuel.Mars),
                    Avril = depenses.Sum(mensuel => mensuel.Avril),
                    Mai = depenses.Sum(mensuel => mensuel.Mai),
                    Juin = depenses.Sum(mensuel => mensuel.Juin),
                    Juillet = depenses.Sum(mensuel => mensuel.Juillet),
                    Aout = depenses.Sum(mensuel => mensuel.Aout),
                    Septembre = depenses.Sum(mensuel => mensuel.Septembre),
                    Octobre = depenses.Sum(mensuel => mensuel.Octobre),
                    Novembre = depenses.Sum(mensuel => mensuel.Novembre),
                    Decembre = depenses.Sum(mensuel => mensuel.Decembre)
                };
                totalGeneral.Total = totalGeneral.TotalDefault();
                depenses.Add(totalGeneral);
                return depenses.Cast<BaseModel>().ToList();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                conn?.Close();
            }
        }

        public PorteFeuilleBudget PorteFeuille()
        {
            var porteFeuille = _model.First(new PorteFeuille());
            return new PorteFeuilleBudget
            {
                Hebdomadaire = 0,
                Mensuel = 0,
                PorteFeuille = 90000
            };
        }
    }
}