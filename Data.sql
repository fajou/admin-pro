create sequence idHeureSup as int start with 1 increment by 1;
create sequence idSalaire as int start with 1 increment by 1;
create sequence idJourFerier as int start with 1 increment by 1;
create sequence idEmploye as int start with 1 increment by 1;
create sequence idPointages as int start with 1 increment by 1;
create sequence idTache as int start with 1 increment by 1;

create table HeureSup
(
  Id varchar(10) primary key,
  Min integer,
  Max integer,
  Pourcentage numeric(3, 2)
);

create table JourFerier
(
  Id   varchar(10),
  Jour DATE,
  Demi BIT
);


create table Employe
(
  Id     varchar(10) primary key,
  Nom    varchar(50),
  Mail   varchar(50),
  Metier varchar(50),
  Type   int
);

create table Tache(
    Id varchar(10) primary key,
    Nom varchar(50),
    Contraintes varchar(10),
    Durree double precision
);

INSERT into Sommet values ('1', 'A', null, '4');
INSERT into Sommet values ('2', 'B', null, '2');
INSERT into Sommet values ('3', 'C', 'A', '1');
INSERT into Sommet values ('4', 'D', 'A,B', '1');
INSERT into Sommet values ('5', 'E', 'A', '2');
INSERT into Sommet values ('6', 'F', 'C', '2');
INSERT into Sommet values ('7', 'G', 'D,F', '2');
INSERT into Sommet values ('8', 'H', 'E', '10');
INSERT into Sommet values ('9', 'I', 'G', '4');
INSERT into Sommet values ('10', 'J', 'H,I', '1');


create table Salaire
(
  Id varchar(10) primary key,
  IdEmploye varchar(10) references Employe (Id),
  Valeur double precision,
  TotalHeur int,
  TravailPar int
);

create table Pointages
(
  Id        varchar(10),
  IdEmploye varchar(10) references Employe (Id),
  DebutDimanche date,
  FinSamedi date,
  Dimanche varchar(100),
  Lundi varchar(100),
  Mardi varchar(100),
  Mercredi varchar(100),
  Jeudi varchar(100),
  Vendredi varchar(100),
  Samedi varchar(100),
  Total varchar(100)
);

create table TravailType
(
  Id varchar(10) primary key,
  Par varchar(10)
);

create table TypeEmploye(
  Id varchar(10) primary key,
  Employe varchar(10)
);


create view ViewPointages as
  select e.Id as IdPointage, e.Nom, p.*
  from Employe e
       left join Pointages p on e.Id = p.IdEmploye;

       

select e.Nom, p.*
from Employe e
       left join (select p.*
                  from Pointages p
                  where DATEDIFF(DAY, DebutDimanche, '2019-02-03 14:56:57Z') <= 0
                  and 0 <= DATEDIFF(DAY, FinSamedi, '2019-02-03 14:56:57Z')) as p on p.IdEmploye = e.Id;

create sequence idPathGraph increment by 1 start with 200;        
create table PathGraph(
    Id varchar(10),
    Graph varchar(100),
    FileName varchar(200)
);