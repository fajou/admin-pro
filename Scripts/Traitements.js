let nameId = "";
let valueId = "";


function GetListe(id, name, url) {
    $("#liste").html("<p style='text-align: center;'>Chargement encours ...</p>");
    $.ajax({
        url:url,
        method: "GET",
        success:function(result) {
            $("#liste").html(result);
            SetOption();
            nameId = name;
            valueId = id;
        }
    });
}

function PagingListeView(url) {
    $("#liste").html("<p style='text-align: center;'>Chargement encours ...</p>");
    $.ajax({
        url:url,
        method: "GET",
        success:function(result) {
            $("#liste").html(result);
            SetOption();
        }
    });
}

function PagingSearchListeView(url, formId) {
    const data = $("#" + formId).serialize();
    $("#liste").html("<p style='text-align: center;'>Chargement encours ...</p>");
    $.ajax({
        url:url,
        method: "GET",
        data:data,
        success:function(result) {  
            $("#liste").html(result);
            SetOption();
        }
        ,
        error:function (error) {
            $("#liste").html(error);
        }
    });
}

function SetOption() {
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass   : 'iradio_minimal-blue'
    });

    $(function() {
        $('.select2').select2();
    });
}

function SetValue() {
    const value = $("input[name='" + nameId + "']:checked").val();
    if (value) {
        $("#" + valueId).val(value);
    }
}

function ImageUploader(event, dos) {
    const files = event.files;
    if (files.length > 0) {
        if (window.FormData !== undefined) {
            const data = new FormData();
            for (let x = 0; x < files.length; x++) {
                data.append(files[x].name, files[x]);
            }
            data.append("__RequestVerificationToken", $('input[name=__RequestVerificationToken]').val());
            data.append("dos", dos);
            $.ajax({
                type: "POST",
                url: '/Admins/ImageUploader',
                contentType: false,
                processData: false,
                data: data,
                success: function (result) {
                    console.log(result);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        } else {
            alert("This browser doesn't support HTML5 file uploads!");
        }
    }
}

//Requette ajax de la page forms d'un model.
function __RequestFormsPage(url) {
    __Load();
    $.ajax({
        url: url,
        success: function (result) {
            $("#forms").html(result);
            $('.select2').select2();
            __EndLoad();
        }
    });
}